﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PortalBranchNav.Models.dbBranchNav;
using PortalBranchNav.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace PortalBranchNav.Component
{
    public class Utility
    {

        private readonly dbBranchNavContext _context;
        private readonly IConfiguration _configuration;
        private readonly LastSessionLog lastSession;
        private readonly AccessSecurity accessSecurity;

        public const int StatusMember_MenungguInputKode = 1;
        public const int StatusMember_MenungguValidasiPanitia = 2;
        public const int StatusMember_Tervalidasi = 3;
        public const int StatusMember_Reject = 4;


        public const int StatusRole_SuperAdmin = 1;
        public const int StatusRole_Panitia = 2;
        public const int StatusRole_Anggota = 3;


        public const int TypeDokumen_Memo = 1;
        public const int TypeDokumen_Notulen = 2;
        public const int TypeDokumen_DRF = 3;
        public const int TypeDokumen_BAST = 4;
        public const int TypeDokumen_BAUAT = 5;
        public const int TypeDokumen_PIR = 6;
        public const int TypeDokumen_TestingQA = 7;
        public const int TypeDokumen_DokumenLainnya = 8;


        #region Select Data Lookup
        public IEnumerable<TblLookup> SelectLookup(string Type, dbBranchNavContext _context, string jwt)
        {
            //List<TblLookup> ListData = new List<TblLookup>();

            //ListData = _context.TblLookup.Where(m => m.Type == Type && m.IsActive == true && m.IsDeleted == false).OrderBy(m => m.OrderBy).ToList();

            //return ListData;
            var ListData = new List<TblLookup>();

            try
            {
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:GetLookupByType"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new {
                        type = Type
                    }, jwt);
                if (resultApi && !string.IsNullOrEmpty(result))
                    ListData = JsonConvert.DeserializeObject<List<TblLookup>>(result);
                return ListData;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        #endregion

        #region Select Data By ID For Dropdown Master Role
        public IEnumerable<DataDropdownServerSide> SelectDataMasterRole(dbBranchNavContext _context, string jwt)
        {
            //List<DataDropdownServerSide> data2 = new List<DataDropdownServerSide>();

            //data2 = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "[sp_Dropdown_MasterRole]", new SqlParameter[]{

            //        });
            //data2 = data2 != null ? data2 : new List<DataDropdownServerSide>();

            //return data;
            //HttpContext p = new HttpContext();
            var Listdata = new List<DataDropdownServerSide>();
            try
            {
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownMasterRole"];
                //var data = new ListDataDropdownServerSide();
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new { }, jwt);
                if (resultApi && !string.IsNullOrEmpty(result))
                    Listdata = JsonConvert.DeserializeObject<List<DataDropdownServerSide>>(result);
                return Listdata;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }

        public static IEnumerable<DataDropdownServerSide> SelectDataMasterRoleById(int? Id, dbBranchNavContext _context)
        {
            List<DataDropdownServerSide> data = new List<DataDropdownServerSide>();

            data = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "[sp_Dropdown_MasterRoleById]", new SqlParameter[]{
                        new SqlParameter("@Id", Id)

                    });
            data = data != null ? data : new List<DataDropdownServerSide>();

            return data;
        }
        #endregion



        #region Select Data By ID For Dropdown Menu
        public IEnumerable<DataDropdownServerSide> SelectDataMenu(dbBranchNavContext _context, string jwt)
        {
            //List<DataDropdownServerSide> data = new List<DataDropdownServerSide>();

            //data = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "sp_dropdown_menu", new SqlParameter[]{
                        
            //        });
            //data = data != null ? data : new List<DataDropdownServerSide>();

            //return data;
            var Listdata2 = new List<DataDropdownServerSide>();
            try
            {
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownMenu"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new { }, jwt);
                if (resultApi && !string.IsNullOrEmpty(result))
                    Listdata2 = JsonConvert.DeserializeObject<List<DataDropdownServerSide>>(result);
                return Listdata2;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        #endregion

        #region Select Data By ID For Dropdown Unit
        public IEnumerable<DataDropdownServerSide> SelectDataUnit(int? Id, dbBranchNavContext _context, string jwt)
        {
            //List<DataDropdownServerSide> data = new List<DataDropdownServerSide>();
            //data = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "SP_Dropdown_Unit_ById", new SqlParameter[]{
            //            new SqlParameter("@Id", Id)
            //        });
            //data = data != null ? data : new List<DataDropdownServerSide>();

            //return data;
            List<DataDropdownServerSide> data2 = new List<DataDropdownServerSide>();
            try
            {
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownUnitById"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        ids = Id
                    }, jwt);
                if (resultApi && !string.IsNullOrEmpty(result))
                    data2 = JsonConvert.DeserializeObject<List<DataDropdownServerSide>>(result);
                return data2;
            }
            catch (Exception Ex)
            {

                throw;
            }

        }
        #endregion

        //#region Select Data By ID For Dropdown Unit 2
        //public static IEnumerable<DataDropdownServerSide> SelectDataUnit2(string Id, dbBranchNavContext _context)
        //{
        //    List<DataDropdownServerSide> data = new List<DataDropdownServerSide>();
        //    data = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "SP_Dropdown_Unit_ById", new SqlParameter[]{
        //                new SqlParameter("@Id", Id)
        //            });
        //    data = data != null ? data : new List<DataDropdownServerSide>();

        //    return data;

        //}
        //#endregion

        //#region Select Data By ID For Dropdown Sistem
        //public static IEnumerable<DataDropdownServerSide> SelectDataMasterSistem(int? Id, dbBranchNavContext _context)
        //{
        //    List<DataDropdownServerSide> data = new List<DataDropdownServerSide>();
        //    data = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "[sp_Dropdown_MasterSistemById]", new SqlParameter[]{
        //                new SqlParameter("@Id", Id)
        //            });
        //    data = data != null ? data : new List<DataDropdownServerSide>();

        //    return data;

        //}
        //#endregion

        //#region Select Data By ID For Dropdown Type Client
        //public static IEnumerable<DataDropdownServerSide> SelectDataTypeClient(int? Id, dbBranchNavContext _context)
        //{
        //    List<DataDropdownServerSide> data = new List<DataDropdownServerSide>();
        //    data = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "[sp_Dropdown_MasterTypeClientById]", new SqlParameter[]{
        //                new SqlParameter("@Id", Id)
        //            });
        //    data = data != null ? data : new List<DataDropdownServerSide>();

        //    return data;

        //}
        //#endregion

        //#region Select Data By ID For Dropdown Client
        //public static IEnumerable<DataDropdownServerSide> SelectDataClient(int? Id, dbBranchNavContext _context)
        //{
        //    List<DataDropdownServerSide> data = new List<DataDropdownServerSide>();
        //    data = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "[sp_Dropdown_ClientById]", new SqlParameter[]{
        //                new SqlParameter("@Id", Id)
        //            });
        //    data = data != null ? data : new List<DataDropdownServerSide>();

        //    return data;

        //}
        //#endregion

        #region LDAP Login
        public static bool AuthenticationLdap(String userName, String passWord)
        {
            string Domain = string.Empty;
            string path = string.Empty;
            string sDomainAndUsername = Domain;
            path = GetConfig.AppSetting["AppSettings:LDAPConnection:Url"];
            Domain = "uid=" + userName + "," + GetConfig.AppSetting["AppSettings:LDAPConnection:LdapHierarchy"];
            AuthenticationTypes at = AuthenticationTypes.ServerBind;
            DirectoryEntry entry = new DirectoryEntry(path, Domain, passWord, at);
            bool cek = false;
            try
            {
                Object obj = entry.NativeObject;
                DirectorySearcher mySearcher = new DirectorySearcher(entry);
                SearchResult result;
                mySearcher.Filter = "(uid=" + userName + ")";
                mySearcher.PropertiesToLoad.Add("sn");
                result = mySearcher.FindOne();
                cek = true;
            }
            catch (Exception ex)
            {
                //throw new Exception("Error authenticating user. " + ex.Message);
                cek = false;
            }
            return cek;
        }
        #endregion

        //#region Select Data By ID For Dropdown Jabatan
        //public static IEnumerable<DataDropdownServerSide> SelectDataJabatanById(int? Id, dbBranchNavContext _context)
        //{
        //    List<DataDropdownServerSide> data = new List<DataDropdownServerSide>();
        //    data = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "SP_Dropdown_Jabatan_ById", new SqlParameter[]{
        //        new SqlParameter("@Id", Id)
        //            });
        //    data = data != null ? data : new List<DataDropdownServerSide>();

        //    return data;

        //}
        ////#endregion

        #region Select Data For Dropdown Jabatan
        public IEnumerable<DataDropdownServerSide> SelectDataJabatan(dbBranchNavContext _context, string jwt)
        {
            //List<DataDropdownServerSide> data = new List<DataDropdownServerSide>();
            //data = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "sp_Dropdown_Jabatan", new SqlParameter[]{
            //        });
            //data = data != null ? data : new List<DataDropdownServerSide>();

            //var Listdata = new List<DataDropdownServerSide>();
            var data2 = new List<DataDropdownServerSide>();

            try
            {
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownJabatan"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new { }, jwt);
                if (resultApi && !string.IsNullOrEmpty(result))
                    data2 = JsonConvert.DeserializeObject<List<DataDropdownServerSide>>(result);
                return data2;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        #endregion

        #region Select Data For Dropdown Jenis Laporan
        public IEnumerable<TblLookup> SelectJenisLaporan(string Type,dbBranchNavContext _context, string jwt)
        {
            //List<DataDropdownServerSide> data = new List<DataDropdownServerSide>();
            //data = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "sp_Dropdown_Jabatan", new SqlParameter[]{
            //        });
            //data = data != null ? data : new List<DataDropdownServerSide>();

            //var Listdata = new List<DataDropdownServerSide>();
            var data2 = new List<TblLookup>();

            try
            {
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:GetLookupByType"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new {
                        type = Type
                    }, jwt);
               /* bool resultApi = true;
                string result = "[{\"id\":1,\"text\":\"Jenis Laporan 1\",\"format_selected\":null,\"nama_text\":null}," +
                    "{\"id\":2,\"text\":\"Jenis Laporan 2\",\"format_selected\":null,\"nama_text\":null}," +
                    "{\"id\":3,\"text\":\"Jenis Laporan 3\",\"format_selected\":null,\"nama_text\":null}," +
                    "{\"id\":4,\"text\":\"Jenis Laporan 4\",\"format_selected\":null,\"nama_text\":null}," +
                    "{\"id\":5,\"text\":\"Jenis Laporan 5\",\"format_selected\":null,\"nama_text\":null}]";*/
                if (resultApi && !string.IsNullOrEmpty(result))
                    data2 = JsonConvert.DeserializeObject<List<TblLookup>>(result);
                return data2;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        #endregion

        #region Select Data For Dropdown Divisi Pengguna
        public IEnumerable<DataDropdownServerSide> SelectDivisiPengguna(dbBranchNavContext _context, string jwt)
        {
            //List<DataDropdownServerSide> data = new List<DataDropdownServerSide>();
            //data = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "sp_Dropdown_Jabatan", new SqlParameter[]{
            //        });
            //data = data != null ? data : new List<DataDropdownServerSide>();

            //var Listdata = new List<DataDropdownServerSide>();
            //var data = new DataResponseDivPengguna();
            var data2 = new ServiceResult2<ListDataDropdownServerSide2>();

            try
            {
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownUnit"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        pageNumber = 1,
                        pageSize = 2300
                    }, jwt);
                //bool resultApi = true;
                //string result = "[{\"id\":1,\"text\":\"Divisi Pengguna 1\",\"format_selected\":null,\"nama_text\":null}," +
                //    "{\"id\":2,\"text\":\"Divisi Pengguna 2\",\"format_selected\":null,\"nama_text\":null}," +
                //    "{\"id\":3,\"text\":\"Divisi Pengguna 3\",\"format_selected\":null,\"nama_text\":null}," +
                //    "{\"id\":4,\"text\":\"Divisi Pengguna 4\",\"format_selected\":null,\"nama_text\":null}," +
                //    "{\"id\":5,\"text\":\"Divisi Pengguna 5\",\"format_selected\":null,\"nama_text\":null},]";

                if (resultApi && !string.IsNullOrEmpty(result))
                        data2 = JsonConvert.DeserializeObject<ServiceResult2<ListDataDropdownServerSide2>>(result);

                return data2.data.data;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        #endregion

        #region Select Data For Dropdown Unit Wilayah
        public IEnumerable<DataDropdownServerSide> SelectUnitWilayah(dbBranchNavContext _context, string jwt)
        {
            //List<DataDropdownServerSide> data = new List<DataDropdownServerSide>();
            //data = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "sp_Dropdown_Jabatan", new SqlParameter[]{
            //        });
            //data = data != null ? data : new List<DataDropdownServerSide>();

            //var Listdata = new List<DataDropdownServerSide>();
            //var data = new DataResponseDivPengguna();
            var data2 = new ServiceResult2<ListDataDropdownServerSide2>();

            try
            {
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownUnitWilayah"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        pageNumber = 1,
                        pageSize = 2300
                    }, jwt);
                //bool resultApi = true;
                //string result = "[{\"id\":1,\"text\":\"Divisi Pengguna 1\",\"format_selected\":null,\"nama_text\":null}," +
                //    "{\"id\":2,\"text\":\"Divisi Pengguna 2\",\"format_selected\":null,\"nama_text\":null}," +
                //    "{\"id\":3,\"text\":\"Divisi Pengguna 3\",\"format_selected\":null,\"nama_text\":null}," +
                //    "{\"id\":4,\"text\":\"Divisi Pengguna 4\",\"format_selected\":null,\"nama_text\":null}," +
                //    "{\"id\":5,\"text\":\"Divisi Pengguna 5\",\"format_selected\":null,\"nama_text\":null},]";

                if (resultApi && !string.IsNullOrEmpty(result))
                    data2 = JsonConvert.DeserializeObject<ServiceResult2<ListDataDropdownServerSide2>>(result);

                return data2.data.data;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        #endregion
    }
}
