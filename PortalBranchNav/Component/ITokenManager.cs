﻿using DigiSalesWebPortal.ViewModels;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace DigiSalesWebPortal.Interface
{
    public interface ITokenManager
    {
        /// <summary>
        ///     Get claims from jwt token
        /// </summary>
        /// <param name="token"></param>
        /// <returns>Jwt token claims Service.WebApi.Catalog.ViewModels.Principal_VM</returns>
        Principal_VM GetPrincipal(string token);
    }
    public class TokenManager : ITokenManager
    {
        private IHttpContextAccessor _context;
        public TokenManager(IHttpContextAccessor httpContextAccessor)
        {
            _context = httpContextAccessor ?? throw new ArgumentNullException(nameof(httpContextAccessor)); ;
        }
        public Principal_VM GetPrincipal(string token)
        {
            //string authorization = _context.HttpContext.Request.Headers["Authorization"];
            if (token != null)
            {
                var tokenHandler = new JwtSecurityTokenHandler();

                var parsedToken = tokenHandler.ReadJwtToken(token);

                var Nama_Pegawai = parsedToken.Claims.Where(c => c.Type == "Nama_Pegawai").FirstOrDefault().Value;
                var Nama_Role = parsedToken.Claims.Where(c => c.Type == "Nama_Role").FirstOrDefault().Value;
                var Nama_Unit = parsedToken.Claims.Where(c => c.Type == "Nama_Unit").FirstOrDefault().Value;
                var Npp = parsedToken.Claims.Where(c => c.Type == "Npp").FirstOrDefault().Value;
                var Pegawai_Id = parsedToken.Claims.Where(c => c.Type == "Pegawai_Id").FirstOrDefault().Value;
                var Role_Id = parsedToken.Claims.Where(c => c.Type == "Role_Id").FirstOrDefault().Value;
                var Role_Nama_Unit = parsedToken.Claims.Where(c => c.Type == "Role_Nama_Unit").FirstOrDefault().Value;
                var Role_Unit_Id = parsedToken.Claims.Where(c => c.Type == "Role_Unit_Id").FirstOrDefault().Value;
                var Status_Role = parsedToken.Claims.Where(c => c.Type == "Status_Role").FirstOrDefault().Value;
                var Unit_Id = parsedToken.Claims.Where(c => c.Type == "Unit_Id").FirstOrDefault().Value;
                var User_Id = parsedToken.Claims.Where(c => c.Type == "User_Id").FirstOrDefault().Value;
                var User_Role_Id = parsedToken.Claims.Where(c => c.Type == "User_Role_Id").FirstOrDefault().Value;

                return new Principal_VM()
                {
                    Nama_Pegawai = Nama_Pegawai,
                    Nama_Role = Nama_Role,
                    Nama_Unit = Nama_Unit,
                    Npp = Npp,
                    Pegawai_Id = Pegawai_Id,
                    Role_Id = Role_Id,
                    Role_Nama_Unit = Role_Nama_Unit,
                    Role_Unit_Id = Role_Unit_Id,
                    Status_Role = Status_Role,
                    Unit_Id = Unit_Id,
                    User_Id = User_Id,
                    User_Role_Id = User_Role_Id
                };
            }
            else
            {
                return null;
            }
        }
    }
}
