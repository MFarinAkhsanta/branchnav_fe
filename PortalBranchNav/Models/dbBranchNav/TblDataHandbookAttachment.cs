﻿using System;
using System.Collections.Generic;

namespace PortalBranchNav.Models.dbBranchNav
{
    public partial class TblDataHandbookAttachment
    {
        public int Id { get; set; }
        public int? HandbookDetailsId { get; set; }
        public string NamaFile { get; set; }
        public string TypeFile { get; set; }
        public string Size { get; set; }
        public string Path { get; set; }
        public string FullPath { get; set; }
        public DateTime? UploadTime { get; set; }
        public int? UploadById { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
