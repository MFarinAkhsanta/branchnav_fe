﻿using System;
using System.Collections.Generic;

namespace PortalBranchNav.Models.dbBranchNav
{
    public partial class TblDataKreditTarget
    {
        public int Id { get; set; }
        public string KodeCabang { get; set; }
        public string NamaUnit { get; set; }
        public decimal? BaselineKreditBbmenengah { get; set; }
        public decimal? TargetKreditBbmenengahTahunBerjalan { get; set; }
        public decimal? BaselinePenyaluranKur { get; set; }
        public decimal? TargetPenyaluranKurtahunBerjalan { get; set; }
        public decimal? BaselineKreditFlexi { get; set; }
        public decimal? TargetKreditFlexiTahunBerjalan { get; set; }
        public decimal? BaselineKreditGriya { get; set; }
        public decimal? TargetKreditGriyaTahunBerjalan { get; set; }
        public decimal? BaselineKreditOthers { get; set; }
        public decimal? TargetKreditOthersTahunBerjalan { get; set; }
        public DateTime? Periode { get; set; }
        public decimal? BaselineKreditBbkecil { get; set; }
        public decimal? TargetKreditBbkecilTahunBerjalan { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
