﻿using System;
using System.Collections.Generic;

namespace PortalBranchNav.Models.dbBranchNav
{
    public partial class TblJwtRepoitory
    {
        public int Id { get; set; }
        public int? EmployeeId { get; set; }
        public string ClientId { get; set; }
        public string ClientIp { get; set; }
        public string RefreshToken { get; set; }
        public bool? IsStop { get; set; }
        public string TokenId { get; set; }
    }
}
