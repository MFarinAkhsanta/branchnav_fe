﻿using System;
using System.Collections.Generic;

namespace PortalBranchNav.Models.dbBranchNav
{
    public partial class TblDataRatingYudisium
    {
        public int Id { get; set; }
        public string KodeOutlet { get; set; }
        public DateTime? Periode { get; set; }
        public int? Tahun { get; set; }
        public string RatingKinerja { get; set; }
        public string RatingPengelolaanResiko { get; set; }
        public string RatingGabungan { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
    }
}
