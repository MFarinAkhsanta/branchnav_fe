﻿using System;
using System.Collections.Generic;

namespace PortalBranchNav.Models.dbBranchNav
{
    public partial class TblDataRevitalisasi
    {
        public int Id { get; set; }
        public int? UnitId { get; set; }
        public string ProjectNo { get; set; }
        public string KodeOutlet { get; set; }
        public DateTime? Periode { get; set; }
        public string NamaProyek { get; set; }
        public string JenisProyek { get; set; }
        public decimal? EstimasiBiaya { get; set; }
        public string NoDokumen { get; set; }
        public string Uic { get; set; }
        public string StatusProject { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
    }
}
