﻿using System;
using System.Collections.Generic;

namespace PortalBranchNav.Models.dbBranchNav
{
    public partial class TblDataSei
    {
        public int Id { get; set; }
        public string KodeUnit { get; set; }
        public DateTime? Periode { get; set; }
        public decimal? Dimensi1 { get; set; }
        public decimal? Dimensi2 { get; set; }
        public decimal? OutletDimensi3 { get; set; }
        public decimal? CabangDimensi3 { get; set; }
        public decimal? WilayahDimensi3 { get; set; }
        public decimal? ScoreSeioutlet { get; set; }
        public decimal? ScoreSeicabang { get; set; }
        public decimal? ScoreSeiwilayah { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
    }
}
