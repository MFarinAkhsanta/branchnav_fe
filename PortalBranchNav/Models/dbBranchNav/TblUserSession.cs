﻿using System;
using System.Collections.Generic;

namespace PortalBranchNav.Models.dbBranchNav
{
    public partial class TblUserSession
    {
        public int UserId { get; set; }
        public string SessionId { get; set; }
        public DateTime LastActive { get; set; }
        public string Info { get; set; }
        public int? RoleId { get; set; }
        public int? UnitId { get; set; }

        public virtual TblMasterRole Role { get; set; }
        public virtual TblUnit1 Unit { get; set; }
        public virtual TblUser User { get; set; }
    }
}
