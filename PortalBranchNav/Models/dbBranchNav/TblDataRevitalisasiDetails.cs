﻿using System;
using System.Collections.Generic;

namespace PortalBranchNav.Models.dbBranchNav
{
    public partial class TblDataRevitalisasiDetails
    {
        public int Id { get; set; }
        public int? DataRevitilisasiId { get; set; }
        public int? PegawaiId { get; set; }
        public int? UnitId { get; set; }
        public DateTime? Tanggal { get; set; }
        public string Catatan { get; set; }
        public decimal? Progress { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
    }
}
