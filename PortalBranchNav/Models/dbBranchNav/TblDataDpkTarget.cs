﻿using System;
using System.Collections.Generic;

namespace PortalBranchNav.Models.dbBranchNav
{
    public partial class TblDataDpkTarget
    {
        public int Id { get; set; }
        public string KodeCabang { get; set; }
        public string NamaUnit { get; set; }
        public decimal? BaselineTabungan { get; set; }
        public decimal? TargetTabunganTahunBerjalan { get; set; }
        public decimal? BaselineGiro { get; set; }
        public decimal? TargetGiroTahunBerjalan { get; set; }
        public decimal? BaselineDeposito { get; set; }
        public decimal? TargetDepositoTahunBerjalan { get; set; }
        public DateTime? Periode { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
