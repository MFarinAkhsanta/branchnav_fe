﻿using System;
using System.Collections.Generic;

namespace PortalBranchNav.Models.dbBranchNav
{
    public partial class TblDataOutletSewa
    {
        public int Id { get; set; }
        public string KodeUnit { get; set; }
        public DateTime? Periode { get; set; }
        public string Alamat { get; set; }
        public string KodePos { get; set; }
        public DateTime? TanggalLive { get; set; }
        public DateTime? TanggalMulaiSewa { get; set; }
        public DateTime? TanggalAkhirSewa { get; set; }
        public decimal? TotalHargaSewa { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public string Nama { get; set; }
        public int? StatusHakMilik { get; set; }
    }
}
