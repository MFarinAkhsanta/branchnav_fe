﻿using System;
using System.Collections.Generic;

namespace PortalBranchNav.Models.dbBranchNav
{
    public partial class TblDataAuditcix
    {
        public int Id { get; set; }
        public string KodeOutlet { get; set; }
        public int? TriwulanId { get; set; }
        public string Triwulan { get; set; }
        public int? Tahun { get; set; }
        public decimal? TotalScore { get; set; }
        public string Yudisium { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
    }
}
