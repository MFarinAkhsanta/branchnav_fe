﻿using System;
using System.Collections.Generic;

namespace PortalBranchNav.Models.dbBranchNav
{
    public partial class TblDataKreditRealisasi
    {
        public int Id { get; set; }
        public string KodeCabang { get; set; }
        public string NamaUnit { get; set; }
        public decimal? RealisasiKreditBbmenengahTahunBerjalan { get; set; }
        public decimal? RealisasiPenyaluranKurtahunBerjalan { get; set; }
        public decimal? RealisasiKreditFlexiTahunBerjalan { get; set; }
        public decimal? RealisasiKreditGriyaTahunBerjalan { get; set; }
        public decimal? RealisasiKreditOthersTahunBerjalan { get; set; }
        public decimal? RealisasiKreditBbkecilTahunBerjalan { get; set; }
        public DateTime? Periode { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
