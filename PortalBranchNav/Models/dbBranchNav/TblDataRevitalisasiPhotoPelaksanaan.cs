﻿using System;
using System.Collections.Generic;

namespace PortalBranchNav.Models.dbBranchNav
{
    public partial class TblDataRevitalisasiPhotoPelaksanaan
    {
        public int Id { get; set; }
        public int? RevitalisasiId { get; set; }
        public string Path { get; set; }
        public string FileName { get; set; }
        public int? UploadedById { get; set; }
        public DateTime? UploadedTime { get; set; }
    }
}
