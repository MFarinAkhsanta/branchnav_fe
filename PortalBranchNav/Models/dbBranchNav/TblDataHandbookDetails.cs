﻿using System;
using System.Collections.Generic;

namespace PortalBranchNav.Models.dbBranchNav
{
    public partial class TblDataHandbookDetails
    {
        public int Id { get; set; }
        public int? HandbookId { get; set; }
        public string Judul { get; set; }
        public string ForKodeUnit { get; set; }
        public bool? ForAllUnit { get; set; }
        public string Deskripsi { get; set; }
        public string Keyword { get; set; }
        public int? OrderBy { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
    }
}
