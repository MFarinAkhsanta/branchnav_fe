﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PortalBranchNav.Models.dbBranchNav
{
    public partial class dbBranchNavContext : DbContext
    {
        public dbBranchNavContext()
        {
        }

        public dbBranchNavContext(DbContextOptions<dbBranchNavContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Navigation> Navigation { get; set; }
        public virtual DbSet<NavigationAssignment> NavigationAssignment { get; set; }
        public virtual DbSet<TblConfigApps> TblConfigApps { get; set; }
        public virtual DbSet<TblDataAuditcix> TblDataAuditcix { get; set; }
        public virtual DbSet<TblDataBro> TblDataBro { get; set; }
        public virtual DbSet<TblDataDpkRealisasi> TblDataDpkRealisasi { get; set; }
        public virtual DbSet<TblDataDpkTarget> TblDataDpkTarget { get; set; }
        public virtual DbSet<TblDataFaq> TblDataFaq { get; set; }
        public virtual DbSet<TblDataHandbook> TblDataHandbook { get; set; }
        public virtual DbSet<TblDataHandbookAttachment> TblDataHandbookAttachment { get; set; }
        public virtual DbSet<TblDataHandbookDetails> TblDataHandbookDetails { get; set; }
        public virtual DbSet<TblDataKreditRealisasi> TblDataKreditRealisasi { get; set; }
        public virtual DbSet<TblDataKreditTarget> TblDataKreditTarget { get; set; }
        public virtual DbSet<TblDataOutletSewa> TblDataOutletSewa { get; set; }
        public virtual DbSet<TblDataPendingAudit> TblDataPendingAudit { get; set; }
        public virtual DbSet<TblDataRatingYudisium> TblDataRatingYudisium { get; set; }
        public virtual DbSet<TblDataRevitalisasi> TblDataRevitalisasi { get; set; }
        public virtual DbSet<TblDataRevitalisasiDetails> TblDataRevitalisasiDetails { get; set; }
        public virtual DbSet<TblDataRevitalisasiPhoto> TblDataRevitalisasiPhoto { get; set; }
        public virtual DbSet<TblDataRevitalisasiPhotoPelaksanaan> TblDataRevitalisasiPhotoPelaksanaan { get; set; }
        public virtual DbSet<TblDataSei> TblDataSei { get; set; }
        public virtual DbSet<TblDataSewaAtm> TblDataSewaAtm { get; set; }
        public virtual DbSet<TblJabatanPegawai> TblJabatanPegawai { get; set; }
        public virtual DbSet<TblJwtRepoitory> TblJwtRepoitory { get; set; }
        public virtual DbSet<TblLogActivity> TblLogActivity { get; set; }
        public virtual DbSet<TblLogBook> TblLogBook { get; set; }
        public virtual DbSet<TblLookup> TblLookup { get; set; }
        public virtual DbSet<TblMappingKewenanganJabatan> TblMappingKewenanganJabatan { get; set; }
        public virtual DbSet<TblMasterBeranda> TblMasterBeranda { get; set; }
        public virtual DbSet<TblMasterIconMenu> TblMasterIconMenu { get; set; }
        public virtual DbSet<TblMasterJabatan> TblMasterJabatan { get; set; }
        public virtual DbSet<TblMasterMataUang> TblMasterMataUang { get; set; }
        public virtual DbSet<TblMasterRole> TblMasterRole { get; set; }
        public virtual DbSet<TblMasterSistem> TblMasterSistem { get; set; }
        public virtual DbSet<TblPegawai> TblPegawai { get; set; }
        public virtual DbSet<TblRedaksional> TblRedaksional { get; set; }
        public virtual DbSet<TblSystemParameter> TblSystemParameter { get; set; }
        public virtual DbSet<TblUnit> TblUnit { get; set; }
        public virtual DbSet<TblUnit1> TblUnit1 { get; set; }
        public virtual DbSet<TblUser> TblUser { get; set; }
        public virtual DbSet<TblUserLog> TblUserLog { get; set; }
        public virtual DbSet<TblUserSession> TblUserSession { get; set; }

        // Unable to generate entity type for table 'dbo.unit_sentra_temp'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.data_unit'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DataWilayah'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=176.9.160.211;Initial Catalog=dbBranchNav;User ID=sa;Password=passw0rd4SQL;multipleactiveresultsets=true");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Navigation>(entity =>
            {
                entity.HasIndex(e => new { e.Type, e.Visible })
                    .HasName("NonClusteredIndex-20200531-134520");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IconClass).HasMaxLength(100);

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ParentNavigationId).HasColumnName("ParentNavigation_Id");

                entity.Property(e => e.Route).HasMaxLength(255);

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");

                entity.HasOne(d => d.ParentNavigation)
                    .WithMany(p => p.InverseParentNavigation)
                    .HasForeignKey(d => d.ParentNavigationId)
                    .HasConstraintName("FK_dbo.Navigation_dbo.Navigation_ParentNavigation_Id");
            });

            modelBuilder.Entity<NavigationAssignment>(entity =>
            {
                entity.HasIndex(e => new { e.NavigationId, e.RoleId, e.IsActive, e.IsDelete })
                    .HasName("NonClusteredIndex-20200531-134541");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.NavigationId).HasColumnName("Navigation_Id");

                entity.Property(e => e.RoleId).HasColumnName("Role_Id");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");

                entity.HasOne(d => d.Navigation)
                    .WithMany(p => p.NavigationAssignment)
                    .HasForeignKey(d => d.NavigationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NavigationAssignment_Navigation");
            });

            modelBuilder.Entity<TblConfigApps>(entity =>
            {
                entity.ToTable("Tbl_Config_Apps");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MaxFileSize).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDataAuditcix>(entity =>
            {
                entity.ToTable("Tbl_Data_Auditcix");

                entity.HasIndex(e => new { e.KodeOutlet, e.TriwulanId, e.Tahun })
                    .HasName("NonClusteredIndex-20201024-072800");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.KodeOutlet).HasMaxLength(100);

                entity.Property(e => e.TotalScore).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Triwulan).HasMaxLength(50);

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");

                entity.Property(e => e.Yudisium).HasMaxLength(150);
            });

            modelBuilder.Entity<TblDataBro>(entity =>
            {
                entity.ToTable("Tbl_Data_BRO");

                entity.Property(e => e.BroGangguanBisnis)
                    .HasColumnName("BRO_GangguanBisnis")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BroKecuranganInternal)
                    .HasColumnName("BRO_KecuranganInternal")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BroKejahatan)
                    .HasColumnName("BRO_Kejahatan")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BroKerusakan)
                    .HasColumnName("BRO_Kerusakan")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BroLokasi)
                    .HasColumnName("BRO_Lokasi")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BroProduk)
                    .HasColumnName("BRO_Produk")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BroTransaksiDanDistribusi)
                    .HasColumnName("BRO_TransaksiDanDistribusi")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.KodeOutlet).HasMaxLength(100);

                entity.Property(e => e.NetBro)
                    .HasColumnName("Net_BRO")
                    .HasColumnType("decimal(30, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Periode).HasColumnType("date");

                entity.Property(e => e.RecoveryBroGangguanBisnis)
                    .HasColumnName("RecoveryBRO_GangguanBisnis")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.RecoveryBroKecuranganInternal)
                    .HasColumnName("RecoveryBRO_KecuranganInternal")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.RecoveryBroKejahatan)
                    .HasColumnName("RecoveryBRO_Kejahatan")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.RecoveryBroKerusakan)
                    .HasColumnName("RecoveryBRO_Kerusakan")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.RecoveryBroLokasi)
                    .HasColumnName("RecoveryBRO_Lokasi")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.RecoveryBroProduk)
                    .HasColumnName("RecoveryBRO_Produk")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.RecoveryBroTransaksiDanDistribusi)
                    .HasColumnName("RecoveryBRO_TransaksiDanDistribusi")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDataDpkRealisasi>(entity =>
            {
                entity.ToTable("Tbl_Data_DPK_Realisasi");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.KodeCabang)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NamaUnit)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Periode).HasColumnType("date");

                entity.Property(e => e.RealiasiGiro).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.RealisasiDeposito).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.RealisasiTabungan).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDataDpkTarget>(entity =>
            {
                entity.ToTable("Tbl_Data_DPK_Target");

                entity.Property(e => e.BaselineDeposito).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BaselineGiro).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BaselineTabungan).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.KodeCabang)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NamaUnit)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Periode).HasColumnType("date");

                entity.Property(e => e.TargetDepositoTahunBerjalan).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TargetGiroTahunBerjalan).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TargetTabunganTahunBerjalan).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDataFaq>(entity =>
            {
                entity.ToTable("Tbl_Data_FAQ");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.Kategori).HasMaxLength(50);

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDataHandbook>(entity =>
            {
                entity.ToTable("Tbl_Data_Handbook");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.Keterangan).HasColumnType("text");

                entity.Property(e => e.Kode).HasMaxLength(150);

                entity.Property(e => e.OrderBy).HasColumnName("Order_By");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDataHandbookAttachment>(entity =>
            {
                entity.ToTable("Tbl_Data_Handbook_Attachment");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.Size).HasMaxLength(50);

                entity.Property(e => e.TypeFile).HasMaxLength(50);

                entity.Property(e => e.UploadById).HasColumnName("UploadBy_Id");

                entity.Property(e => e.UploadTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDataHandbookDetails>(entity =>
            {
                entity.ToTable("Tbl_Data_Handbook_Details");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.Deskripsi).HasColumnType("text");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDataKreditRealisasi>(entity =>
            {
                entity.ToTable("Tbl_Data_Kredit_Realisasi");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.KodeCabang).HasMaxLength(50);

                entity.Property(e => e.NamaUnit)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Periode).HasColumnType("date");

                entity.Property(e => e.RealisasiKreditBbkecilTahunBerjalan)
                    .HasColumnName("RealisasiKreditBBKecilTahunBerjalan")
                    .HasColumnType("decimal(30, 2)");

                entity.Property(e => e.RealisasiKreditBbmenengahTahunBerjalan)
                    .HasColumnName("RealisasiKreditBBMenengahTahunBerjalan")
                    .HasColumnType("decimal(30, 2)");

                entity.Property(e => e.RealisasiKreditFlexiTahunBerjalan).HasColumnType("decimal(30, 2)");

                entity.Property(e => e.RealisasiKreditGriyaTahunBerjalan).HasColumnType("decimal(30, 2)");

                entity.Property(e => e.RealisasiKreditOthersTahunBerjalan).HasColumnType("decimal(30, 2)");

                entity.Property(e => e.RealisasiPenyaluranKurtahunBerjalan)
                    .HasColumnName("RealisasiPenyaluranKURTahunBerjalan")
                    .HasColumnType("decimal(30, 2)");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDataKreditTarget>(entity =>
            {
                entity.ToTable("Tbl_Data_Kredit_Target");

                entity.Property(e => e.BaselineKreditBbkecil)
                    .HasColumnName("BaselineKreditBBKecil")
                    .HasColumnType("decimal(30, 2)");

                entity.Property(e => e.BaselineKreditBbmenengah)
                    .HasColumnName("BaselineKreditBBMenengah")
                    .HasColumnType("decimal(30, 2)");

                entity.Property(e => e.BaselineKreditFlexi).HasColumnType("decimal(30, 2)");

                entity.Property(e => e.BaselineKreditGriya).HasColumnType("decimal(30, 2)");

                entity.Property(e => e.BaselineKreditOthers).HasColumnType("decimal(30, 2)");

                entity.Property(e => e.BaselinePenyaluranKur)
                    .HasColumnName("BaselinePenyaluranKUR")
                    .HasColumnType("decimal(30, 2)");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.KodeCabang).HasMaxLength(50);

                entity.Property(e => e.NamaUnit)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Periode).HasColumnType("date");

                entity.Property(e => e.TargetKreditBbkecilTahunBerjalan)
                    .HasColumnName("TargetKreditBBKecilTahunBerjalan")
                    .HasColumnType("decimal(30, 2)");

                entity.Property(e => e.TargetKreditBbmenengahTahunBerjalan)
                    .HasColumnName("TargetKreditBBMenengahTahunBerjalan")
                    .HasColumnType("decimal(30, 2)");

                entity.Property(e => e.TargetKreditFlexiTahunBerjalan).HasColumnType("decimal(30, 2)");

                entity.Property(e => e.TargetKreditGriyaTahunBerjalan).HasColumnType("decimal(30, 2)");

                entity.Property(e => e.TargetKreditOthersTahunBerjalan).HasColumnType("decimal(30, 2)");

                entity.Property(e => e.TargetPenyaluranKurtahunBerjalan)
                    .HasColumnName("TargetPenyaluranKURTahunBerjalan")
                    .HasColumnType("decimal(30, 2)");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDataOutletSewa>(entity =>
            {
                entity.ToTable("Tbl_Data_Outlet_Sewa");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.KodePos).HasMaxLength(50);

                entity.Property(e => e.KodeUnit).HasMaxLength(100);

                entity.Property(e => e.Periode).HasColumnType("date");

                entity.Property(e => e.StatusHakMilik).HasDefaultValueSql("((1))");

                entity.Property(e => e.TanggalAkhirSewa).HasColumnType("date");

                entity.Property(e => e.TanggalLive).HasColumnType("date");

                entity.Property(e => e.TanggalMulaiSewa).HasColumnType("date");

                entity.Property(e => e.TotalHargaSewa).HasColumnType("decimal(30, 0)");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDataPendingAudit>(entity =>
            {
                entity.ToTable("Tbl_Data_PendingAudit");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.JudulDokumen).IsUnicode(false);

                entity.Property(e => e.KodeOutlet).HasMaxLength(100);

                entity.Property(e => e.Periode).HasColumnType("date");

                entity.Property(e => e.Presentase).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDataRatingYudisium>(entity =>
            {
                entity.ToTable("Tbl_Data_Rating_Yudisium");

                entity.HasIndex(e => new { e.KodeOutlet, e.Periode, e.Tahun, e.IsDeleted, e.IsActive })
                    .HasName("NonClusteredIndex-20201024-145042");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.KodeOutlet).HasMaxLength(100);

                entity.Property(e => e.Periode).HasColumnType("date");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDataRevitalisasi>(entity =>
            {
                entity.ToTable("Tbl_Data_Revitalisasi");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.EstimasiBiaya).HasColumnType("decimal(30, 0)");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.KodeOutlet).HasMaxLength(100);

                entity.Property(e => e.Periode).HasColumnType("date");

                entity.Property(e => e.ProjectNo).HasMaxLength(100);

                entity.Property(e => e.StatusProject).IsUnicode(false);

                entity.Property(e => e.Uic)
                    .HasColumnName("UIC")
                    .HasMaxLength(250);

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDataRevitalisasiDetails>(entity =>
            {
                entity.ToTable("Tbl_Data_Revitalisasi_Details");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.Progress).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Tanggal).HasColumnType("date");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDataRevitalisasiPhoto>(entity =>
            {
                entity.ToTable("Tbl_Data_Revitalisasi_Photo");

                entity.Property(e => e.UploadedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDataRevitalisasiPhotoPelaksanaan>(entity =>
            {
                entity.ToTable("Tbl_Data_Revitalisasi_Photo_Pelaksanaan");

                entity.Property(e => e.UploadedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDataSei>(entity =>
            {
                entity.ToTable("Tbl_Data_SEI");

                entity.Property(e => e.CabangDimensi3).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.Dimensi1).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Dimensi2).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.KodeUnit).HasMaxLength(100);

                entity.Property(e => e.OutletDimensi3).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Periode).HasColumnType("date");

                entity.Property(e => e.ScoreSeicabang)
                    .HasColumnName("ScoreSEICabang")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ScoreSeioutlet)
                    .HasColumnName("ScoreSEIOutlet")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ScoreSeiwilayah)
                    .HasColumnName("ScoreSEIWilayah")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");

                entity.Property(e => e.WilayahDimensi3).HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<TblDataSewaAtm>(entity =>
            {
                entity.ToTable("Tbl_Data_Sewa_ATM");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.HargaTotalSewa).HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Idatm)
                    .HasColumnName("IDATM")
                    .HasMaxLength(100);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsInBranch).HasDefaultValueSql("((1))");

                entity.Property(e => e.KodePos).HasMaxLength(50);

                entity.Property(e => e.KodeUnit).HasMaxLength(100);

                entity.Property(e => e.TanggalAkhirSewa).HasColumnType("date");

                entity.Property(e => e.TanggalAktivasi).HasColumnType("date");

                entity.Property(e => e.TanggalMulaiSewa).HasColumnType("date");

                entity.Property(e => e.TipeAtm)
                    .HasColumnName("TipeATM")
                    .HasMaxLength(100);

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblJabatanPegawai>(entity =>
            {
                entity.ToTable("Tbl_Jabatan_Pegawai");

                entity.HasIndex(e => new { e.PegawaiId, e.JabatanId, e.UnitId, e.IsDeleted })
                    .HasName("NonClusteredIndex-20200531-134656");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateEnd).HasColumnType("datetime");

                entity.Property(e => e.DateStart).HasColumnType("datetime");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.JabatanId).HasColumnName("Jabatan_Id");

                entity.Property(e => e.PegawaiId).HasColumnName("Pegawai_Id");

                entity.Property(e => e.UnitId).HasColumnName("Unit_Id");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");

                entity.HasOne(d => d.Pegawai)
                    .WithMany(p => p.TblJabatanPegawai)
                    .HasForeignKey(d => d.PegawaiId)
                    .HasConstraintName("FK_Tbl_Role_Pegawai_Tbl_Pegawai");
            });

            modelBuilder.Entity<TblJwtRepoitory>(entity =>
            {
                entity.ToTable("Tbl_JWT_Repoitory");

                entity.Property(e => e.ClientIp).HasMaxLength(250);
            });

            modelBuilder.Entity<TblLogActivity>(entity =>
            {
                entity.ToTable("Tbl_LogActivity");

                entity.HasIndex(e => e.UserId)
                    .HasName("NonClusteredIndex-20200531-134556");

                entity.Property(e => e.ActionTime).HasColumnType("datetime");

                entity.Property(e => e.Ip)
                    .HasColumnName("IP")
                    .HasMaxLength(150);

                entity.Property(e => e.Npp).HasMaxLength(150);

                entity.Property(e => e.Os).HasColumnName("OS");
            });

            modelBuilder.Entity<TblLogBook>(entity =>
            {
                entity.ToTable("Tbl_Log_Book");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("Created_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Nik)
                    .HasColumnName("NIK")
                    .HasMaxLength(50);

                entity.Property(e => e.Npp).HasMaxLength(50);
            });

            modelBuilder.Entity<TblLookup>(entity =>
            {
                entity.ToTable("Tbl_Lookup");

                entity.HasIndex(e => new { e.Type, e.Value, e.OrderBy, e.IsDeleted, e.IsActive })
                    .HasName("NonClusteredIndex-20200531-134448");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.OrderBy).HasColumnName("Order_By");

                entity.Property(e => e.Type).HasMaxLength(50);

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblMappingKewenanganJabatan>(entity =>
            {
                entity.ToTable("Tbl_Mapping_Kewenangan_Jabatan");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblMasterBeranda>(entity =>
            {
                entity.ToTable("Tbl_Master_Beranda");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.Keterangan).HasColumnType("text");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");

                entity.Property(e => e.Value).HasColumnType("text");
            });

            modelBuilder.Entity<TblMasterIconMenu>(entity =>
            {
                entity.ToTable("Tbl_MasterIconMenu");

                entity.Property(e => e.CeratedById).HasColumnName("CeratedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Icon).HasMaxLength(150);

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((1))");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblMasterJabatan>(entity =>
            {
                entity.ToTable("Tbl_Master_Jabatan");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.Keterangan).HasColumnType("text");

                entity.Property(e => e.Kode).HasMaxLength(250);

                entity.Property(e => e.OrderBy).HasColumnName("Order_By");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblMasterMataUang>(entity =>
            {
                entity.ToTable("Tbl_Master_Mata_Uang");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.Kode).HasMaxLength(150);

                entity.Property(e => e.OrderBy).HasColumnName("Order_By");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblMasterRole>(entity =>
            {
                entity.ToTable("Tbl_Master_Role");

                entity.HasIndex(e => new { e.IsDeleted, e.IsActive })
                    .HasName("NonClusteredIndex-20200531-134611");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.Nama).HasMaxLength(150);

                entity.Property(e => e.OrderBy).HasColumnName("Order_By");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblMasterSistem>(entity =>
            {
                entity.ToTable("Tbl_Master_Sistem");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.Keterangan).HasColumnType("text");

                entity.Property(e => e.Kode).HasMaxLength(50);

                entity.Property(e => e.OrderBy).HasColumnName("Order_By");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblPegawai>(entity =>
            {
                entity.ToTable("Tbl_Pegawai");

                entity.HasIndex(e => new { e.UnitId, e.JabatanId, e.Npp, e.IsDeleted, e.Ldaplogin })
                    .HasName("NonClusteredIndex-20200531-134632");

                entity.Property(e => e.Alamat).HasMaxLength(150);

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("Created_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeleteById).HasColumnName("DeleteBy_Id");

                entity.Property(e => e.DeleteDate)
                    .HasColumnName("Delete_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.IdJenisKelamin).HasColumnName("Id_JenisKelamin");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.JabatanId).HasColumnName("Jabatan_Id");

                entity.Property(e => e.Lastlogin).HasColumnType("datetime");

                entity.Property(e => e.Ldaplogin).HasColumnName("LDAPLogin");

                entity.Property(e => e.Nama).HasMaxLength(300);

                entity.Property(e => e.NoHp)
                    .HasColumnName("No_HP")
                    .HasMaxLength(25);

                entity.Property(e => e.Npp).HasMaxLength(80);

                entity.Property(e => e.TanggalLahir)
                    .HasColumnName("Tanggal_Lahir")
                    .HasColumnType("date");

                entity.Property(e => e.TempatLahir)
                    .HasColumnName("Tempat_Lahir")
                    .HasMaxLength(150);

                entity.Property(e => e.UnitId).HasColumnName("Unit_Id");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("Updated_Date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<TblRedaksional>(entity =>
            {
                entity.ToTable("Tbl_Redaksional");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.RedaksionalBeranda)
                    .HasColumnName("Redaksional_Beranda")
                    .HasColumnType("text");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblSystemParameter>(entity =>
            {
                entity.ToTable("Tbl_SystemParameter");

                entity.HasIndex(e => new { e.IsActive, e.IsDeleted })
                    .HasName("NonClusteredIndex-20200531-134714");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.Keterangan).HasColumnType("text");

                entity.Property(e => e.Key).IsRequired();

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblUnit>(entity =>
            {
                entity.ToTable("Tbl_Unit");

                entity.Property(e => e.Code).HasMaxLength(100);

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Dati2).HasMaxLength(300);

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.DivisiId).HasColumnName("Divisi_Id");

                entity.Property(e => e.Email).HasMaxLength(150);

                entity.Property(e => e.IbuKota).HasMaxLength(300);

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsJabodetabek).HasMaxLength(300);

                entity.Property(e => e.IsPulauJawa).HasMaxLength(300);

                entity.Property(e => e.Kecamatan).HasMaxLength(300);

                entity.Property(e => e.Kelas).HasMaxLength(50);

                entity.Property(e => e.Kelurahan).HasMaxLength(300);

                entity.Property(e => e.KodeBi)
                    .HasColumnName("KodeBI")
                    .HasMaxLength(50);

                entity.Property(e => e.KodeEis)
                    .HasColumnName("KodeEIS")
                    .HasMaxLength(50);

                entity.Property(e => e.KodeOutlet).HasMaxLength(100);

                entity.Property(e => e.KodeParentUnit).HasMaxLength(100);

                entity.Property(e => e.KodeRubrikDiv).HasMaxLength(50);

                entity.Property(e => e.KodeRubrikMemo).HasMaxLength(50);

                entity.Property(e => e.KodeRubrikNotin).HasMaxLength(50);

                entity.Property(e => e.KodeWilayah).HasMaxLength(50);

                entity.Property(e => e.Latitude).HasMaxLength(100);

                entity.Property(e => e.Longitude).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.NameVerEis)
                    .HasColumnName("NameVerEIS")
                    .HasMaxLength(500);

                entity.Property(e => e.Npwp).HasColumnName("NPWP");

                entity.Property(e => e.ParentId).HasColumnName("Parent_Id");

                entity.Property(e => e.PostalCode).HasMaxLength(50);

                entity.Property(e => e.Province).HasMaxLength(250);

                entity.Property(e => e.Pulau).HasMaxLength(300);

                entity.Property(e => e.Rbb)
                    .HasColumnName("RBB")
                    .HasMaxLength(50);

                entity.Property(e => e.ShortName).HasMaxLength(50);

                entity.Property(e => e.StatusOutlet).HasMaxLength(50);

                entity.Property(e => e.Telepon).HasMaxLength(50);

                entity.Property(e => e.TypeInAlfabeth).HasMaxLength(5);

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");

                entity.Property(e => e.WilayahId).HasColumnName("Wilayah_Id");

                entity.Property(e => e.ZonaBi)
                    .HasColumnName("ZonaBI")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TblUnit1>(entity =>
            {
                entity.ToTable("Tbl_Unit_1");

                entity.HasIndex(e => new { e.ParentId, e.WilayahId, e.DivisiId, e.IsActive, e.IsDelete })
                    .HasName("NonClusteredIndex-20200531-134733");

                entity.Property(e => e.Code).HasMaxLength(100);

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Dati2).HasMaxLength(300);

                entity.Property(e => e.DeletedById).HasColumnName("DeletedBy_Id");

                entity.Property(e => e.DeletedTime).HasColumnType("datetime");

                entity.Property(e => e.DivisiId).HasColumnName("Divisi_Id");

                entity.Property(e => e.Email).HasMaxLength(150);

                entity.Property(e => e.IbuKota).HasMaxLength(300);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsJabodetabek).HasMaxLength(300);

                entity.Property(e => e.IsPulauJawa).HasMaxLength(300);

                entity.Property(e => e.Kecamatan).HasMaxLength(300);

                entity.Property(e => e.Kelas).HasMaxLength(50);

                entity.Property(e => e.Kelurahan).HasMaxLength(300);

                entity.Property(e => e.KodeBi)
                    .HasColumnName("KodeBI")
                    .HasMaxLength(50);

                entity.Property(e => e.KodeEis)
                    .HasColumnName("KodeEIS")
                    .HasMaxLength(50);

                entity.Property(e => e.KodeOutlet).HasMaxLength(100);

                entity.Property(e => e.KodeParentUnit).HasMaxLength(100);

                entity.Property(e => e.KodeRubrikDiv).HasMaxLength(50);

                entity.Property(e => e.KodeRubrikMemo).HasMaxLength(50);

                entity.Property(e => e.KodeRubrikNotin).HasMaxLength(50);

                entity.Property(e => e.KodeWilayah).HasMaxLength(50);

                entity.Property(e => e.Latitude).HasMaxLength(100);

                entity.Property(e => e.Longitude).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.NameVerEis)
                    .HasColumnName("NameVerEIS")
                    .HasMaxLength(500);

                entity.Property(e => e.Npwp).HasColumnName("NPWP");

                entity.Property(e => e.ParentId).HasColumnName("Parent_Id");

                entity.Property(e => e.PostalCode).HasMaxLength(50);

                entity.Property(e => e.Province).HasMaxLength(250);

                entity.Property(e => e.Pulau).HasMaxLength(300);

                entity.Property(e => e.Rbb)
                    .HasColumnName("RBB")
                    .HasMaxLength(50);

                entity.Property(e => e.ShortName).HasMaxLength(50);

                entity.Property(e => e.StatusOutlet).HasMaxLength(50);

                entity.Property(e => e.Telepon).HasMaxLength(50);

                entity.Property(e => e.TypeInAlfabeth).HasMaxLength(5);

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");

                entity.Property(e => e.WilayahId).HasColumnName("Wilayah_Id");

                entity.Property(e => e.ZonaBi)
                    .HasColumnName("ZonaBI")
                    .HasMaxLength(50);

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_dbo.Unit_dbo.Unit_Parent_Id");
            });

            modelBuilder.Entity<TblUser>(entity =>
            {
                entity.ToTable("Tbl_User");

                entity.HasIndex(e => new { e.Username, e.PegawaiId })
                    .HasName("NonClusteredIndex-20200531-134749");

                entity.Property(e => e.CreatedById).HasColumnName("CreatedBy_Id");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.LastLogin).HasColumnType("datetime");

                entity.Property(e => e.Password).HasMaxLength(50);

                entity.Property(e => e.PegawaiId).HasColumnName("Pegawai_Id");

                entity.Property(e => e.UpdatedById).HasColumnName("UpdatedBy_Id");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Pegawai)
                    .WithMany(p => p.TblUser)
                    .HasForeignKey(d => d.PegawaiId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_Tbl_Pegawai");
            });

            modelBuilder.Entity<TblUserLog>(entity =>
            {
                entity.ToTable("Tbl_UserLog");

                entity.Property(e => e.Browser).HasMaxLength(255);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Ipaddress)
                    .HasColumnName("IPAddress")
                    .HasMaxLength(20);

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.UserName).HasMaxLength(10);
            });

            modelBuilder.Entity<TblUserSession>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("Tbl_UserSession");

                entity.Property(e => e.UserId)
                    .HasColumnName("User_Id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Info).HasMaxLength(255);

                entity.Property(e => e.LastActive).HasColumnType("datetime");

                entity.Property(e => e.RoleId).HasColumnName("Role_Id");

                entity.Property(e => e.SessionId)
                    .IsRequired()
                    .HasColumnName("SessionID")
                    .HasMaxLength(50);

                entity.Property(e => e.UnitId).HasColumnName("Unit_Id");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.TblUserSession)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_Tbl_UserSession_Tbl_Master_Role");

                entity.HasOne(d => d.Unit)
                    .WithMany(p => p.TblUserSession)
                    .HasForeignKey(d => d.UnitId)
                    .HasConstraintName("FK_Tbl_UserSession_Tbl_Unit");

                entity.HasOne(d => d.User)
                    .WithOne(p => p.TblUserSession)
                    .HasForeignKey<TblUserSession>(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Tbl_UserSession_Tbl_User");
            });
        }
    }
}
