﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using PortalBranchNav.Component;
using PortalBranchNav.Models.dbBranchNav;
using PortalBranchNav.ViewModels;


namespace PortalBranchNav.Controllers
{
    public class DataRatingYudisiumController : Controller
    {
        private readonly dbBranchNavContext _context;
        private readonly IConfiguration _configuration;
        private readonly LastSessionLog lastSession;
        private readonly AccessSecurity accessSecurity;
        private IHostingEnvironment _env;

        public static List<DataRatingYudisium_ViewModels> ListDataRatingYudisiumTemp = new List<DataRatingYudisium_ViewModels>();

        public DataRatingYudisiumController(IHostingEnvironment env, IConfiguration config, dbBranchNavContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _configuration = config;
            lastSession = new LastSessionLog(accessor, context, config);
            accessSecurity = new AccessSecurity(accessor, context, config);
            _env = env;
        }



        public IActionResult Index()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            var location = new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}");
            string Path = location.AbsolutePath;

            if (!accessSecurity.IsGetAccess(".." + Path))
            {
                return RedirectToAction("NotAccess", "Error");
            }

            ViewBag.CurrentPath = Path;

            return View();
        }

        #region LoadData
        [HttpPost]
        public IActionResult LoadData()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

                var draw = dict["draw"];

                //Untuk mengetahui info paging dari datatable
                var start = dict["start"];
                var length = dict["length"];

                //Server side datatable hanya support untuk mendapatkan data mulai ke berapa, untuk mengirim row ke berapa
                //Kita perlu membuat logika sendiri
                var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;

                //Untuk mengetahui info order column datatable
                var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                var sortColumnDir = dict["order[0][dir]"];
                var PeriodeSearchParam = dict["columns[1][search][value]"];
                var NamaWilayahSearchParam = dict["columns[2][search][value]"];
                var NamaUnitSearchParam = dict["columns[4][search][value]"];

                //var TahunSearchParam = dict["columns[3][search][value]"];
                var RatingKerjaSearchParam = dict["columns[6][search][value]"];
                var RatingPengelolaanResikoSearchParam = dict["columns[7][search][value]"];
                var RatingGabunganSearchParam = dict["columns[8][search][value]"];

                //Untuk mengetahui info jumlah page dan total skip data
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                var data = new DataTableViewModel();

                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataRatingYudisium:get"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        pageNumber = pageNumber,
                        pageSize = pageSize,
                        sortColumn = sortColumn,
                        sortColumnDir = sortColumnDir,
                        namaWilayah = String.IsNullOrEmpty(NamaWilayahSearchParam) ? null : NamaWilayahSearchParam,
                        namaUnit = String.IsNullOrEmpty(NamaUnitSearchParam) ? null : NamaUnitSearchParam,
                        //kodeOutlet = String.IsNullOrEmpty(TriwulanSearchParam) ? null : TriwulanSearchParam,
                        periode = String.IsNullOrEmpty(PeriodeSearchParam) ? null : PeriodeSearchParam,
                        //tahun = String.IsNullOrEmpty(TahunPeriodeSearchParam) ? null : TahunPeriodeSearchParam,
                        ratingKerja = String.IsNullOrEmpty(RatingKerjaSearchParam) ? null : RatingKerjaSearchParam,
                        ratingPengelolaanResiko = String.IsNullOrEmpty(RatingPengelolaanResikoSearchParam) ? null : RatingPengelolaanResikoSearchParam,
                        ratingGabungan = String.IsNullOrEmpty(RatingGabunganSearchParam) ? null : RatingGabunganSearchParam
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    data = JsonConvert.DeserializeObject<DataTableViewModel>(result);
                }
                return Json(
                    new
                    {
                        draw = draw,
                        recordsFiltered = data.data == null ? 0 : data.data.recordTotals,
                        recordsTotal = data.data == null ? 0 : data.data.recordTotals,
                        data = data.data == null ? new List<object>() : data.data.data == null ? new List<object>() : data.data.data
                    }
                );
            }
            catch (Exception Ex)
            {
                throw;
            }
        }
        #endregion

        #region Load Data Temp Import
        public ActionResult LoadDataTemp()
        {

            return Json(new { data = ListDataRatingYudisiumTemp });
        }
        #endregion

        #region Import Data
        public ActionResult Import()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login");
            }
            ListDataRatingYudisiumTemp = new List<DataRatingYudisium_ViewModels>();


            return PartialView("_Import");
        }
        #endregion

        #region Import File
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ImportFile(ImportFile_ViewModels model)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Account");
            }

            try
            {
                if (model.file == null && model.file.Length == 0)
                {
                    return Content("Masukkan file terlebih dahulu");
                }

                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataRatingYudisium:upload"];
                (bool resultApi, string result) = RequestToAPI.PostFormDataToWebApi(url, model, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    var data = JsonConvert.DeserializeObject<ServiceResult<object>>(result);
                    if (data.Code == 1)
                        return Content("");
                    else
                        return Content("Error Code : " + data.Code + ", " + data.Data);
                }
                return Content(result);

                //List<TblDataRatingYudisium> ListTemp = new List<TblDataRatingYudisium>();


                //var RowErrorKe = 0;

                //try
                //{

                //    //Ambil ext file yang diijinkan
                //    TblSystemParameter AllowExtValue = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "AllowedFileImportType");
                //    TblSystemParameter MaxFileValue = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "MaxFileImportUploadSize");
                //    TblSystemParameter PathFolderFileImport = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "PathFolderFileImport");

                //    string AllowedFileUploadType = AllowExtValue.Value;
                //    string PathFolder = PathFolderFileImport.Value;
                //    decimal MaxFileUploadSize = decimal.Parse(MaxFileValue.Value);
                //    decimal SizeFile = model.file.Length / 1000000;

                //    string[] Periode = model.PeriodeBulanTahun.Split(" ");


                //    string Ext = Path.GetExtension(model.file.FileName);

                //    //Validate Upload
                //    if (!AllowedFileUploadType.Contains(Ext))
                //    {
                //        return Content(GetConfig.AppSetting["AppSettings:GlobalMessage:ImportDataTypeTidakSesuai"]);
                //    }


                //    string SubPath = PathFolder + "DataBRO/";

                //    if (!Directory.Exists(SubPath))
                //    {
                //        Directory.CreateDirectory(SubPath);
                //    }


                //    if (model.file.Length > 0)
                //    {
                //        string sFileExtension = Path.GetExtension(model.file.FileName).ToLower();
                //        ISheet sheet;
                //        string fullPath = Path.Combine(SubPath, model.file.FileName);
                //        using (var stream = new FileStream(fullPath, FileMode.Create))
                //        {
                //            model.file.CopyTo(stream);
                //            stream.Position = 0;
                //            if (sFileExtension == ".xls")
                //            {
                //                HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //This will read the Excel 97-2000 formats  
                //                sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook  
                //            }
                //            else
                //            {
                //                XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //This will read 2007 Excel format  
                //                sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook   
                //            }
                //        }


                //        using (TransactionScope trx = new TransactionScope())
                //        {

                //            for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                //            {
                //                RowErrorKe = i;
                //                IRow row = sheet.GetRow(i);
                //                if (row == null) continue;
                //                if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;

                //                TblDataRatingYudisium data = new TblDataRatingYudisium();

                //                data.KodeOutlet = row.GetCell(0).ToString();
                //                data.Periode = DateTime.Parse(model.PeriodeBulanTahun);
                //                data.Tahun = int.Parse(row.GetCell(2).ToString());
                //                data.RatingKinerja = row.GetCell(3).ToString();
                //                data.RatingPengelolaanResiko = row.GetCell(4).ToString();
                //                data.RatingGabungan = row.GetCell(5).ToString();
                //                ListTemp.Add(data);
                //            }

                //            string dataKodeOutlet = string.Join(";", ListTemp.Select(m => m.KodeOutlet).ToList());
                //            string[] dataKodeUnitArray = ListTemp.Select(m => m.KodeOutlet).ToList().ToArray();
                //            string query = "update dbo.[Tbl_Data_Rating_Yudisium] set IsActive = 0 where KodeOutlet in (select * from dbo.uf_SplitString('" + dataKodeOutlet + "',';')) and Tahun ='"+model.PeriodeTahun+"'";
                //            _context.Database.ExecuteSqlCommand(query);

                //            _context.TblDataRatingYudisium.AddRange(ListTemp);
                //            _context.SaveChanges();
                //            trx.Complete();
                //        }
                //    }
                //    return Content("");

                //}
                //catch (Exception Ex)
                //{
                //    return Content("Error Row ke -" + RowErrorKe + Ex.Message);
                //}
            }
            catch (Exception Ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion

        #region Get Templates
        public FileStreamResult GetTemplate()
        {
            string fileName = GetConfig.AppSetting["templateUpload:RatingYudisium"];
            if (string.IsNullOrWhiteSpace(_env.WebRootPath))
            {
                _env.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
            }
            var webRoot = _env.WebRootPath;
            string Folder = Path.Combine(webRoot, "templates");
            //string folderFile = Path.Combine(Folder, "product");
            string file = System.IO.Path.Combine(Folder, fileName);
            FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read);
            return File(fs, "application/vnd.ms-excel", fileName);
        }
        #endregion
    }
}
