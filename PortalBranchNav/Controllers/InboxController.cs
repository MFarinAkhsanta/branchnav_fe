﻿using DigiSalesWebPortal.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PortalBranchNav.Component;
using PortalBranchNav.Models.dbBranchNav;
using PortalBranchNav.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.Controllers
{
    public class InboxController : Controller
    {
        private readonly dbBranchNavContext _context;
        private readonly IConfiguration _configuration;
        private readonly LastSessionLog lastSession;
        private readonly AccessSecurity accessSecurity;
        private Utility Utility = new Utility();
        public InboxController(IConfiguration config, dbBranchNavContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _configuration = config;
            lastSession = new LastSessionLog(accessor, context, config);
            accessSecurity = new AccessSecurity(accessor, context, config);
        }

        public IActionResult Index()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            var location = new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}");
            string Path = location.AbsolutePath;

            //if (!accessSecurity.IsGetAccess(".." + Path))
            //{
            //    return RedirectToAction("NotAccess", "Error");
            //}

            ViewBag.CurrentPath = Path;

            return View();
        }

        #region LoadData
        [HttpPost]
        public IActionResult LoadData()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

                var draw = dict["draw"];

                //Untuk mengetahui info paging dari datatable
                var start = dict["start"];
                var length = dict["length"];

                //Server side datatable hanya support untuk mendapatkan data mulai ke berapa, untuk mengirim row ke berapa
                //Kita perlu membuat logika sendiri
                var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;

                //Untuk mengetahui info order column datatable
                var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                var sortColumnDir = dict["order[0][dir]"];
                var TypeSearchParam = dict["columns[2][search][value]"];
                var NamaSearchParam = dict["columns[3][search][value]"];
                var ParentSearchParam = dict["columns[4][search][value]"];
                var RoleSearchParam = dict["columns[7][search][value]"];


                //Untuk mengetahui info jumlah page dan total skip data
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var Role_Id = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Role_Id));
                var isCabang = int.Parse(HttpContext.Session.GetString(SessionConstan.isCabang));

                var data = new DataTableViewModel();
                ////connect to API
                if (isCabang == 0)
                {
                    var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:BNICloudReport:loadInboxPusat"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        pageNumber = pageNumber,
                        pageSize = pageSize,
                        sortColumn = sortColumn,
                        sortColumnDir = sortColumnDir
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));

                    if (resultApi && !string.IsNullOrEmpty(result))
                    {
                        data = JsonConvert.DeserializeObject<DataTableViewModel>(result);
                    }
                    return Json(
                        new
                        {
                            draw = draw,
                            recordsFiltered = data.data == null ? 0 : data.data.recordTotals,
                            recordsTotal = data.data == null ? 0 : data.data.recordTotals,
                            data = data.data == null ? new List<object>() : data.data.data == null ? new List<object>() : data.data.data
                        }
                    );
                } else
                {
                    var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:BNICloudReport:loadInboxCabang"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        pageNumber = pageNumber,
                        pageSize = pageSize,
                        sortColumn = sortColumn,
                        sortColumnDir = sortColumnDir
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));

                    if (resultApi && !string.IsNullOrEmpty(result))
                    {
                        data = JsonConvert.DeserializeObject<DataTableViewModel>(result);
                    }
                    return Json(
                        new
                        {
                            draw = draw,
                            recordsFiltered = data.data == null ? 0 : data.data.recordTotals,
                            recordsTotal = data.data == null ? 0 : data.data.recordTotals,
                            data = data.data == null ? new List<object>() : data.data.data == null ? new List<object>() : data.data.data
                        }
                    );
                }
                
                //bool resultApi = true;
                //string result = "{\"code\":1,\"message\":\"sukses\",\"data\":{\"recordTotals\":2,\"data\":[" +
                //    "{\"number\":1,\"id\":1,\"jenisLaporan\":\"XXX\",\"namaLaporan\":\"Laporan Cabang\",\"divisiPengguna\":\"JAL\",\"tanggalUpload\":\"19-11-2021\",\"diUploadOleh\":\"821179-Tika\",\"diDownloadOleh\":\"83213-Farin\",\"status\":\"Submitted\",\"keterangan\":\"Dokumen anda sudah diDownload\"}," +
                //    "{\"number\":2,\"id\":2,\"jenisLaporan\":\"XXX\",\"namaLaporan\":\"Laporan Cabang\",\"divisiPengguna\":\"JAL\",\"tanggalUpload\":\"19-11-2021\",\"diUploadOleh\":\"821179-Tika\",\"diDownloadOleh\":\"83213-Farin\",\"status\":\"Rejected\",\"keterangan\":\"Dokumen anda sudah diDownload\"}]}}";

                
            }
            catch (Exception Ex)
            {
                throw;
            }
        }
        #endregion

        //#region View
        //public ActionResult View(int id)
        //{
        //    var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:BNICloudReport:view"];
        //    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { id = id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
        //    var jsonResult = JsonConvert.DeserializeObject<ServiceResult<DataBNICLoud>>(result);

        //    return PartialView("_View", jsonResult.Data);
        //}
        //#endregion
    }
}
