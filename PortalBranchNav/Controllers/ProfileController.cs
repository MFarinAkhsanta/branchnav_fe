﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PortalBranchNav.Component;
using PortalBranchNav.Models;
using PortalBranchNav.ViewModels;
using PortalBranchNav.Models.dbBranchNav;
using System.Transactions;
using PortalBranchNav.Component;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Http;
using System.Net;
using Microsoft.Extensions.Configuration;
using UAParser;

namespace PortalBranchNav.Controllers
{
    public class ProfileController : Controller
    {
        private readonly dbBranchNavContext _context;
        private readonly IConfiguration _configuration;
        IHttpContextAccessor _accessor;
        private readonly LastSessionLog lastSession;

        public ProfileController(IConfiguration config, dbBranchNavContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _configuration = config;
            _accessor = accessor;
            lastSession = new LastSessionLog(accessor, context, config);
        }
        public IActionResult Edit()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }
           
            return View();
        }
        
    }
}
