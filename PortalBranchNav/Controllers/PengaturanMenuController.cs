﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MimeKit;
using Newtonsoft.Json;
using NPOI.SS.Formula.Functions;
using PortalBranchNav.Component;
using PortalBranchNav.Models.dbBranchNav;
using PortalBranchNav.ViewModels;


namespace PortalBranchNav.Controllers
{
    public class PengaturanMenuController : Controller
    {
        private readonly dbBranchNavContext _context;
        private readonly IConfiguration _configuration;
        private readonly LastSessionLog lastSession;
        private readonly AccessSecurity accessSecurity;
        private Utility Utility = new Utility();
        public PengaturanMenuController(IConfiguration config, dbBranchNavContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _configuration = config;
            lastSession = new LastSessionLog(accessor, context, config);
            accessSecurity = new AccessSecurity(accessor, context, config);
        }

        public IActionResult Index()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            var location = new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}");
            string Path = location.AbsolutePath;

            if (!accessSecurity.IsGetAccess(".." + Path))
            {
                return RedirectToAction("NotAccess", "Error");
            }

            ViewBag.CurrentPath = Path;

            return View();
        }

        #region LoadData
        [HttpPost]
        public IActionResult LoadData()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

                var draw = dict["draw"];

                //Untuk mengetahui info paging dari datatable
                var start = dict["start"];
                var length = dict["length"];

                //Server side datatable hanya support untuk mendapatkan data mulai ke berapa, untuk mengirim row ke berapa
                //Kita perlu membuat logika sendiri
                var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;

                //Untuk mengetahui info order column datatable
                var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                var sortColumnDir = dict["order[0][dir]"];
                var TypeSearchParam = dict["columns[2][search][value]"];
                var NamaSearchParam = dict["columns[3][search][value]"];
                var ParentSearchParam = dict["columns[4][search][value]"];
                var RoleSearchParam = dict["columns[7][search][value]"];


                //Untuk mengetahui info jumlah page dan total skip data
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var Role_Id = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Role_Id));

                var data = new DataTableViewModel();

                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterMenu:get"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        pageNumber = pageNumber,
                        pageSize = pageSize,
                        sortColumn = sortColumn,
                        sortColumnDir = sortColumnDir,
                        name = String.IsNullOrEmpty(NamaSearchParam) ? null : NamaSearchParam,
                        type = String.IsNullOrEmpty(TypeSearchParam) ? null : TypeSearchParam,
                        role = String.IsNullOrEmpty(RoleSearchParam) ? null : RoleSearchParam,
                        parent = String.IsNullOrEmpty(ParentSearchParam) ? null : ParentSearchParam
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    data = JsonConvert.DeserializeObject<DataTableViewModel>(result);
                }
                return Json(
                    new
                    {
                        draw = draw,
                        recordsFiltered = data.data == null ? 0 : data.data.recordTotals,
                        recordsTotal = data.data == null ? 0 : data.data.recordTotals,
                        data = data.data == null ? new List<object>() : data.data.data == null ? new List<object>() : data.data.data
                    }
                );
            }
            catch (Exception Ex)
            {
                throw;
            }
        }
        #endregion

        #region Create
        public ActionResult Create()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login");
            }

            ViewBag.TypeMenu = new SelectList(Utility.SelectLookup("TypeMenu", _context, HttpContext.Session.GetString(SessionConstan.jwt_Token)), "Value", "Name");
            ViewBag.RolePegawai = new SelectList(Utility.SelectDataMasterRole(_context, HttpContext.Session.GetString(SessionConstan.jwt_Token)), "id", "text");
            ViewBag.ParentMenu = new SelectList(Utility.SelectDataMenu(_context, HttpContext.Session.GetString(SessionConstan.jwt_Token)), "id", "text");

            return PartialView("_Create");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SubmitCreate(Navigation model, string Roles)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                if (!lastSession.Update())
                {
                    return RedirectToAction("Login", "Login", new { a = true });
                }

                if (Roles == null)
                {
                    return Content(GetConfig.AppSetting["AppSettings:PilihRolesNavigation:BelumPilihRoles"]);
                }
                string[] ArrayRoles = Roles.Split(',');
                List<NavigationAssignment> listnav = new List<NavigationAssignment>();
                foreach (var item in ArrayRoles)
                {
                    NavigationAssignment dataAssign = new NavigationAssignment();
                    dataAssign.NavigationId = model.Id;
                    dataAssign.RoleId = int.Parse(item);
                    dataAssign.CreatedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id));
                    dataAssign.CreatedTime = DateTime.Now;
                    dataAssign.IsActive = true;
                    listnav.Add(dataAssign);
                }
                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterMenu:create"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url,
                    new {
                        Id = model.Id,
                        Name = model.Name,
                        Type = model.Type,
                        ParentNavigationId = model.ParentNavigationId,
                        Route = model.Route,
                        IconClass = model.IconClass,
                        Order = model.Order,
                        Visible = model.Visible,
                        CreatedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id)),
                        CreatedTime = DateTime.Now,
                        NavigationAssignment = listnav
                        }
                    , HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Save");
            }
            catch (Exception)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }

        #endregion

        #region Edit
        public ActionResult Edit(int id)
        {
            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterMenu:getById"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { Id = id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResult = JsonConvert.DeserializeObject<ServiceResult<PengaturanMenu_ViewModels>>(result);
            ViewBag.ParentMenu = new SelectList(Utility.SelectDataMenu(_context, HttpContext.Session.GetString(SessionConstan.jwt_Token)), "id", "text", jsonResult.Data.ParentNavigationId);

            ViewBag.TypeMenu = new SelectList(Utility.SelectLookup("TypeMenu", _context, HttpContext.Session.GetString(SessionConstan.jwt_Token)), "Value", "Name", jsonResult.Data.Type);

            ViewBag.RolePegawai = new SelectList(Utility.SelectDataMasterRole(_context, HttpContext.Session.GetString(SessionConstan.jwt_Token)), "id", "text");

            //var dataAssignment = _context.NavigationAssignment.Where(m => m.NavigationId == jsonResult.Data.Id).Select(m => m.RoleId).ToList();
            //ViewBag.NavigationAssignment = String.Join(",", dataAssignment.ToArray());
            ViewBag.NavigationAssignment = jsonResult.Data.RoleId;
            return PartialView("_Edit", jsonResult.Data);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SubmitEdit(Navigation model, string Roles)
        {
            try
            {
                if (!lastSession.Update())
                {
                    return RedirectToAction("Login", "Login", new { a = true });
                }

                string[] ArrayRoles = Roles.Split(',');
                List<NavigationAssignment> listnav = new List<NavigationAssignment>();
                foreach (var item in ArrayRoles)
                {
                    NavigationAssignment dataAssign = new NavigationAssignment();
                    dataAssign.NavigationId = model.Id;
                    dataAssign.RoleId = int.Parse(item);
                    dataAssign.UpdatedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id));
                    dataAssign.UpdatedTime = DateTime.Now;
                    dataAssign.IsActive = true;
                    listnav.Add(dataAssign);
                }
                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterMenu:edit"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url,
                    new
                    {
                        Id = model.Id,
                        Name = model.Name,
                        Type = model.Type,
                        ParentNavigationId = model.ParentNavigationId,
                        Route = model.Route,
                        IconClass = model.IconClass,
                        Order = model.Order,
                        Visible = model.Visible,
                        UpdateById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id)),
                        UpdateTime = DateTime.Now,
                        NavigationAssignment = listnav
                    }
                    , HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Save");
            }
            catch(Exception ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion


        #region View
        public ActionResult View(int id)
        {
            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterMenu:getById"];
            var url2 = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:GetRoleByMenuId"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { Id = id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResult = JsonConvert.DeserializeObject<ServiceResult<PengaturanMenu_ViewModels>>(result);
            ViewBag.ParentMenu = new SelectList(Utility.SelectDataMenu(_context, HttpContext.Session.GetString(SessionConstan.jwt_Token)), "id", "text", jsonResult.Data.ParentNavigationId);

            ViewBag.TypeMenu = new SelectList(Utility.SelectLookup("TypeMenu", _context, HttpContext.Session.GetString(SessionConstan.jwt_Token)), "Value", "Name", jsonResult.Data.Type);

            ViewBag.RolePegawai = new SelectList(Utility.SelectDataMasterRole(_context, HttpContext.Session.GetString(SessionConstan.jwt_Token)), "id", "text");

            //(bool resultApi2, string result2) = RequestToAPI.PostRequestToWebApi(url, new { navigation_id = jsonResult.Data.Id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            //var jsonResult2 = JsonConvert.DeserializeObject<ServiceResult<PengaturanMenu_ViewModels>>(result2);
            //var dataAssignment = _context.NavigationAssignment.Where(m => m.NavigationId == jsonResult.Data.Id).Select(m => m.RoleId).ToList();
            ViewBag.NavigationAssignment = jsonResult.Data.RoleId;
            return PartialView("_View", jsonResult.Data);
        }

        #endregion

        #region Delete
        public ActionResult Delete(string Ids)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Account");
            }

            try
            {
                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterMenu:delete"];
                var idarray = Ids.Split(',').Select(int.Parse).ToList();
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url,
                new
                {
                    ids = idarray
                }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Delete");
            }
            catch
            {
                return Content("gagal");
            }
        }
        #endregion
    }
}
