﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using PortalBranchNav.Component;
using PortalBranchNav.Models.dbBranchNav;
using PortalBranchNav.ViewModels;


namespace PortalBranchNav.Controllers
{
    public class DataRevitalisasiController : Controller
    {
        private readonly dbBranchNavContext _context;
        private readonly IConfiguration _configuration;
        private readonly LastSessionLog lastSession;
        private readonly AccessSecurity accessSecurity;

        public static List<DataRevitalisasi_ViewModels> ListDataRevitalisasiTemp = new List<DataRevitalisasi_ViewModels>();

        public DataRevitalisasiController(IConfiguration config, dbBranchNavContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _configuration = config;
            lastSession = new LastSessionLog(accessor, context, config);
            accessSecurity = new AccessSecurity(accessor, context, config);
        }

        public IActionResult Index()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            var location = new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}");
            string Path = location.AbsolutePath;

            if (!accessSecurity.IsGetAccess(".." + Path))
            {
                return RedirectToAction("NotAccess", "Error");
            }

            ViewBag.CurrentPath = Path;

            return View();
        }

     

        #region LoadData
        [HttpPost]
        public IActionResult LoadData()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

                var draw = dict["draw"];

                //Untuk mengetahui info paging dari datatable
                var start = dict["start"];
                var length = dict["length"];

                //Server side datatable hanya support untuk mendapatkan data mulai ke berapa, untuk mengirim row ke berapa
                //Kita perlu membuat logika sendiri
                var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;

                //Untuk mengetahui info order column datatable
                var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                var sortColumnDir = dict["order[0][dir]"];
                var KodeOutletSearchParam = dict["columns[2][search][value]"];
                var NamaUnitSearchParam = dict["columns[3][search][value]"];
                var NamaProyekSearchParam = dict["columns[4][search][value]"];

                //Untuk mengetahui info jumlah page dan total skip data
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                var data = new DataTableViewModel();

                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataRevitalisasi:get"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        pageNumber = pageNumber,
                        pageSize = pageSize,
                        sortColumn = sortColumn,
                        sortColumnDir = sortColumnDir,
                        kodeOutlet = String.IsNullOrEmpty(KodeOutletSearchParam) ? null : KodeOutletSearchParam,
                        namaUnit = String.IsNullOrEmpty(NamaUnitSearchParam) ? null : NamaUnitSearchParam,
                        namaProyek = String.IsNullOrEmpty(NamaProyekSearchParam) ? null : NamaProyekSearchParam
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    data = JsonConvert.DeserializeObject<DataTableViewModel>(result);
                }
                return Json(
                    new
                    {
                        draw = draw,
                        recordsFiltered = data.data == null ? 0 : data.data.recordTotals,
                        recordsTotal = data.data == null ? 0 : data.data.recordTotals,
                        data = data.data == null ? new List<object>() : data.data.data == null ? new List<object>() : data.data.data
                    }
                );
            }
            catch (Exception Ex)
            {
                throw;
            }
        }
        #endregion

        #region Load Data Temp Import
        public ActionResult LoadDataTemp()
        {

            return Json(new { data = ListDataRevitalisasiTemp });
        }
        #endregion

        #region LoadDataPhoto
        [HttpPost]
        public IActionResult LoadDataPhoto(int Id)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

                var draw = dict["draw"];

                //Untuk mengetahui info paging dari datatable
                var start = dict["start"];
                var length = dict["length"];

                //Server side datatable hanya support untuk mendapatkan data mulai ke berapa, untuk mengirim row ke berapa
                //Kita perlu membuat logika sendiri
                var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;

                //Untuk mengetahui info order column datatable
                var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                var sortColumnDir = dict["order[0][dir]"];
                var FileNameSearchParam = dict["columns[3][search][value]"];
                var UploaderSearchParam = dict["columns[4][search][value]"];

                //Untuk mengetahui info jumlah page dan total skip data
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                #region Not Used
                //List<DataRevitalisasiPhoto_ViewModels> list = new List<DataRevitalisasiPhoto_ViewModels>();

                //list = StoredProcedureExecutor.ExecuteSPList<DataRevitalisasiPhoto_ViewModels>(_context, "sp_Data_Revitalisasi_Photo_View", new SqlParameter[]{
                //        new SqlParameter("@FileName", FileNameSearchParam),
                //        new SqlParameter("@IdRevitalisasi", Id),
                //        new SqlParameter("@Uploader", UploaderSearchParam),
                //        new SqlParameter("@sortColumn", sortColumn),
                //        new SqlParameter("@sortColumnDir", sortColumnDir),
                //        new SqlParameter("@PageNumber", pageNumber),
                //        new SqlParameter("@RowsPage", pageSize)
                //});

                //recordsTotal = StoredProcedureExecutor.ExecuteScalarInt(_context, "sp_Data_Revitalisasi_Photo_Count", new SqlParameter[]{
                //       new SqlParameter("@FileName", FileNameSearchParam),
                //       new SqlParameter("@IdRevitalisasi", Id),
                //       new SqlParameter("@Uploader", UploaderSearchParam)
                //});

                //if (list == null)
                //{
                //    list = new List<DataRevitalisasiPhoto_ViewModels>();
                //    recordsTotal = 0;
                //}
                //return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = list });
                #endregion

                var data = new DataTableViewModel();

                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataRevitalisasi:getPhoto"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        pageNumber = pageNumber,
                        pageSize = pageSize,
                        sortColumn = sortColumn,
                        sortColumnDir = sortColumnDir,
                        fileName = String.IsNullOrEmpty(FileNameSearchParam) ? null : FileNameSearchParam,
                        uploader = String.IsNullOrEmpty(UploaderSearchParam) ? null : UploaderSearchParam,
                        idRevitalisasi = Id
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    data = JsonConvert.DeserializeObject<DataTableViewModel>(result);
                }
                return Json(
                    new
                    {
                        draw = draw,
                        recordsFiltered = data.data == null ? 0 : data.data.recordTotals,
                        recordsTotal = data.data == null ? 0 : data.data.recordTotals,
                        data = data.data == null ? new List<object>() : data.data.data == null ? new List<object>() : data.data.data
                    }
                );
            }
            catch (Exception Ex)
            {
                throw;
            }
        }
        #endregion

        #region LoadDataDesain
        [HttpPost]
        public IActionResult LoadDataDesain(int Id)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

                var draw = dict["draw"];

                //Untuk mengetahui info paging dari datatable
                var start = dict["start"];
                var length = dict["length"];

                //Server side datatable hanya support untuk mendapatkan data mulai ke berapa, untuk mengirim row ke berapa
                //Kita perlu membuat logika sendiri
                var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;

                //Untuk mengetahui info order column datatable
                var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                var sortColumnDir = dict["order[0][dir]"];
                var FileNameSearchParam = dict["columns[3][search][value]"];
                var UploaderSearchParam = dict["columns[4][search][value]"];

                //Untuk mengetahui info jumlah page dan total skip data
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                #region Not Used
                //List<DataRevitalisasiPhoto_ViewModels> list = new List<DataRevitalisasiPhoto_ViewModels>();

                //list = StoredProcedureExecutor.ExecuteSPList<DataRevitalisasiPhoto_ViewModels>(_context, "sp_Data_Revitalisasi_Desain_View", new SqlParameter[]{
                //        new SqlParameter("@FileName", FileNameSearchParam),
                //        new SqlParameter("@IdRevitalisasi", Id),
                //        new SqlParameter("@Uploader", UploaderSearchParam),
                //        new SqlParameter("@sortColumn", sortColumn),
                //        new SqlParameter("@sortColumnDir", sortColumnDir),
                //        new SqlParameter("@PageNumber", pageNumber),
                //        new SqlParameter("@RowsPage", pageSize)
                //});

                //recordsTotal = StoredProcedureExecutor.ExecuteScalarInt(_context, "sp_Data_Revitalisasi_Desain_Count", new SqlParameter[]{
                //       new SqlParameter("@FileName", FileNameSearchParam),
                //       new SqlParameter("@IdRevitalisasi", Id),
                //       new SqlParameter("@Uploader", UploaderSearchParam)
                //});

                //if (list == null)
                //{
                //    list = new List<DataRevitalisasiPhoto_ViewModels>();
                //    recordsTotal = 0;
                //}
                //return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = list });
                #endregion

                var data = new DataTableViewModel();

                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataRevitalisasi:getPelaksanaan"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        pageNumber = pageNumber,
                        pageSize = pageSize,
                        sortColumn = sortColumn,
                        sortColumnDir = sortColumnDir,
                        fileName = String.IsNullOrEmpty(FileNameSearchParam) ? null : FileNameSearchParam,
                        uploader = String.IsNullOrEmpty(UploaderSearchParam) ? null : UploaderSearchParam,
                        idRevitalisasi = Id
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    data = JsonConvert.DeserializeObject<DataTableViewModel>(result);
                }
                return Json(
                    new
                    {
                        draw = draw,
                        recordsFiltered = data.data == null ? 0 : data.data.recordTotals,
                        recordsTotal = data.data == null ? 0 : data.data.recordTotals,
                        data = data.data == null ? new List<object>() : data.data.data == null ? new List<object>() : data.data.data
                    }
                );

            }
            catch (Exception Ex)
            {
                throw;
            }
        }
        #endregion

        #region Import Data
        public ActionResult Import()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login");
            }
            ListDataRevitalisasiTemp = new List<DataRevitalisasi_ViewModels>();


            return PartialView("_Import");
        }
        #endregion

        #region Create
        public async Task<ActionResult> Create()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login");
            }

            var data = new DataTableViewModel<PengaturanLookup_API_ViewModels>();
            //connect to API
            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Lookup:get"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                url,
                new
                {
                    pageNumber = 1,
                    pageSize = GetConfig.AppSetting["Parameter:SizeLookup"],
                    sortColumn = "id",
                    sortColumnDir = "desc"
                },
                HttpContext.Session.GetString(SessionConstan.jwt_Token));
            if (resultApi && !string.IsNullOrEmpty(result))
            {
                data = JsonConvert.DeserializeObject<DataTableViewModel<PengaturanLookup_API_ViewModels>>(result);
            }
            var listJenisProyek = data.data.data.Where(x => x.type == "JenisProyek" && x.isActive == true).ToList();
            var listStatusProyek = data.data.data.Where(x => x.type == "StatusProyek" && x.isActive == true).ToList();

            ViewBag.jenisProyek = new SelectList(listJenisProyek, "order_By", "name");
            ViewBag.statusProject = new SelectList(listStatusProyek, "order_By", "name");


            var urlGetAllUnit = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:getAllUnit"];
            (bool resultApiGetAllUnit, string resultGetAllUnit) = RequestToAPI.PostRequestToWebApi(urlGetAllUnit, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResultGetAllUnit = JsonConvert.DeserializeObject<DataTableViewModel<TblUnit>>(resultGetAllUnit);

            var ListUnit = jsonResultGetAllUnit.data.data.Where(x => x.IsActive == true && x.IsDeleted == false).ToList();
            ViewBag.ListUnit = new SelectList(ListUnit, "Id", "Name");

            return PartialView("_Create");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> SubmitCreate(cekRevitalisasi_ViewModel model)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                if (!lastSession.Update())
                {
                    return RedirectToAction("Login", "Login", new { a = true });
                }

                #region Not Used
                //using (TransactionScope trx = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                //{
                //    model.CreatedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id));
                //    int.TryParse(model.JenisProyek, out int result);
                //    var JenisProyekString = await _context.TblLookup.Where(x => x.Value == result
                //                                && x.Type == GetConfig.AppSetting["AppSettings:dataLookup:JenisProyek"]).Select(x => x.Name).LastOrDefaultAsync().ConfigureAwait(false);
                //    model.JenisProyek = JenisProyekString;
                //    model.CreatedTime = DateTime.Now;
                //    model.IsDeleted = false;
                //    model.Periode = DateTime.Parse(model.PeriodeString);
                //    TblDataRevitalisasi saveData = new TblDataRevitalisasi
                //    {
                //        CreatedById = model.CreatedById,
                //        CreatedTime = model.CreatedTime,
                //        EstimasiBiaya = model.EstimasiBiaya,
                //        IsActive = model.IsActive,
                //        IsDeleted = false,
                //        JenisProyek = model.JenisProyek,
                //        KodeOutlet = model.KodeOutlet,
                //        NamaProyek = model.NamaProyek,
                //        NoDokumen = model.NoDokumen,
                //        Periode = model.Periode,
                //        ProjectNo = model.ProjectNo,
                //        StatusProject = model.StatusProject,
                //        Uic = model.Uic,
                //        UnitId = model.UnitId
                //    };
                //    _context.TblDataRevitalisasi.Add(saveData);
                //    _context.SaveChanges();

                //    trx.Complete();
                //}
                #endregion

                model.CreatedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id));
                int.TryParse(model.JenisProyek, out int resultParse);
                var JenisProyekString = await _context.TblLookup.Where(x => x.Value == resultParse
                                            && x.Type == GetConfig.AppSetting["AppSettings:dataLookup:JenisProyek"]).Select(x => x.Name).LastOrDefaultAsync().ConfigureAwait(false);
                model.KodeOutlet = await _context.TblUnit.Where(x => x.Id == model.UnitId).Select(x => x.KodeOutlet).LastOrDefaultAsync().ConfigureAwait(false);
                model.JenisProyek = JenisProyekString;
                model.CreatedTime = DateTime.Now;
                model.IsDeleted = false;
                model.EstimasiBiaya = Convert.ToDecimal(model.EstimasiBiayaString.ToString().Replace(".", ""));
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataRevitalisasi:create"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Save");
            }
            catch (Exception Ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }

        #endregion

        #region Edit
        public async Task<ActionResult> Edit(int id)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });

            }
            //TblDataRevitalisasi data = _context.TblDataRevitalisasi.Where(m => m.Id == id).FirstOrDefault();
            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataRevitalisasi:getById"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { Id = id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResult = JsonConvert.DeserializeObject<ServiceResult<TblDataRevitalisasi>>(result);

            var dataGetLookup = new DataTableViewModel<PengaturanLookup_API_ViewModels>();
            //connect to API
            var urlGetLookup = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Lookup:get"];
            (bool resultApiGetLookup, string resultGetLookup) = RequestToAPI.PostRequestToWebApi(
                urlGetLookup,
                new
                {
                    pageNumber = 1,
                    pageSize = GetConfig.AppSetting["Parameter:SizeLookup"],
                    sortColumn = "id",
                    sortColumnDir = "desc"
                },
                HttpContext.Session.GetString(SessionConstan.jwt_Token));
            if (resultApiGetLookup && !string.IsNullOrEmpty(resultGetLookup))
            {
                dataGetLookup = JsonConvert.DeserializeObject<DataTableViewModel<PengaturanLookup_API_ViewModels>>(resultGetLookup);
            }

            cekRevitalisasi_ViewModel output = new cekRevitalisasi_ViewModel();

            if (jsonResult.Data == null)
            {
                jsonResult.Data = new TblDataRevitalisasi();

                //ViewBag.jenisProyek = new SelectList(Utility.SelectLookup("JenisProyek", _context), "OrderBy", "Name");
                //ViewBag.statusProject = new SelectList(Utility.SelectLookup("StatusProyek", _context), "OrderBy", "Name");

                var listJenisProyek = dataGetLookup.data.data.Where(x => x.type == "JenisProyek" && x.isActive == true).ToList();
                var listStatusProyek = dataGetLookup.data.data.Where(x => x.type == "StatusProyek" && x.isActive == true).ToList();

                ViewBag.jenisProyek = new SelectList(listJenisProyek, "order_By", "name");
                ViewBag.statusProject = new SelectList(listStatusProyek, "order_By", "name");

                var urlGetAllUnit = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:getAllUnit"];
                (bool resultApiGetAllUnit, string resultGetAllUnit) = RequestToAPI.PostRequestToWebApi(urlGetAllUnit, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                var jsonResultGetAllUnit = JsonConvert.DeserializeObject<DataTableViewModel<TblUnit>>(resultGetAllUnit);

                var ListUnit = jsonResultGetAllUnit.data.data.Where(x => x.IsActive == true && x.IsDeleted == false).ToList();
                ViewBag.ListUnit = new SelectList(ListUnit, "Id", "Name");

            }
            else
            {
                //var JenisProyekId = await _context.TblLookup.Where(x => x.Name == jsonResult.Data.JenisProyek 
                //                        && x.Type == GetConfig.AppSetting["AppSettings:dataLookup:JenisProyek"]).Select(x => x.Value).LastOrDefaultAsync();
                //jsonResult.Data.JenisProyek = JenisProyekId.ToString();
                //ViewBag.jenisProyek = new SelectList(Utility.SelectLookup("JenisProyek", _context), "OrderBy", "Name", jsonResult.Data.JenisProyek);
                //ViewBag.statusProject = new SelectList(Utility.SelectLookup("StatusProyek", _context), "OrderBy", "Name", jsonResult.Data.StatusProject);

                var JenisProyekId = dataGetLookup.data.data.Where(x => x.name == jsonResult.Data.JenisProyek
                                        && x.type == GetConfig.AppSetting["AppSettings:dataLookup:JenisProyek"]).Select(x => x.value).LastOrDefault();
                jsonResult.Data.JenisProyek = JenisProyekId.ToString();
                var listJenisProyek = dataGetLookup.data.data.Where(x => x.type == "JenisProyek" && x.isActive == true).ToList();
                var listStatusProyek = dataGetLookup.data.data.Where(x => x.type == "StatusProyek" && x.isActive == true).ToList();

                ViewBag.jenisProyek = new SelectList(listJenisProyek, "order_By", "name", jsonResult.Data.JenisProyek);
                ViewBag.statusProject = new SelectList(listStatusProyek, "order_By", "name", jsonResult.Data.StatusProject);

                var urlGetAllUnit = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:getAllUnit"];
                (bool resultApiGetAllUnit, string resultGetAllUnit) = RequestToAPI.PostRequestToWebApi(urlGetAllUnit, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                var jsonResultGetAllUnit = JsonConvert.DeserializeObject<DataTableViewModel<TblUnit>>(resultGetAllUnit);

                var ListUnit = jsonResultGetAllUnit.data.data.Where(x => x.IsActive == true && x.IsDeleted == false).ToList();
                ViewBag.ListUnit = new SelectList(ListUnit, "Id", "Name", jsonResult.Data.UnitId);
                output = new cekRevitalisasi_ViewModel
                {
                    CreatedById = jsonResult.Data.CreatedById,
                    CreatedTime = jsonResult.Data.CreatedTime,
                    EstimasiBiaya = jsonResult.Data.EstimasiBiaya,
                    EstimasiBiayaString = jsonResult.Data.EstimasiBiaya.ToString(),
                    IsActive = jsonResult.Data.IsActive,
                    IsDeleted = false,
                    JenisProyek = jsonResult.Data.JenisProyek,
                    KodeOutlet = jsonResult.Data.KodeOutlet,
                    NamaProyek = jsonResult.Data.NamaProyek,
                    NoDokumen = jsonResult.Data.NoDokumen,
                    Periode = jsonResult.Data.Periode,
                    PeriodeString = Convert.ToDateTime(jsonResult.Data.Periode).ToString("MMM yyyy", CultureInfo.InvariantCulture),
                    ProjectNo = jsonResult.Data.ProjectNo,
                    StatusProject = jsonResult.Data.StatusProject,
                    Uic = jsonResult.Data.Uic,
                    UnitId = jsonResult.Data.UnitId
                };
            }

            return PartialView("_Edit", output);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> SubmitEdit(cekRevitalisasi_ViewModel model)
        {
            try
            {
                if (!lastSession.Update())
                {
                    return RedirectToAction("Login", "Login", new { a = true });
                }

                #region Not Used
                //using (TransactionScope trx = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                //{
                //    int.TryParse(model.JenisProyek, out int result);
                //    var JenisProyekString = await _context.TblLookup.Where(x => x.Value == result
                //                                && x.Type == GetConfig.AppSetting["AppSettings:dataLookup:JenisProyek"]).Select(x => x.Name).LastOrDefaultAsync().ConfigureAwait(false);
                //    model.JenisProyek = JenisProyekString;
                //    model.UpdatedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id));
                //    model.UpdatedTime = DateTime.Now;
                //    model.IsDeleted = false;
                //    model.Periode = DateTime.Parse(model.PeriodeString);
                //    TblDataRevitalisasi saveData = new TblDataRevitalisasi()
                //    {
                //        Id = model.Id,
                //        CreatedById = model.CreatedById,
                //        CreatedTime = model.CreatedTime,
                //        UpdatedById = model.UpdatedById,
                //        UpdatedTime = model.UpdatedTime,
                //        EstimasiBiaya = model.EstimasiBiaya,
                //        IsActive = model.IsActive,
                //        IsDeleted = false,
                //        JenisProyek = model.JenisProyek,
                //        KodeOutlet = model.KodeOutlet,
                //        NamaProyek = model.NamaProyek,
                //        NoDokumen = model.NoDokumen,
                //        Periode = model.Periode,
                //        ProjectNo = model.ProjectNo,
                //        StatusProject = model.StatusProject,
                //        Uic = model.Uic,
                //        UnitId = model.UnitId
                //    };

                //    _context.Entry(saveData).State = EntityState.Modified;
                //    _context.SaveChanges();

                //    trx.Complete();
                //}
                #endregion

                int.TryParse(model.JenisProyek, out int resultParse);
                var JenisProyekString = await _context.TblLookup.Where(x => x.Value == resultParse
                                            && x.Type == GetConfig.AppSetting["AppSettings:dataLookup:JenisProyek"]).Select(x => x.Name).LastOrDefaultAsync().ConfigureAwait(false);
                model.JenisProyek = JenisProyekString;
                model.UpdatedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id));
                model.UpdatedTime = DateTime.Now;
                model.IsDeleted = false;
                model.Periode = DateTime.Parse(model.PeriodeString);
                model.KodeOutlet = await _context.TblUnit.Where(x => x.Id == model.UnitId).Select(x => x.KodeOutlet).LastOrDefaultAsync().ConfigureAwait(false);
                model.EstimasiBiaya = Convert.ToDecimal(model.EstimasiBiayaString.ToString().Replace(".", ""));
                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataRevitalisasi:edit"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Save");
            }
            catch(Exception ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion

        #region View
        public async Task<ActionResult> View(int id)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });

            }

            #region Not Used
            //TblDataRevitalisasi data = _context.TblDataRevitalisasi.Where(m => m.Id == id).FirstOrDefault();
            //if (data == null)
            //{
            //    data = new TblDataRevitalisasi();

            //    ViewBag.jenisProyek = new SelectList(Utility.SelectLookup("JenisProyek", _context), "OrderBy", "Name");
            //    ViewBag.statusProject = new SelectList(Utility.SelectLookup("StatusProyek", _context), "OrderBy", "Name");

            //    var ListUnit = await _context.TblUnit.Where(x => x.IsActive == true && x.IsDeleted == false).ToListAsync();
            //    ViewBag.ListUnit = new SelectList(ListUnit, "Id", "Name");
            //}
            //else
            //{
            //    var JenisProyekId = await _context.TblLookup.Where(x => x.Name == data.JenisProyek
            //                            && x.Type == GetConfig.AppSetting["AppSettings:dataLookup:JenisProyek"]).Select(x => x.Value).LastOrDefaultAsync();
            //    data.JenisProyek = JenisProyekId.ToString();
            //    ViewBag.jenisProyek = new SelectList(Utility.SelectLookup("JenisProyek", _context), "OrderBy", "Name", data.JenisProyek);
            //    ViewBag.statusProject = new SelectList(Utility.SelectLookup("StatusProyek", _context), "OrderBy", "Name", data.StatusProject);

            //    var ListUnit = await _context.TblUnit.Where(x => x.IsActive == true && x.IsDeleted == false).ToListAsync();
            //    ViewBag.ListUnit = new SelectList(ListUnit, "Id", "Name", data.UnitId);
            //}
            #endregion

            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataRevitalisasi:getById"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { Id = id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResult = JsonConvert.DeserializeObject<ServiceResult<cekRevitalisasi_ViewModel>>(result);

            #region Not Used
            ////var JenisProyekId = await _context.TblLookup.Where(x => x.Name == jsonResult.Data.JenisProyek
            ////            && x.Type == GetConfig.AppSetting["AppSettings:dataLookup:JenisProyek"]).Select(x => x.Value).LastOrDefaultAsync();
            ////jsonResult.Data.JenisProyek = JenisProyekId.ToString();
            //jsonResult.Data.JenisProyek = jsonResult.Data.JenisProyekId.ToString();

            //ViewBag.jenisProyek = new SelectList(Utility.SelectLookup("JenisProyek", _context), "OrderBy", "Name", jsonResult.Data.JenisProyek);
            //ViewBag.statusProject = new SelectList(Utility.SelectLookup("StatusProyek", _context), "OrderBy", "Name", jsonResult.Data.StatusProject);

            //var ListUnit = await _context.TblUnit.Where(x => x.IsActive == true && x.IsDeleted == false).ToListAsync();
            //ViewBag.ListUnit = new SelectList(ListUnit, "Id", "Name", jsonResult.Data.UnitId);
            //jsonResult.Data.PeriodeString = Convert.ToDateTime(jsonResult.Data.Periode).ToString("MMM yyyy", CultureInfo.InvariantCulture);
            #endregion

            
            var dataGetLookup = new DataTableViewModel<PengaturanLookup_API_ViewModels>();
            //connect to API
            var urlGetLookup = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Lookup:get"];
            (bool resultApiGetLookup, string resultGetLookup) = RequestToAPI.PostRequestToWebApi(
                urlGetLookup,
                new
                {
                    pageNumber = 1,
                    pageSize = GetConfig.AppSetting["Parameter:SizeLookup"],
                    sortColumn = "id",
                    sortColumnDir = "desc"
                },
                HttpContext.Session.GetString(SessionConstan.jwt_Token));
            if (resultApiGetLookup && !string.IsNullOrEmpty(resultGetLookup))
            {
                dataGetLookup = JsonConvert.DeserializeObject<DataTableViewModel<PengaturanLookup_API_ViewModels>>(resultGetLookup);
            }
            var JenisProyekId = dataGetLookup.data.data.Where(x => x.name == jsonResult.Data.JenisProyek
                        && x.type == GetConfig.AppSetting["AppSettings:dataLookup:JenisProyek"]).Select(x => x.value).LastOrDefault();
            jsonResult.Data.JenisProyek = JenisProyekId.ToString();
            jsonResult.Data.EstimasiBiayaString = jsonResult.Data.EstimasiBiaya.ToString();
            //jsonResult.Data.JenisProyek = jsonResult.Data.JenisProyekId.ToString();

            var listJenisProyek = dataGetLookup.data.data.Where(x => x.type == "JenisProyek" && x.isActive == true).ToList();
            var listStatusProyek = dataGetLookup.data.data.Where(x => x.type == "StatusProyek" && x.isActive == true).ToList();

            ViewBag.jenisProyek = new SelectList(listJenisProyek, "order_By", "name", jsonResult.Data.JenisProyek);
            ViewBag.statusProject = new SelectList(listStatusProyek, "order_By", "name", jsonResult.Data.StatusProject);


            var urlGetAllUnit = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:getAllUnit"];
            (bool resultApiGetAllUnit, string resultGetAllUnit) = RequestToAPI.PostRequestToWebApi(urlGetAllUnit, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResultGetAllUnit = JsonConvert.DeserializeObject<DataTableViewModel<TblUnit>>(resultGetAllUnit);

            var ListUnit = jsonResultGetAllUnit.data.data.Where(x => x.IsActive == true && x.IsDeleted == false).ToList();
            ViewBag.ListUnit = new SelectList(ListUnit, "Id", "Name", jsonResult.Data.UnitId);
            jsonResult.Data.PeriodeString = Convert.ToDateTime(jsonResult.Data.Periode).ToString("MMM yyyy", CultureInfo.InvariantCulture);

            return PartialView("_View", jsonResult.Data);
            
            //return PartialView("_View", data);
        }

        #endregion

        #region Delete
        public async Task<ActionResult> Delete(int Id)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                #region Not Used
                //int[] confirmedDeleteId = Ids.Split(',').Select(int.Parse).ToArray();

                //List<TblDataAuditcix> Transaksis = _context.TblDataAuditcix.Where(x => confirmedDeleteId.Contains(x.Id)).ToList(); //Ambil data sesuai dengan ID
                //for (int i = 0; i < confirmedDeleteId.Length; i++)
                //{
                //    TblDataAuditcix data = _context.TblDataAuditcix.Find(Transaksis[i].Id);
                //    data.IsDeleted = true; //Jika true data tidak akan ditampilkan dan data masih tersimpan di dalam database
                //    data.DeletedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id));
                //    //data.DeletedTime = System.DateTime.Now;
                //    _context.Entry(data).State = EntityState.Modified;
                //    _context.SaveChanges();
                //}

                //TblDataRevitalisasi getData = await _context.TblDataRevitalisasi.Where(x => x.Id == Id
                //                                && x.IsActive == true && x.IsDeleted == false).LastOrDefaultAsync();
                //if(getData != null)
                //{
                //    getData.IsDeleted = true;
                //    getData.DeletedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id));
                //    getData.DeletedTime = DateTime.Now;

                //    _context.Entry(getData).State = EntityState.Modified;
                //    await _context.SaveChangesAsync();
                //}
                //else
                //{
                //    return Content("Idnull");
                //}
                #endregion

                string Ids = Id.ToString();

                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataRevitalisasi:delete"];
                var idarray = Ids.Split(',').Select(int.Parse).ToList();
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url,
                new
                {
                    ids = idarray
                }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Delete");
            }
            catch
            {
                return Content("gagal");
            }
        }
        #endregion

        #region EditDetailData
        public async Task<ActionResult> EditDetailData(int Id)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login");
            }

            //var getDataDetails = await _context.TblDataRevitalisasiDetails.Where(x => x.DataRevitilisasiId == Id
            //                            && x.IsActive == true && x.IsDeleted == false).LastOrDefaultAsync();

            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataRevitalisasi:getByIdDetailRevitalisasi"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { revitalisasiId = Id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var getDataDetail = JsonConvert.DeserializeObject<ServiceResult<TblDataRevitalisasiDetails>>(result);

            //var urlGetPhoto = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:GetAllDataPhotoByRevitalisasiId"];
            //(bool resultApiGetPhoto, string resultGetPhoto) = RequestToAPI.PostRequestToWebApi(urlGetPhoto, new { id = Id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            //var getDataPhoto = JsonConvert.DeserializeObject<ServiceResult<TblDataRevitalisasiPhoto>>(resultGetPhoto);

            ////var getDataPhoto = await _context.TblDataRevitalisasiPhoto.Where(x => x.RevitalisasiId == Id).ToListAsync();

            //var urlGetDesain = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:GetAllDataPhotoPelaksanaanByRevitalisasiId"];
            //(bool resultApiGetDesain, string resultGetDesain) = RequestToAPI.PostRequestToWebApi(urlGetDesain, new { id = Id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            //var getDataDesain = JsonConvert.DeserializeObject<ServiceResult<TblDataRevitalisasiPhotoPelaksanaan>>(resultGetDesain);

            ////var getDataDesain = await _context.TblDataRevitalisasiPhotoPelaksanaan.Where(x => x.RevitalisasiId == Id).ToListAsync();

            DataRevitalisasiDetails_ViewModels dataDetail = new DataRevitalisasiDetails_ViewModels();
            if (getDataDetail.Data != null)
            {
                dataDetail = new DataRevitalisasiDetails_ViewModels()
                {
                    Id = getDataDetail.Data.Id,
                    Catatan = getDataDetail.Data.Catatan,
                    CreatedById = getDataDetail.Data.CreatedById,
                    CreatedTime = getDataDetail.Data.CreatedTime,
                    DataRevitilisasiId = getDataDetail.Data.DataRevitilisasiId,
                    DeletedById = getDataDetail.Data.DeletedById,
                    DeletedTime = getDataDetail.Data.DeletedTime,
                    IsActive = getDataDetail.Data.IsActive,
                    IsDeleted = getDataDetail.Data.IsDeleted,
                    Progress = getDataDetail.Data.Progress,
                    ProgressString = Convert.ToString(getDataDetail.Data.Progress),
                    PegawaiId = getDataDetail.Data.PegawaiId,
                    Tanggal = getDataDetail.Data.Tanggal,
                    TanggalString = getDataDetail.Data.Tanggal == null ? "" : Convert.ToDateTime(getDataDetail.Data.Tanggal).ToString("MMM yyyy", CultureInfo.InvariantCulture),
                    UnitId = getDataDetail.Data.UnitId,
                    UpdatedById = getDataDetail.Data.UpdatedById,
                    UpdatedTime = getDataDetail.Data.UpdatedTime
                };

                var dataGetAllPegawai = new DataTableViewModel<TblPegawai>();
                //connect to API
                var urlGetAllPegawai = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:GetAllPegawai"];
                (bool resultApiGetAllPegawai, string resultGetAllPegawai) = RequestToAPI.PostRequestToWebApi(
                    urlGetAllPegawai,
                    new
                    {
                        pageNumber = 1,
                        pageSize = GetConfig.AppSetting["Parameter:SizePegawai"],
                        sortColumn = "id",
                        sortColumnDir = "desc"
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApiGetAllPegawai && !string.IsNullOrEmpty(resultGetAllPegawai))
                {
                    dataGetAllPegawai = JsonConvert.DeserializeObject<DataTableViewModel<TblPegawai>>(resultGetAllPegawai);
                }
                //var ListDataPegawai = dataGetAllPegawai.data.data.Where(x => x.IsActive == true && x.IsDeleted == false).ToList();
                ViewBag.dataPegawai = new SelectList(dataGetAllPegawai.data.data, "Id", "Nama", getDataDetail.Data.PegawaiId);

                var urlGetAllUnit = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:getAllUnit"];
                (bool resultApiGetAllUnit, string resultGetAllUnit) = RequestToAPI.PostRequestToWebApi(urlGetAllUnit, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                var jsonResultGetAllUnit = JsonConvert.DeserializeObject<DataTableViewModel<TblUnit>>(resultGetAllUnit);
                //List<TblUnit> ListDataUnit = await _context.TblUnit.Where(x => x.IsActive == true && x.IsDeleted == false).ToListAsync();
                ViewBag.dataUnit = new SelectList(jsonResultGetAllUnit.data.data, "Id", "Name", getDataDetail.Data.UnitId);
            }
            else
            {
                var dataGetAllPegawai = new DataTableViewModel<TblPegawai>();
                //connect to API
                var urlGetAllPegawai = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:GetAllPegawai"];
                (bool resultApiGetAllPegawai, string resultGetAllPegawai) = RequestToAPI.PostRequestToWebApi(
                    urlGetAllPegawai,
                    new
                    {
                        pageNumber = 1,
                        pageSize = GetConfig.AppSetting["Parameter:SizePegawai"],
                        sortColumn = "id",
                        sortColumnDir = "desc"
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApiGetAllPegawai && !string.IsNullOrEmpty(resultGetAllPegawai))
                {
                    dataGetAllPegawai = JsonConvert.DeserializeObject<DataTableViewModel<TblPegawai>>(resultGetAllPegawai);
                }
                //var ListDataPegawai = dataGetAllPegawai.data.data.Where(x => x.IsActive == true && x.IsDeleted == false).ToList();
                ViewBag.dataPegawai = new SelectList(dataGetAllPegawai.data.data, "Id", "Nama", getDataDetail.Data.PegawaiId);

                //var ListDataPegawai = await _context.TblPegawai.Where(x => x.IsActive == true && x.IsDeleted == false).ToListAsync();
                //ViewBag.dataPegawai = new SelectList(ListDataPegawai, "Id", "Nama");

                var urlGetAllUnit = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:getAllUnit"];
                (bool resultApiGetAllUnit, string resultGetAllUnit) = RequestToAPI.PostRequestToWebApi(urlGetAllUnit, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                var jsonResultGetAllUnit = JsonConvert.DeserializeObject<DataTableViewModel<TblUnit>>(resultGetAllUnit);
                //List<TblUnit> ListDataUnit = await _context.TblUnit.Where(x => x.IsActive == true && x.IsDeleted == false).ToListAsync();
                ViewBag.dataUnit = new SelectList(jsonResultGetAllUnit.data.data, "Id", "Name");
            }

            #region Not Used
            //var urlPhoto = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataRevitalisasi:getPhoto"];
            //(bool resultApiPhoto, string resultPhoto) = RequestToAPI.PostRequestToWebApi(
            //    url,
            //    new
            //    {
            //        pageNumber = pageNumber,
            //        pageSize = pageSize,
            //        sortColumn = sortColumn,
            //        sortColumnDir = sortColumnDir,
            //        fileName = String.IsNullOrEmpty(FileNameSearchParam) ? null : FileNameSearchParam,
            //        uploader = String.IsNullOrEmpty(UploaderSearchParam) ? null : UploaderSearchParam,
            //        idRevitalisasi = Id
            //    },
            //    HttpContext.Session.GetString(SessionConstan.jwt_Token));
            //if (resultApi && !string.IsNullOrEmpty(result))
            //{
            //    data = JsonConvert.DeserializeObject<DataTableViewModel>(result);
            //}

            //List<DataRevitalisasiPhoto_ViewModels> dataListPhoto = new List<DataRevitalisasiPhoto_ViewModels>();
            //if (getDataPhoto.Count != 0)
            //{
            //    foreach (var dataPhoto in getDataPhoto)
            //    {
            //        DataRevitalisasiPhoto_ViewModels Photo = new DataRevitalisasiPhoto_ViewModels()
            //        {
            //            Id = dataPhoto.Id,
            //            FileName = dataPhoto.FileName,
            //            Path = dataPhoto.Path,
            //            RevitalisasiId = dataPhoto.RevitalisasiId,
            //            UploadedById = dataPhoto.UploadedById,
            //            UploadedTime = dataPhoto.UploadedTime,
            //            UploadedTimeString = dataPhoto.UploadedTime == null ? "" : dataPhoto.UploadedTime.Value.Date.ToString()
            //        };

            //        dataListPhoto.Add(Photo);
            //    };
            //}

            //List<DataRevitalisasiPhoto_ViewModels> dataListDesain = new List<DataRevitalisasiPhoto_ViewModels>();
            //if (getDataDesain.Count != 0)
            //{
            //    foreach (var dataDesain in getDataDesain)
            //    {
            //        DataRevitalisasiPhoto_ViewModels Desain = new DataRevitalisasiPhoto_ViewModels()
            //        {
            //            Id = dataDesain.Id,
            //            FileName = dataDesain.FileName,
            //            Path = dataDesain.Path,
            //            RevitalisasiId = dataDesain.RevitalisasiId,
            //            UploadedById = dataDesain.UploadedById,
            //            UploadedTime = dataDesain.UploadedTime,
            //            UploadedTimeString = dataDesain.UploadedTime == null ? "" : dataDesain.UploadedTime.Value.Date.ToString()
            //        };

            //        dataListDesain.Add(Desain);
            //    };
            //}
            #endregion

            var getDataRevDetail = new DataRevitalisasiEditDataDetails_ViewModels() {
                Id = Id,
                Detail = dataDetail,
                //Photo = dataListPhoto,
                //Desain = dataListDesain
            };

            #region Not Used
            //var ListDataPegawai = await _context.TblPegawai.Where(x => x.IsActive == true && x.IsDeleted == false).ToListAsync();
            //ViewBag.dataPegawai = new SelectList(ListDataPegawai, "Id", "Nama", getDataDetails.PegawaiId);

            //var ListDatajabatan = await _context.TblJabatanPegawai.Where(x => x.PegawaiId == getDataDetails.PegawaiId && x.IsDeleted == false).ToListAsync();
            //List<TblUnit> ListDataUnit = await _context.TblUnit.Where(x => x.IsActive == true && x.IsDeleted == false).ToListAsync();
            //foreach (var dataJabatan in ListDatajabatan)
            //{
            //    var dataUnit = await _context.TblUnit.Where(x => x.Id == dataJabatan.UnitId 
            //                    && x.IsActive == true && x.IsDeleted == false).LastOrDefaultAsync();

            //}
            //ViewBag.dataUnit = new SelectList(ListDataUnit, "Id", "Name", getDataDetails.UnitId);
            #endregion

            return PartialView("_EditDetailData", getDataRevDetail);
        }
        #endregion

        #region GetUnit
        public async Task<ActionResult> getUnit(string selectedPegawaiString, int selectedPegawaiId, int DataRevitalisasiId)
        {
            List<TblUnit> listUnit = new List<TblUnit>();
            TblDataRevitalisasiDetails getDataDetails = new TblDataRevitalisasiDetails();
            string listUnitString = "";
            try
            {
                #region Not Used
                //if (selectedPegawaiId != 0)
                //{
                //    var getListUnitIdPegawai = await _context.TblJabatanPegawai.Where(x => x.PegawaiId == selectedPegawaiId).Select(x => x.UnitId).Distinct().ToListAsync();

                //    bool cekRedundanUnitId = false;
                //    foreach (var dataIdUnit in getListUnitIdPegawai)
                //    {
                //        TblUnit unit = await _context.TblUnit.Where(x => x.Id == dataIdUnit
                //                        && x.IsActive == true && x.IsDeleted == false).LastOrDefaultAsync();

                //        if (unit != null)
                //        {
                //            foreach(var data in listUnit)
                //            {
                //                if(data.Id == unit.Id)
                //                {
                //                    cekRedundanUnitId = true;
                //                }
                //            }

                //            if (cekRedundanUnitId == false)
                //            {
                //                listUnitString = listUnitString + unit.Id.ToString() + ",";
                //                listUnit.Add(unit);
                //            }
                //            cekRedundanUnitId = false;
                //        }
                //        unit = new TblUnit();
                //    }
                //    getDataDetails = await _context.TblDataRevitalisasiDetails.Where(x => x.DataRevitilisasiId == DataRevitalisasiId).LastOrDefaultAsync();
                //}


                //var outputUnit = Utility.SelectDataUnit2(listUnitString, _context);

                //var outputUnit_Count = outputUnit.Count();

                ////if (getDataDetails != null)
                ////{
                ////    int cekUnit = listUnit.Where(x => x.Id == getDataDetails.UnitId).Select(x => x.Id).LastOrDefault();
                ////    if (cekUnit != 0)
                ////    {
                ////        ViewBag.dataUnit = new SelectList(listUnit, "Id", "Name", getDataDetails.UnitId);
                ////    }
                ////    else
                ////    {
                ////        ViewBag.dataUnit = new SelectList(listUnit, "Id", "Name");
                ////    }
                ////}
                ////else
                ////{
                ////    ViewBag.dataUnit = new SelectList(listUnit, "Id", "Name");
                ////}

                ////return Content("");
                #endregion

                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:GetUnitByPegawaiId"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { pegawai_Id = selectedPegawaiId }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                var jsonResult = JsonConvert.DeserializeObject<DataTableViewModel<DataDropdownServerSide>>(result);

                var outputUnit = jsonResult.data.data;
                var outputUnit_Count = jsonResult.data.recordTotals;

                return Json(new { dataUnit = listUnit, items = outputUnit, total_count = outputUnit_Count });
            }
            catch(Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region Import File Photo
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> ImportFilePhoto(DataRevitalisasiEditDataDetails_ViewModels model)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Account");
            }

            try
            {
                if (model.FileDetailPhoto == null && model.FileDetailPhoto.Length == 0)
                {
                    return Content("Masukkan file terlebih dahulu");
                }

                
                //ListDataRevitalisasiTemp = new List<DataRevitalisasi_ViewModels>();


                //var RowErrorKe = 0;

                try
                {
                    model.RevitalisasiId = model.Id;
                    model.file = model.FileDetailPhoto;
                    sendUploadPhoto_ViewModels sendToApi = new sendUploadPhoto_ViewModels
                    {
                        RevitalisasiId = model.RevitalisasiId,
                        file = model.file,
                        Catatan = model.CatatanDesain
                    };
                    var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataRevitalisasi:uploadPhoto"];
                    (bool resultApi, string result) = RequestToAPI.PostFormDataToWebApi(url, sendToApi, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                    if (resultApi && !string.IsNullOrEmpty(result))
                    {
                        var data = JsonConvert.DeserializeObject<ServiceResult<object>>(result);
                        if (data.Code == 1)
                            return Content("");
                        else
                            return Content(data.Code + ": " + data.Message);
                    }
                    return Content(result);

                    #region Not Used
                    //    //Ambil ext file yang diijinkan
                    //    TblSystemParameter AllowExtValue = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "AllowedFileImportType");
                    //    TblSystemParameter MaxFileValue = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "MaxFileImportUploadSize");
                    //    TblSystemParameter PathFolderFileImport = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "PathFolderFileImport");

                    //    string AllowedFileUploadType = AllowExtValue.Value;
                    //    string PathFolder = PathFolderFileImport.Value;
                    //    decimal MaxFileUploadSize = decimal.Parse(MaxFileValue.Value);
                    //    decimal SizeFile = model.FileDetailPhoto.Length / 1000000;


                    //    string Ext = Path.GetExtension(model.FileDetailPhoto.FileName);

                    //    //Validate Upload
                    //    if (!AllowedFileUploadType.Contains(Ext))
                    //    {
                    //        return Content(GetConfig.AppSetting["AppSettings:GlobalMessage:ImportDataTypeTidakSesuai"]);
                    //    }


                    //    string SubPath = PathFolder + "DataRevitalisasi/Photo/";

                    //    if (!Directory.Exists(SubPath))
                    //    {
                    //        Directory.CreateDirectory(SubPath);
                    //    }


                    //    if (model.FileDetailPhoto.Length > 0)
                    //    {
                    //        //string sFileExtension = Path.GetExtension(model.FileDetailPhoto.FileName).ToLower();
                    //        //ISheet sheet;
                    //        string fullPath = Path.Combine(SubPath, model.FileDetailPhoto.FileName);
                    //        using (var stream = new FileStream(fullPath, FileMode.Create))
                    //        {
                    //            model.FileDetailPhoto.CopyTo(stream);
                    //            //stream.Position = 0;
                    //            //if (sFileExtension == ".xls")
                    //            //{
                    //            //    HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //This will read the Excel 97-2000 formats  
                    //            //    sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook  
                    //            //}
                    //            //else
                    //            //{
                    //            //    XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //This will read 2007 Excel format  
                    //            //    sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook   
                    //            //}
                    //        }


                    //        using (TransactionScope trx = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                    //        {
                    //            TblDataRevitalisasiPhoto dataPhoto = new TblDataRevitalisasiPhoto
                    //            {
                    //                FileName = model.FileDetailPhoto.FileName,
                    //                Path = "DataRevitalisasi/Photo/"+ model.FileDetailPhoto.FileName,
                    //                RevitalisasiId = model.Id,
                    //                UploadedById = Convert.ToInt32(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id)),
                    //                UploadedTime = DateTime.Now
                    //            };

                    //            await _context.TblDataRevitalisasiPhoto.AddAsync(dataPhoto).ConfigureAwait(false);
                    //            await _context.SaveChangesAsync().ConfigureAwait(false);

                    //            //for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
                    //            //{
                    //            //    RowErrorKe = i;
                    //            //    IRow row = sheet.GetRow(i);
                    //            //    if (row == null) continue;
                    //            //    if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;

                    //            //    DataRevitalisasiPhoto_ViewModels data = new DataRevitalisasiPhoto_ViewModels();

                    //            //    data.FileName = model.FileDetailPhoto.FileName;
                    //            //    data.Path = row.GetCell(1).ToString();
                    //            //    data.NamaProyek = row.GetCell(2).ToString();
                    //            //    data.JenisProyek = row.GetCell(3).ToString();
                    //            //    data.EstimasiBiaya = decimal.Parse(row.GetCell(4).ToString());
                    //            //    //data.StatusProgress = row.GetCell(5).ToString();
                    //            //    //data.DetailProgress = row.GetCell(6).ToString();
                    //            //    data.NoDokumen = row.GetCell(7).ToString();
                    //            //    data.UIC = row.GetCell(8).ToString();
                    //            //    ListDataRevitalisasiTemp.Add(data);
                    //            //}
                    //            trx.Complete();
                    //        }
                    //    }
                    //    return Content("");
                    #endregion
                }
                catch (Exception Ex)
                {
                    return Content(Ex.Message);
                }


            }
            catch (Exception Ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion

        #region Import File Desain
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> ImportFileDesain(DataRevitalisasiEditDataDetails_ViewModels model)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Account");
            }

            try
            {
                if (model.FileDetailDesain == null && model.FileDetailDesain.Length == 0)
                {
                    return Content("Masukkan file terlebih dahulu");
                }

                ListDataRevitalisasiTemp = new List<DataRevitalisasi_ViewModels>();
                
                try
                {
                    model.RevitalisasiId = model.Id;
                    model.file = model.FileDetailDesain;
                    sendUploadPhoto_ViewModels sendToApi = new sendUploadPhoto_ViewModels
                    {
                        RevitalisasiId = model.RevitalisasiId,
                        file = model.file,
                        Catatan = model.CatatanPelaksanaan
                    };
                    var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataRevitalisasi:uploadPelaksanaan"];
                    (bool resultApi, string result) = RequestToAPI.PostFormDataToWebApi(url, sendToApi, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                    if (resultApi && !string.IsNullOrEmpty(result))
                    {
                        var data = JsonConvert.DeserializeObject<ServiceResult<object>>(result);
                        if (data.Code == 1)
                            return Content("");
                        else
                            return Content(data.Code + ": " + data.Message);
                    }
                    return Content(result);

                    #region Not Used
                    ////Ambil ext file yang diijinkan
                    //TblSystemParameter AllowExtValue = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "AllowedFileImportType");
                    //TblSystemParameter MaxFileValue = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "MaxFileImportUploadSize");
                    //TblSystemParameter PathFolderFileImport = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "PathFolderFileImport");

                    //string AllowedFileUploadType = AllowExtValue.Value;
                    //string PathFolder = PathFolderFileImport.Value;
                    //decimal MaxFileUploadSize = decimal.Parse(MaxFileValue.Value);
                    //decimal SizeFile = model.FileDetailDesain.Length / 1000000;


                    //string Ext = Path.GetExtension(model.FileDetailDesain.FileName);

                    ////Validate Upload
                    //if (!AllowedFileUploadType.Contains(Ext))
                    //{
                    //    return Content(GetConfig.AppSetting["AppSettings:GlobalMessage:ImportDataTypeTidakSesuai"]);
                    //}


                    //string SubPath = PathFolder + "DataRevitalisasi/Desain";

                    //if (!Directory.Exists(SubPath))
                    //{
                    //    Directory.CreateDirectory(SubPath);
                    //}


                    //if (model.FileDetailDesain.Length > 0)
                    //{
                    //    //string sFileExtension = Path.GetExtension(model.FileDetailPhoto.FileName).ToLower();
                    //    //ISheet sheet;
                    //    string fullPath = Path.Combine(SubPath, model.FileDetailDesain.FileName);
                    //    using (var stream = new FileStream(fullPath, FileMode.Create))
                    //    {
                    //        model.FileDetailDesain.CopyTo(stream);
                    //        //stream.Position = 0;
                    //        //if (sFileExtension == ".xls")
                    //        //{
                    //        //    HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //This will read the Excel 97-2000 formats  
                    //        //    sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook  
                    //        //}
                    //        //else
                    //        //{
                    //        //    XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //This will read 2007 Excel format  
                    //        //    sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook   
                    //        //}
                    //    }


                    //    using (TransactionScope trx = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                    //    {
                    //        TblDataRevitalisasiPhotoPelaksanaan dataDesain = new TblDataRevitalisasiPhotoPelaksanaan
                    //        {
                    //            FileName = model.FileDetailDesain.FileName,
                    //            Path = "DataRevitalisasi/Desain/" + model.FileDetailDesain.FileName,
                    //            RevitalisasiId = model.Id,
                    //            UploadedById = Convert.ToInt32(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id)),
                    //            UploadedTime = DateTime.Now
                    //        };

                    //        await _context.TblDataRevitalisasiPhotoPelaksanaan.AddAsync(dataDesain).ConfigureAwait(false);
                    //        await _context.SaveChangesAsync().ConfigureAwait(false);

                    //        //for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
                    //        //{
                    //        //    RowErrorKe = i;
                    //        //    IRow row = sheet.GetRow(i);
                    //        //    if (row == null) continue;
                    //        //    if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;

                    //        //    DataRevitalisasiPhoto_ViewModels data = new DataRevitalisasiPhoto_ViewModels();

                    //        //    data.FileName = model.FileDetailPhoto.FileName;
                    //        //    data.Path = row.GetCell(1).ToString();
                    //        //    data.NamaProyek = row.GetCell(2).ToString();
                    //        //    data.JenisProyek = row.GetCell(3).ToString();
                    //        //    data.EstimasiBiaya = decimal.Parse(row.GetCell(4).ToString());
                    //        //    //data.StatusProgress = row.GetCell(5).ToString();
                    //        //    //data.DetailProgress = row.GetCell(6).ToString();
                    //        //    data.NoDokumen = row.GetCell(7).ToString();
                    //        //    data.UIC = row.GetCell(8).ToString();
                    //        //    ListDataRevitalisasiTemp.Add(data);
                    //        //}
                    //        trx.Complete();
                    //    }
                    //}
                    //return Content("");
                    #endregion
                }
                catch (Exception Ex)
                {
                    return Content(Ex.Message);
                }


            }
            catch (Exception Ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion

        #region Submit Edit Detail Revitalisasi
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> SubmitEditDetail(DataRevitalisasiEditDataDetails_ViewModels model)
        {
            try
            {
                #region Not Used
                //TblDataRevitalisasiDetails getDataDetail = await _context.TblDataRevitalisasiDetails.Where(x => x.Id == model.Detail.Id
                //                                            && x.IsActive == true && x.IsDeleted == false).LastOrDefaultAsync();

                //using(TransactionScope trx = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                //{
                //    if(getDataDetail == null)
                //    {
                //        TblDataRevitalisasiDetails setDataDetail = new TblDataRevitalisasiDetails
                //        {
                //            Catatan = model.Detail.Catatan,
                //            CreatedById = Convert.ToInt32(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id)),
                //            CreatedTime = DateTime.Now,
                //            DataRevitilisasiId = model.Id,
                //            IsActive = true,
                //            IsDeleted = false,
                //            PegawaiId = model.Detail.PegawaiId,
                //            Progress = model.Detail.Progress,
                //            Tanggal = DateTime.Parse(model.Detail.TanggalString),
                //            UnitId = model.Detail.UnitId,
                //        };

                //        await _context.TblDataRevitalisasiDetails.AddAsync(setDataDetail);
                //    }
                //    else
                //    {
                //        getDataDetail.Catatan = model.Detail.Catatan;
                //        getDataDetail.DataRevitilisasiId = model.Id;
                //        getDataDetail.IsActive = true;
                //        getDataDetail.IsDeleted = false;
                //        getDataDetail.PegawaiId = model.Detail.PegawaiId;
                //        getDataDetail.Progress = model.Detail.Progress;
                //        getDataDetail.Tanggal = DateTime.Parse(model.Detail.TanggalString);
                //        getDataDetail.UnitId = model.Detail.UnitId;
                //        getDataDetail.UpdatedById = Convert.ToInt32(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id));
                //        getDataDetail.UpdatedTime = DateTime.Now;

                //        _context.Entry(getDataDetail).State = EntityState.Modified;
                //    }
                //    await _context.SaveChangesAsync().ConfigureAwait(false);
                //    trx.Complete();

                //    return Content("");
                //}
                #endregion

                DataRevitalisasiDetails_ViewModels sendApi = new DataRevitalisasiDetails_ViewModels();
                if (model.Detail.Id == 0)
                {
                    sendApi = new DataRevitalisasiDetails_ViewModels
                    {
                        DataRevitilisasiId = model.Id,
                        PegawaiId = model.Detail.PegawaiId,
                        UnitId = model.Detail.UnitId,
                        Tanggal = DateTime.Parse(model.Detail.TanggalString),
                        Catatan = model.Detail.Catatan,
                        Progress = Convert.ToDecimal(model.Detail.ProgressString),
                        ProgressString = model.Detail.ProgressString,
                        CreatedTime = DateTime.Now,
                        CreatedById = Convert.ToInt32(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id)),
                        IsDeleted = false,
                        IsActive = model.Detail.IsActive
                    };
                }
                else
                {
                    sendApi = new DataRevitalisasiDetails_ViewModels
                    {
                        Id = model.Detail.Id,
                        DataRevitilisasiId = model.Id,
                        PegawaiId = model.Detail.PegawaiId,
                        UnitId = model.Detail.UnitId,
                        Tanggal = DateTime.Parse(model.Detail.TanggalString),
                        Catatan = model.Detail.Catatan,
                        Progress = Convert.ToDecimal(model.Detail.ProgressString),
                        ProgressString = model.Detail.ProgressString,
                        UpdatedTime = DateTime.Now,
                        UpdatedById = Convert.ToInt32(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id)),
                        IsDeleted = false,
                        IsActive = model.Detail.IsActive
                    };
                }
                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataRevitalisasi:editDetailRevitalisasi"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, sendApi, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Save");
            }
            catch(Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region Import File
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ImportFile(ImportFile_ViewModels model)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Account");
            }

            try
            {
                if (model.FileImport == null && model.FileImport.Length == 0)
                {
                    return Content("Masukkan file terlebih dahulu");
                }

                ListDataRevitalisasiTemp = new List<DataRevitalisasi_ViewModels>();


                var RowErrorKe = 0;

                try
                {

                    //Ambil ext file yang diijinkan
                    TblSystemParameter AllowExtValue = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "AllowedFileImportType");
                    TblSystemParameter MaxFileValue = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "MaxFileImportUploadSize");
                    TblSystemParameter PathFolderFileImport = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "PathFolderFileImport");

                    string AllowedFileUploadType = AllowExtValue.Value;
                    string PathFolder = PathFolderFileImport.Value;
                    decimal MaxFileUploadSize = decimal.Parse(MaxFileValue.Value);
                    decimal SizeFile = model.FileImport.Length / 1000000;


                    string Ext = Path.GetExtension(model.FileImport.FileName);

                    //Validate Upload
                    if (!AllowedFileUploadType.Contains(Ext))
                    {
                        return Content(GetConfig.AppSetting["AppSettings:GlobalMessage:ImportDataTypeTidakSesuai"]);
                    }

                  
                    string SubPath = PathFolder + "DataRevitalisasi/";

                    if (!Directory.Exists(SubPath))
                    {
                        Directory.CreateDirectory(SubPath);
                    }


                    if (model.FileImport.Length > 0)
                    {
                        string sFileExtension = Path.GetExtension(model.FileImport.FileName).ToLower();
                        ISheet sheet;
                        string fullPath = Path.Combine(SubPath, model.FileImport.FileName);
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            model.FileImport.CopyTo(stream);
                            stream.Position = 0;
                            if (sFileExtension == ".xls")
                            {
                                HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //This will read the Excel 97-2000 formats  
                                sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook  
                            }
                            else
                            {
                                XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //This will read 2007 Excel format  
                                sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook   
                            }
                        }


                        using (TransactionScope trx = new TransactionScope())
                        {

                            for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                            {
                                RowErrorKe = i;
                                IRow row = sheet.GetRow(i);
                                if (row == null) continue;
                                if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;

                                DataRevitalisasi_ViewModels data = new DataRevitalisasi_ViewModels();

                                data.KodeOutlet = row.GetCell(0).ToString();
                                data.NamaUnit = row.GetCell(1).ToString();
                                data.NamaProyek = row.GetCell(2).ToString();
                                data.JenisProyek = row.GetCell(3).ToString();
                                data.EstimasiBiaya = decimal.Parse(row.GetCell(4).ToString());
                                //data.StatusProgress = row.GetCell(5).ToString();
                                //data.DetailProgress = row.GetCell(6).ToString();
                                data.NoDokumen = row.GetCell(7).ToString();
                                data.UIC = row.GetCell(8).ToString();
                                ListDataRevitalisasiTemp.Add(data);
                            }
                            trx.Complete();
                        }
                    }
                    return Content("");

                }
                catch (Exception Ex)
                {
                    return Content("Error Row ke -" + RowErrorKe + Ex.Message);
                }


            }
            catch (Exception Ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion

        #region Create Catatan Desain
        public async Task<ActionResult> FormCreateCatatanDesain()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login");
            }
            DataRevitalisasiEditDataDetails_ViewModels cek = new DataRevitalisasiEditDataDetails_ViewModels();
            //if (form.FileDetailPhoto != null)
            //{
            //    //InfoDebitur_ViewModel outputData = new InfoDebitur_ViewModel();
            //    //var data = await _context.TblDebiturPabrik.Where(x => x.Id == Id).LastOrDefaultAsync();
            //    //InfoPabrik_ViewModel getData = new InfoPabrik_ViewModel
            //    //{
            //    //    Alamat = data.Alamat,
            //    //    JumlahKaryawan = data.JumlahKaryawan,
            //    //    Nama = data.Nama,
            //    //    Telp = data.Telp,
            //    //    PenanggungJawab = data.PenanggungJawab,
            //    //    Id = data.Id
            //    //};
            //    //outputData.InfoPabrik = getData;

            //    return PartialView("_CatatanDesain");
            //}
            //else
            //{
            //    return PartialView("_CatatanDesain");
            //    //return Content("dataNull");
            //}
            return PartialView("_CatatanDesain", cek);
        }
        #endregion

        #region Create Catatan Pelaksanaan
        public async Task<ActionResult> FormCreateCatatanPelaksanaan()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login");
            }
            DataRevitalisasiEditDataDetails_ViewModels cek = new DataRevitalisasiEditDataDetails_ViewModels();
            
            return PartialView("_CatatanPelaksanaan", cek);
        }
        #endregion
    }
}
