﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PortalBranchNav.Component;
using PortalBranchNav.Models.dbBranchNav;
using PortalBranchNav.ViewModels;


namespace PortalBranchNav.Controllers
{
    public class DataPenggunaController : Controller
    {
        private readonly dbBranchNavContext _context;
        private readonly IConfiguration _configuration;
        private readonly LastSessionLog lastSession;
        private readonly AccessSecurity accessSecurity;
        public DataPenggunaController(IConfiguration config, dbBranchNavContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _configuration = config;
            lastSession = new LastSessionLog(accessor, context, config);
            accessSecurity = new AccessSecurity(accessor, context, config);
        }

        public IActionResult Index()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            var location = new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}");
            string Path = location.AbsolutePath;

            if (!accessSecurity.IsGetAccess(".." + Path))
            {
                return RedirectToAction("NotAccess", "Error");
            }

            ViewBag.CurrentPath = Path;

            return View();
        }

        #region LoadData
        [HttpPost]
        public IActionResult LoadData()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

                var draw = dict["draw"];

                //Untuk mengetahui info paging dari datatable
                var start = dict["start"];
                var length = dict["length"];

                //Server side datatable hanya support untuk mendapatkan data mulai ke berapa, untuk mengirim row ke berapa
                //Kita perlu membuat logika sendiri
                var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;

                //Untuk mengetahui info order column datatable
                var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                var sortColumnDir = dict["order[0][dir]"];
                var NppSearchParam = dict["columns[2][search][value]"];
                var NamaSearchParam = dict["columns[3][search][value]"];
                var UnitSearchParam = dict["columns[4][search][value]"];

                var JabatanId = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Role_Id));
                var UnitId = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Unit_Id));


                //Untuk mengetahui info jumlah page dan total skip data
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                var data = new DataTableViewModel();
                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterPengguna:get"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        pageNumber = pageNumber,
                        pageSize = pageSize,
                        sortColumn = sortColumn,
                        sortColumnDir = sortColumnDir,
                        roleId = JabatanId,
                        unitId = UnitId,
                        npp = String.IsNullOrEmpty(NppSearchParam) ? null : NppSearchParam,
                        nama = String.IsNullOrEmpty(NamaSearchParam) ? null : NamaSearchParam,
                        unit = String.IsNullOrEmpty(UnitSearchParam) ? null : UnitSearchParam
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    data = JsonConvert.DeserializeObject<DataTableViewModel>(result);
                }
                return Json(
                    new
                    {
                        draw = draw,
                        recordsFiltered = data.data == null ? 0 : data.data.recordTotals,
                        recordsTotal = data.data == null ? 0 : data.data.recordTotals,
                        data = data.data == null ? new List<object>() : data.data.data == null ? new List<object>() : data.data.data
                    }
                );
            }
            catch (Exception Ex)
            {
                throw;
            }
        }
        #endregion


        #region Create
        public async Task<ActionResult> Create()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login");
            }
            

            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownJabatan"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResult = JsonConvert.DeserializeObject<List<DataDropdownServerSide>>(result);
            ViewBag.RolePegawai = new SelectList(jsonResult, "id", "text");

            return PartialView("_Create");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> SubmitCreate(MasterDataPengguna_ViewModels model, string JabatanIds)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                if (!lastSession.Update())
                {
                    return RedirectToAction("Login", "Login", new { a = true });
                }

                var data = new DataTableViewModel();
                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterPengguna:create"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        username = model.Npp,
                        password = model.Npp,
                        unitId = model.UnitId,
                        jabatanId = model.JabatanId,
                        npp = model.Npp,
                        nama = model.Nama,
                        email = model.Email,
                        ldapLogin = model.IsLDAP,
                        jabatanIds = JabatanIds
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Save");
            }
            catch (Exception Ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }

        #endregion

        #region Edit
        public async Task<ActionResult> Edit(int id)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });

            }

            MasterDataPengguna_ViewModels data = new MasterDataPengguna_ViewModels();
            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterPengguna:getById"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { id = id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResult = JsonConvert.DeserializeObject<GetDataFromAPI<GetItem<GetDataMasterPenggunaFromAPI, GetJabatanPegawaiFromAPI>>>(result);

            if (jsonResult.data.item1 == null)
            {
                data = new MasterDataPengguna_ViewModels();
            }
            else
            {
                data = new MasterDataPengguna_ViewModels
                {
                    CreatedBy = jsonResult.data.item1.createdBy,
                    CreatedTime = jsonResult.data.item1.createdTime,
                    Email = jsonResult.data.item1.email,
                    Id = jsonResult.data.item1.id,
                    IsActive = jsonResult.data.item1.isActive,
                    IsLDAP = jsonResult.data.item1.isLDAP,
                    JabatanId = jsonResult.data.item1.jabatanId,
                    ListJabatan = jsonResult.data.item1.listJabatan,
                    LastLogin = jsonResult.data.item1.lastLogin,
                    Nama = jsonResult.data.item1.nama,
                    Npp = jsonResult.data.item1.npp,
                    Number = jsonResult.data.item1.number,
                    RoleId = jsonResult.data.item1.roleId,
                    Status = jsonResult.data.item1.status,
                    UnitId = jsonResult.data.item1.unitId,
                    UpdatedBy = jsonResult.data.item1.updatedBy,
                    UpdatedTime = jsonResult.data.item1.updatedTime,
                    Unit = jsonResult.data.item1.unit
                };
            }

            if (jsonResult.data.item1.unitId != null)
            {
                //ViewBag.Unit = new SelectList(Utility.SelectDataUnit(data.UnitId, _context), "id", "text", data.UnitId);
                var urlGetAllUnit = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:getAllUnit"];
                (bool resultApiGetAllUnit, string resultGetAllUnit) = RequestToAPI.PostRequestToWebApi(urlGetAllUnit, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                var jsonResultGetAllUnit = JsonConvert.DeserializeObject<DataTableViewModel<TblUnit>>(resultGetAllUnit);
                ViewBag.Unit = new SelectList(jsonResultGetAllUnit.data.data, "Id", "Name", jsonResult.data.item1.unitId);
            }


            var urlGetJabatanAll = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownJabatan"];
            (bool resultApiGetJabatanAll, string resultGetJabatanAll) = RequestToAPI.PostRequestToWebApi(urlGetJabatanAll, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResultGetJabatanAll = JsonConvert.DeserializeObject<List<DataDropdownServerSide>>(resultGetJabatanAll);
            ViewBag.RolePegawai = new SelectList(jsonResultGetJabatanAll, "id", "text");


            //var listJabatanId = jsonResult.data.item2.Select(x => x.jabatan_Id).ToList();
            ViewBag.ListJabatanId = jsonResult.data.item1.listJabatan;



            return PartialView("_Edit", data);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> SubmitEdit(MasterDataPengguna_ViewModels model, string JabatanIds)
        {
            try
            {
                if (!lastSession.Update())
                {
                    return RedirectToAction("Login", "Login", new { a = true });
                }

                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterPengguna:edit"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        id = model.Id,
                        username = model.Npp,
                        password = model.Npp,
                        unitId = model.UnitId,
                        jabatanId = model.JabatanId,
                        npp = model.Npp,
                        nama = model.Nama,
                        email = model.Email,
                        ldapLogin = model.IsLDAP,
                        jabatanIds = JabatanIds
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Save");
            }
            catch(Exception ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion


        #region View
        public async Task<ActionResult> View(int id)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });

            }
            MasterDataPengguna_ViewModels data = new MasterDataPengguna_ViewModels();
            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterPengguna:getById"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { id = id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResult = JsonConvert.DeserializeObject<GetDataFromAPI<GetItem<GetDataMasterPenggunaFromAPI, GetJabatanPegawaiFromAPI>>>(result);

            if (jsonResult.data.item1 == null)
            {
                data = new MasterDataPengguna_ViewModels();
            }
            else
            {
                data = new MasterDataPengguna_ViewModels
                {
                    CreatedBy = jsonResult.data.item1.createdBy,
                    CreatedTime = jsonResult.data.item1.createdTime,
                    Email = jsonResult.data.item1.email,
                    Id = jsonResult.data.item1.id,
                    IsActive = jsonResult.data.item1.isActive,
                    IsLDAP = jsonResult.data.item1.isLDAP,
                    JabatanId = jsonResult.data.item1.jabatanId,
                    ListJabatan = jsonResult.data.item1.listJabatan,
                    LastLogin = jsonResult.data.item1.lastLogin,
                    Nama = jsonResult.data.item1.nama,
                    Npp = jsonResult.data.item1.npp,
                    Number = jsonResult.data.item1.number,
                    RoleId = jsonResult.data.item1.roleId,
                    Status = jsonResult.data.item1.status,
                    UnitId = jsonResult.data.item1.unitId,
                    UpdatedBy = jsonResult.data.item1.updatedBy,
                    UpdatedTime = jsonResult.data.item1.updatedTime,
                    Unit = jsonResult.data.item1.unit
                };
            }

            if (jsonResult.data.item1.unitId != null)
            {
                //ViewBag.Unit = new SelectList(Utility.SelectDataUnit(data.UnitId, _context), "id", "text", data.UnitId);
                var urlGetAllUnit = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:getAllUnit"];
                (bool resultApiGetAllUnit, string resultGetAllUnit) = RequestToAPI.PostRequestToWebApi(urlGetAllUnit, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                var jsonResultGetAllUnit = JsonConvert.DeserializeObject<DataTableViewModel<TblUnit>>(resultGetAllUnit);
                ViewBag.Unit = new SelectList(jsonResultGetAllUnit.data.data, "Id", "Name", jsonResult.data.item1.unitId);
            }
            

            var urlGetJabatanAll = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownJabatan"];
            (bool resultApiGetJabatanAll, string resultGetJabatanAll) = RequestToAPI.PostRequestToWebApi(urlGetJabatanAll, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResultGetJabatanAll = JsonConvert.DeserializeObject<List<DataDropdownServerSide>>(resultGetJabatanAll);
            ViewBag.RolePegawai = new SelectList(jsonResultGetJabatanAll, "id", "text");


            /*var listJabatanId = jsonResult.data.item2.Select(x => x.jabatan_Id).ToList();*/
            ViewBag.ListJabatanId = jsonResult.data.item1.listJabatan;


            return PartialView("_View", data);
        }

        #endregion

        #region Delete
        public ActionResult Delete(string Ids)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterPengguna:delete"];
                var idarray = Ids.Split(',').Select(int.Parse).ToList();
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url,
                new
                {
                    ids = idarray
                }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Delete");
            }
            catch
            {
                return Content("gagal");
            }
        }
        #endregion

        #region CheckNppToGetName
        public async Task<ActionResult> CheckNppToGetName(string npp)
        {
            try
            {
                if (!lastSession.Update())
                {
                    return RedirectToAction("Login", "Login", new { a = true });

                }

                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:datapegawai:getName"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        npp = npp
                    });
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    var jsonResult = JsonConvert.DeserializeObject<ServiceResponseSingle2<DataPegawai>>(result);
                    return Content(jsonResult.data.nama);
                }
                else
                    return Content("Failed");
            }
            catch (Exception ex)
            {
                return Content("Failed");
                //return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion

        #region CheckNppToGetEmail
        public async Task<ActionResult> CheckNppToGetEmail(string npp)
        {
            try
            {
                if (!lastSession.Update())
                {
                    return RedirectToAction("Login", "Login", new { a = true });

                }

                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:datapegawai:getEmail"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        npp = npp
                    });
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    var jsonResult = JsonConvert.DeserializeObject<ServiceResponseSingle2<DataPegawai>>(result);
                    return Content(jsonResult.data.email);
                }
                else
                    return Content("Failed");
            }
            catch (Exception ex)
            {
                return Content("Failed");
                //return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion
    }
}
