﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PortalBranchNav.Component;
using PortalBranchNav.Models.dbBranchNav;
using PortalBranchNav.ViewModels;


namespace PortalBranchNav.Controllers
{
    public class MasterDataUnitController : Controller
    {
        private readonly dbBranchNavContext _context;
        private readonly IConfiguration _configuration;
        private readonly LastSessionLog lastSession;
        private readonly AccessSecurity accessSecurity;
        private Utility Utility = new Utility();
        public MasterDataUnitController(IConfiguration config, dbBranchNavContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _configuration = config;
            lastSession = new LastSessionLog(accessor, context, config);
            accessSecurity = new AccessSecurity(accessor, context, config);
        }

        public IActionResult Index()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            var location = new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}");
            string Path = location.AbsolutePath;

            if (!accessSecurity.IsGetAccess(".." + Path))
            {
                return RedirectToAction("NotAccess", "Error");
            }

            ViewBag.CurrentPath = Path;

            return View();
        }

        #region LoadData
        [HttpPost]
        public IActionResult LoadData()
         {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

                var draw = dict["draw"];

                //Untuk mengetahui info paging dari datatable
                var start = dict["start"];
                var length = dict["length"];

                //Server side datatable hanya support untuk mendapatkan data mulai ke berapa, untuk mengirim row ke berapa
                //Kita perlu membuat logika sendiri
                var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;

                //Untuk mengetahui info order column datatable
                var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                var sortColumnDir = dict["order[0][dir]"];

                var TypeUnitSearchParam = dict["columns[2][search][value]"];
                var KodeUnitSearchParam = dict["columns[3][search][value]"];
                var NamaUnitSearchParam = dict["columns[4][search][value]"];

                //Untuk mengetahui info jumlah page dan total skip data
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                var data = new DataTableViewModel();

                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterDataUnit:get"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        pageNumber = pageNumber,
                        pageSize = pageSize,
                        sortColumn = sortColumn,
                        sortColumnDir = sortColumnDir,
                        typeunit = String.IsNullOrEmpty(TypeUnitSearchParam) ? null : TypeUnitSearchParam,
                        kodeunit = String.IsNullOrEmpty(KodeUnitSearchParam) ? null : KodeUnitSearchParam,
                        namaunit = String.IsNullOrEmpty(NamaUnitSearchParam) ? null : NamaUnitSearchParam
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    data = JsonConvert.DeserializeObject<DataTableViewModel>(result);
                }
                return Json(
                    new
                    {
                        draw = draw,
                        recordsFiltered = data.data == null ? 0 : data.data.recordTotals,
                        recordsTotal = data.data == null ? 0 : data.data.recordTotals,
                        data = data.data == null ? new List<object>() : data.data.data == null ? new List<object>() : data.data.data
                    }
                );

            }
            catch (Exception Ex)
            {
                throw;
            }
        }
        #endregion

        #region Create
        public ActionResult Create()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login");
            }
            ViewBag.TypeUnit = new SelectList(Utility.SelectLookup("TypeUnit", _context, HttpContext.Session.GetString(SessionConstan.jwt_Token)), "Value", "Name");


            return PartialView("_Create");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SubmitCreate(TblUnit model)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                if (!lastSession.Update())
                {
                    return RedirectToAction("Login", "Login", new { a = true });
                }

                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterDataUnit:create"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Save");
            }
            catch(Exception Ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }

        #endregion

        #region Edit
        public ActionResult Edit(int id)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });

            }
            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterDataUnit:getById"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { Id = id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResult = JsonConvert.DeserializeObject<ServiceResult<TblUnit>>(result);
            ViewBag.TypeUnit = new SelectList(Utility.SelectLookup("TypeUnit", _context, HttpContext.Session.GetString(SessionConstan.jwt_Token)).ToList(), "Value", "Name", jsonResult.Data.Type);
            if (jsonResult.Data.ParentId != null)
            {
                ViewBag.ParentUnit = new SelectList(Utility.SelectDataUnit(jsonResult.Data.ParentId, _context, HttpContext.Session.GetString(SessionConstan.jwt_Token)).ToList(), "id", "text", jsonResult.Data.ParentId);
            }
            else
            {
                ViewBag.ParentUnit = new SelectList("", "");
            }

            if (jsonResult.Data.WilayahId != null)
            {
                ViewBag.Wilayah = new SelectList(Utility.SelectDataUnit(jsonResult.Data.WilayahId, _context, HttpContext.Session.GetString(SessionConstan.jwt_Token)).ToList(), "id", "text", jsonResult.Data.WilayahId);
            }
            else
            {
                ViewBag.Wilayah = new SelectList("", "");
            }

            if (jsonResult.Data.DivisiId != null)
            {
                ViewBag.Divisi = new SelectList(Utility.SelectDataUnit(jsonResult.Data.DivisiId, _context, HttpContext.Session.GetString(SessionConstan.jwt_Token)).ToList(), "id", "text", jsonResult.Data.DivisiId);
            }
            else
            {
                ViewBag.Divisi = new SelectList("", "");
            }
            return PartialView("_Edit", jsonResult.Data);

            //return PartialView("_Edit", Data);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SubmitEdit(TblUnit model)
        {
            try
            {
                if (!lastSession.Update())
                {
                    return RedirectToAction("Login", "Login", new { a = true });
                }

                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterDataUnit:edit"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Update");
            }
            catch
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion


        #region View
        public ActionResult View(int id)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });

            }
            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterDataUnit:getById"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { Id = id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResult = JsonConvert.DeserializeObject<ServiceResult<TblUnit>>(result);
            ViewBag.TypeUnit = new SelectList(Utility.SelectLookup("TypeUnit", _context, HttpContext.Session.GetString(SessionConstan.jwt_Token)).ToList(), "Value", "Name", jsonResult.Data.Type);
            if (jsonResult.Data.ParentId != null)
            {
                ViewBag.ParentUnit = new SelectList(Utility.SelectDataUnit(jsonResult.Data.ParentId, _context, HttpContext.Session.GetString(SessionConstan.jwt_Token)).ToList(), "id", "text", jsonResult.Data.ParentId);
            }
            else
            {
                ViewBag.ParentUnit = new SelectList("", "");
            }

            if (jsonResult.Data.WilayahId != null)
            {
                ViewBag.Wilayah = new SelectList(Utility.SelectDataUnit(jsonResult.Data.WilayahId, _context, HttpContext.Session.GetString(SessionConstan.jwt_Token)).ToList(), "id", "text", jsonResult.Data.WilayahId);
            }
            else
            {
                ViewBag.Wilayah = new SelectList("", "");
            }

            if (jsonResult.Data.DivisiId != null)
            {
                ViewBag.Divisi = new SelectList(Utility.SelectDataUnit(jsonResult.Data.DivisiId, _context, HttpContext.Session.GetString(SessionConstan.jwt_Token)).ToList(), "id", "text", jsonResult.Data.DivisiId);
            }
            else
            {
                ViewBag.Divisi = new SelectList("", "");
            }
            return PartialView("_View", jsonResult.Data);
            //return PartialView("_View", Data);

        }

        #endregion

        #region Delete
        public ActionResult Delete(string Ids)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterDataUnit:delete"];
                var idarray = Ids.Split(',').Select(int.Parse).ToList();
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url,
                new
                {
                    ids = idarray
                }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Delete");
            }
            catch
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion
    }
}
