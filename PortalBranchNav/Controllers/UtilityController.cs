﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PortalBranchNav.ViewModels;
using PortalBranchNav.Models.dbBranchNav;
using PortalBranchNav.Component;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net;
using System.DirectoryServices;
using PortalBranchNav.Content;
using Newtonsoft.Json;
//using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PortalBranchNav.Controllers
{
    public class UtilityController : Controller
    {

        private readonly dbBranchNavContext _context;
        private readonly IConfiguration _configuration;
        private readonly LastSessionLog lastSession;


        public UtilityController(IConfiguration config, dbBranchNavContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _configuration = config;
            lastSession = new LastSessionLog(accessor, context, config);
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            lastSession.Update();
            return View();
        }

        public IActionResult EmptyData()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Account");
            }
            return View();
        }

        #region LDAP Login
        public static bool AuthenticationLdap(String userName, String passWord)
        {
            string Domain = string.Empty;
            string path = string.Empty;
            string sDomainAndUsername = Domain;
            path = GetConfig.AppSetting["AppSettings:LDAPConnection:Url"];
            Domain = "uid=" + userName + "," + GetConfig.AppSetting["AppSettings:LDAPConnection:LdapHierarchy"];
            AuthenticationTypes at = AuthenticationTypes.ServerBind;
            DirectoryEntry entry = new DirectoryEntry(path, Domain, passWord, at);
            bool cek = false;
            try
            {
                Object obj = entry.NativeObject;
                DirectorySearcher mySearcher = new DirectorySearcher(entry);
                SearchResult result;
                mySearcher.Filter = "(uid=" + userName + ")";
                mySearcher.PropertiesToLoad.Add("sn");
                result = mySearcher.FindOne();
                cek = true;
            }
            catch (Exception ex)
            {
                //throw new Exception("Error authenticating user. " + ex.Message);
                cek = false;
            }
            return cek;
        }
        #endregion

        #region Get All Role Pegawai
        public JsonResult GetAllRolePegawai(int Pegawai_Id)
        {
            //lastSession.Update();
            //List<Dropdown_ViewModels> data = new List<Dropdown_ViewModels>();
            //string Date = DateTime.Now.ToString("yyyy-MM-dd");

            //data = StoredProcedureExecutor.ExecuteSPList<Dropdown_ViewModels>(_context, "sp_Login_GetDataRolePegawai", new SqlParameter[]{
            //            new SqlParameter("@Pegawai_id", Pegawai_Id),
            //            new SqlParameter("@Date", Date)
            //});
            //return Json(data);
            try
            {
                var url = GetConfig.AppSetting["baseapi"] + GetConfig.AppSetting["urlapi:Utility:GetAllRoleByPegawaiId"];
                (bool resultapi, string result) = RequestToAPI.PostRequestToWebApi(url, new { pegawai_Id = Pegawai_Id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                var jsonResult = JsonConvert.DeserializeObject<List<Dropdown_ViewModels>>(result);
                return Json(jsonResult);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region Get JenisLaporan
        public JsonResult GetJenisLaporan(string q, string kodeDivisi)
        {
            //lastSession.Update();
            //List<Dropdown_ViewModels> data = new List<Dropdown_ViewModels>();
            //string Date = DateTime.Now.ToString("yyyy-MM-dd");

            //data = StoredProcedureExecutor.ExecuteSPList<Dropdown_ViewModels>(_context, "sp_Login_GetDataRolePegawai", new SqlParameter[]{
            //            new SqlParameter("@Pegawai_id", Pegawai_Id),
            //            new SqlParameter("@Date", Date)
            //});
            //return Json(data);
            try
            {
                var urlGetJnsLaporan = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownJnsLaporanByKodeDivisi"];
                (bool resultApiJnsLaporan, string resultGetJnsLaporan) = RequestToAPI.PostRequestToWebApi(urlGetJnsLaporan, new { q = q,kodeDivisi = kodeDivisi }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                //var jsonResultJnsLaporan = JsonConvert.DeserializeObject<List<DataDropdownServerSide>>(resultGetJnsLaporan);

                //var stringRes = await response.Content.ReadAsStringAsync();
                var data = JsonConvert.DeserializeObject<List<DataDropdownServerSide>>(resultGetJnsLaporan);
                if (data != null)
                {
                    return Json(data);
                }
                else
                {
                    List<DataDropdownServerSide> dataNull = new List<DataDropdownServerSide>();
                    return Json(dataNull);
                }

                //return Json(jsonResultJnsLaporan);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region Get DataPegawai
        public JsonResult GetDataPegawai(string npp) {
            try
            {
                var urlGetJnsLaporan = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:datapegawai:getName"];
                (bool resultApiJnsLaporan, string resultGetJnsLaporan) = RequestToAPI.PostRequestToWebApi(urlGetJnsLaporan, new { npp = npp }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                //var jsonResultJnsLaporan = JsonConvert.DeserializeObject<List<DataDropdownServerSide>>(resultGetJnsLaporan);

                //var stringRes = await response.Content.ReadAsStringAsync();
                var data = JsonConvert.DeserializeObject<ServiceResponseSingle2<DataPegawai>>(resultGetJnsLaporan);
                if (data != null)
                {
                    return Json(data.data.nama);
                }
                else
                {
                    List<DataDropdownServerSide> dataNull = new List<DataDropdownServerSide>();
                    return Json(dataNull);
                }

                //return Json(jsonResultJnsLaporan);
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        #endregion

        #region Dropdown Role
        public JsonResult GetDropdownRolesByMenuId(string Roles)
        {
            lastSession.Update();
            //List<DataDropdownServerSide> data = new List<DataDropdownServerSide>();

            //data = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "[SP_Dropdown_Menu_GetDataRolesById]", new SqlParameter[]{
            //            new SqlParameter("@Id", Roles)});

            //return Json(data);
            try {
                var url = GetConfig.AppSetting["baseapi"] + GetConfig.AppSetting["urlapi:Utility:GetDropdownRolesByMenuId"];
                (bool resultapi, string result) = RequestToAPI.PostRequestToWebApi(url, new { roles = Roles }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                var jsonResult = JsonConvert.DeserializeObject<List<DataDropdownServerSide>>(result);
                return Json(jsonResult);
            }
            catch(Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region Dropdown Unit
        public JsonResult GetDropdownUnitById(string Ids)
        {
            List<DataDropdownServerSide> data = new List<DataDropdownServerSide>();
            //data = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "SP_Dropdown_Unit_ById", new SqlParameter[]{
            //            new SqlParameter("@Id", Ids)
            //        });
            //data = data != null ? data : new List<DataDropdownServerSide>();

            //return Json(data);
            try {
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownUnitById"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { ids = Ids }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                var jsonResult = JsonConvert.DeserializeObject<ServiceResult<List<DataDropdownServerSide>>>(result);
                data = jsonResult.Data;
                return Json(data);
            }
            catch (Exception Ex)
            {

                throw;
            }

        }
        #endregion

        #region Get All Dropdown Unit
        public JsonResult GetAllDataUnit(string q, string page, int rowPerPage, string TypeUnit)
        {
            lastSession.Update();
            if (page == null)
            {
                page = "1";
            }
            ListDataDropdownServerSide source = new ListDataDropdownServerSide();
            //source.items = new List<DataDropdownServerSide>();

            //source.items = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "SP_Dropdown_Unit", new SqlParameter[]{
            //            new SqlParameter("@Parameter", q),
            //            new SqlParameter("@TypeUnit", TypeUnit),

            //            new SqlParameter("@PageNumber", page),
            //            new SqlParameter("@RowsPage", rowPerPage)});

            //source.total_count = StoredProcedureExecutor.ExecuteScalarInt(_context, "SP_Dropdown_Unit_Count", new SqlParameter[]{
            //            new SqlParameter("@Parameter", q),
            //            new SqlParameter("@TypeUnit", TypeUnit)
            //});

            //return Json(source);

            var data = new ServiceResult<DeserializeListDataDropdownServerSide<DataDropdownServerSide>>();
            data.Data = new DeserializeListDataDropdownServerSide<DataDropdownServerSide>();

            try
            {
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownUnit"];
                //var data = new ListDataDropdownServerSide();
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        searchParam = q,
                        pageNumber = page,
                        pageSize = rowPerPage,
                        typeUnit = TypeUnit
                    }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                    data = JsonConvert.DeserializeObject<ServiceResult<DeserializeListDataDropdownServerSide<DataDropdownServerSide>>>(result);

                source.items = data.Data.data;
                source.total_count = data.Data.recordTotals;
            }
            catch (Exception Ex)
            {

                throw;
            }

            return Json(source);
        }
        #endregion

        //#region Get All Dropdown Sistem
        //public JsonResult GetAllDataSistem(string q, string page, int rowPerPage, string TypeUnit)
        //{
        //    lastSession.Update();
        //    if (page == null)
        //    {
        //        page = "1";
        //    }
        //    ListDataDropdownServerSide source = new ListDataDropdownServerSide();
        //    source.items = new List<DataDropdownServerSide>();

        //    source.items = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "SP_Dropdown_MasterSistem", new SqlParameter[]{
        //                new SqlParameter("@Parameter", q),
        //                new SqlParameter("@PageNumber", page),
        //                new SqlParameter("@RowsPage", rowPerPage)});

        //    source.total_count = StoredProcedureExecutor.ExecuteScalarInt(_context, "SP_Dropdown_MasterSistem_Count", new SqlParameter[]{
        //                new SqlParameter("@Parameter", q)
        //    });

        //    return Json(source);
        //}
        //#endregion

        #region Get All Dropdown Kategori Project
        //public JsonResult GetAllDataKategoriProject(string q, string page, int rowPerPage, string TypeUnit)
        //{
        //    lastSession.Update();
        //    if (page == null)
        //    {
        //        page = "1";
        //    }
        //    ListDataDropdownServerSide source = new ListDataDropdownServerSide();
        //    source.items = new List<DataDropdownServerSide>();

        //    source.items = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "SP_Dropdown_KategoriProject", new SqlParameter[]{
        //                new SqlParameter("@Parameter", q),
        //                new SqlParameter("@PageNumber", page),
        //                new SqlParameter("@RowsPage", rowPerPage)});

        //    source.total_count = StoredProcedureExecutor.ExecuteScalarInt(_context, "SP_Dropdown_KategoriProject_Count", new SqlParameter[]{
        //                new SqlParameter("@Parameter", q)
        //    });

        //    return Json(source);
        //}
        #endregion

        #region Get All Dropdown Project
        //public JsonResult GetAllDataProject(string q, string page, int rowPerPage, string TypeUnit)
        //{
        //    lastSession.Update();
        //    if (page == null)
        //    {
        //        page = "1";
        //    }
        //    ListDataDropdownServerSide source = new ListDataDropdownServerSide();
        //    source.items = new List<DataDropdownServerSide>();

        //    source.items = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "SP_Dropdown_Project", new SqlParameter[]{
        //                new SqlParameter("@Parameter", q),
        //                new SqlParameter("@PageNumber", page),
        //                new SqlParameter("@RowsPage", rowPerPage)});

        //    source.total_count = StoredProcedureExecutor.ExecuteScalarInt(_context, "SP_Dropdown_Project_Count", new SqlParameter[]{
        //                new SqlParameter("@Parameter", q)
        //    });

        //    return Json(source);
        //}
        #endregion

        #region Get All Dropdown Master Type Client
        //public JsonResult GetAllDataTypeClient(string q, string page, int rowPerPage, string TypeUnit)
        //{
        //    lastSession.Update();
        //    if (page == null)
        //    {
        //        page = "1";
        //    }
        //    ListDataDropdownServerSide source = new ListDataDropdownServerSide();
        //    source.items = new List<DataDropdownServerSide>();

        //    source.items = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "SP_Dropdown_MasterTypeClient", new SqlParameter[]{
        //                new SqlParameter("@Parameter", q),
        //                new SqlParameter("@PageNumber", page),
        //                new SqlParameter("@RowsPage", rowPerPage)});

        //    source.total_count = StoredProcedureExecutor.ExecuteScalarInt(_context, "SP_Dropdown_MasterTypeClient_Count", new SqlParameter[]{
        //                new SqlParameter("@Parameter", q)
        //    });

        //    return Json(source);
        //}
        #endregion

        #region Get All Dropdown Master Client
        //public JsonResult GetAllDataClient(string q, string page, int rowPerPage, string TypeUnit)
        //{
        //    lastSession.Update();
        //    if (page == null)
        //    {
        //        page = "1";
        //    }
        //    ListDataDropdownServerSide source = new ListDataDropdownServerSide();
        //    source.items = new List<DataDropdownServerSide>();

        //    source.items = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "[SP_Dropdown_Client]", new SqlParameter[]{
        //                new SqlParameter("@Parameter", q),
        //                new SqlParameter("@PageNumber", page),
        //                new SqlParameter("@RowsPage", rowPerPage)});

        //    source.total_count = StoredProcedureExecutor.ExecuteScalarInt(_context, "[SP_Dropdown_Client_Count]", new SqlParameter[]{
        //                new SqlParameter("@Parameter", q)
        //    });

        //    return Json(source);
        //}
        #endregion

        #region Select Data By ID For Dropdown Jabatan
        public JsonResult GetDropdownJabatanById(string Ids)
        {
            List<DataDropdownServerSide> data = new List<DataDropdownServerSide>();
            //data = StoredProcedureExecutor.ExecuteSPList<DataDropdownServerSide>(_context, "SP_Dropdown_Jabatan_ById", new SqlParameter[]{
            //    new SqlParameter("@Id", Ids)
            //        });
            //data = data != null ? data : new List<DataDropdownServerSide>();

            //return Json(data);

            try
            {
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownJabatanById"];
                //var data = new ListDataDropdownServerSide();
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        id = Ids
                    }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                    data = JsonConvert.DeserializeObject<List<DataDropdownServerSide>>(result);
                return Json(data);

            }
            catch (Exception Ex)
            {

                throw;
            }

        }
        #endregion
    }
}
