﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using PortalBranchNav.Component;
using PortalBranchNav.Models.dbBranchNav;
using PortalBranchNav.ViewModels;


namespace PortalBranchNav.Controllers
{
    public class DataPendingAuditController : Controller
    {
        private readonly dbBranchNavContext _context;
        private readonly IConfiguration _configuration;
        private readonly LastSessionLog lastSession;
        private readonly AccessSecurity accessSecurity;
        private IHostingEnvironment _env;

        public static List<DataPendingAudit_ViewModels> ListDataPendingAuditTemp = new List<DataPendingAudit_ViewModels>();

        public DataPendingAuditController(IHostingEnvironment env, IConfiguration config, dbBranchNavContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _configuration = config;
            lastSession = new LastSessionLog(accessor, context, config);
            accessSecurity = new AccessSecurity(accessor, context, config);
            _env = env;
        }



        public IActionResult Index()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            var location = new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}");
            string Path = location.AbsolutePath;

            if (!accessSecurity.IsGetAccess(".." + Path))
            {
                return RedirectToAction("NotAccess", "Error");
            }

            ViewBag.CurrentPath = Path;

            return View();
        }

     

        #region LoadData
        [HttpPost]
        public IActionResult LoadData()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

                var draw = dict["draw"];

                //Untuk mengetahui info paging dari datatable
                var start = dict["start"];
                var length = dict["length"];

                //Server side datatable hanya support untuk mendapatkan data mulai ke berapa, untuk mengirim row ke berapa
                //Kita perlu membuat logika sendiri
                var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;

                //Untuk mengetahui info order column datatable
                var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                var sortColumnDir = dict["order[0][dir]"];
                //var NamaWilayahSearchParam = dict["columns[1][search][value]"];
                var KodeOutletSearchParam = dict["columns[2][search][value]"];
                var NamaUnitSearchParam = dict["columns[3][search][value]"];
                //var TahunSearchParam = dict["columns[3][search][value]"];
                //var PeriodeSearchParam = dict["columns[5][search][value]"];

                //Untuk mengetahui info jumlah page dan total skip data
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                var data = new DataTableViewModel();

                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataPendingAudit:get"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        pageNumber = pageNumber,
                        pageSize = pageSize,
                        sortColumn = sortColumn,
                        sortColumnDir = sortColumnDir,
                        kodeOutlet = String.IsNullOrEmpty(KodeOutletSearchParam) ? null : KodeOutletSearchParam,
                        namaUnit = String.IsNullOrEmpty(NamaUnitSearchParam) ? null : NamaUnitSearchParam
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    data = JsonConvert.DeserializeObject<DataTableViewModel>(result);
                }
                return Json(
                    new
                    {
                        draw = draw,
                        recordsFiltered = data.data == null ? 0 : data.data.recordTotals,
                        recordsTotal = data.data == null ? 0 : data.data.recordTotals,
                        data = data.data == null ? new List<object>() : data.data.data == null ? new List<object>() : data.data.data
                    }
                );
            }
            catch (Exception Ex)
            {
                throw;
            }
        }
        #endregion

        #region Load Data Temp Import
        public ActionResult LoadDataTemp()
        {

            return Json(new { data = ListDataPendingAuditTemp });
        }
        #endregion

        #region Import Data
        public ActionResult Import()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login");
            }
            ListDataPendingAuditTemp = new List<DataPendingAudit_ViewModels>();


            return PartialView("_Import");
        }
        #endregion

        #region Import File
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ImportFile(ImportFile_ViewModels model)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Account");
            }

            try
            {
                if (model.file == null && model.file.Length == 0)
                {
                    return Content("Masukkan file terlebih dahulu");
                }
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataPendingAudit:upload"];
                (bool resultApi, string result) = RequestToAPI.PostFormDataToWebApi(url, model, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    var data = JsonConvert.DeserializeObject<ServiceResult<object>>(result);
                    if (data.Code == 1)
                        return Content("");
                    else
                        return Content("Error Code : " + data.Code + ", " + data.Data);
                }
                return Content(result);
            }
            catch (Exception Ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion

        #region Get Templates
        public FileStreamResult GetTemplate()
        {
            string fileName = GetConfig.AppSetting["templateUpload:PendingAudit"];
            if (string.IsNullOrWhiteSpace(_env.WebRootPath))
            {
                _env.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
            }
            var webRoot = _env.WebRootPath;
            string Folder = Path.Combine(webRoot, "templates");
            //string folderFile = Path.Combine(Folder, "product");
            string file = System.IO.Path.Combine(Folder, fileName);
            FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read);
            return File(fs, "application/vnd.ms-excel", fileName);
        }
        #endregion
    }
}
