﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PortalBranchNav.Component;
using PortalBranchNav.Models.dbBranchNav;
using PortalBranchNav.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace PortalBranchNav.Controllers
{
    public class BNICloudController : Controller
    {
        private readonly dbBranchNavContext _context;
        private readonly IConfiguration _configuration;
        private readonly LastSessionLog lastSession;
        private readonly AccessSecurity accessSecurity;
        private IHostingEnvironment _env;
        private Utility Utility = new Utility();
        public BNICloudController(IConfiguration config, dbBranchNavContext context, IHttpContextAccessor accessor,
            IHostingEnvironment env)
        {
            _context = context;
            _configuration = config;
            lastSession = new LastSessionLog(accessor, context, config);
            accessSecurity = new AccessSecurity(accessor, context, config);
            _env = env;
        }

        public IActionResult Index(string KodeDivisi)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            var location = new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}");
            string Path = location.AbsolutePath;

            //if (!accessSecurity.IsGetAccess(".." + Path))
            //{
            //    return RedirectToAction("NotAccess", "Error");
            //}

            ViewBag.CurrentPath = Path;
            var urlGetDivPengguna = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownDivisiPengguna"];
            (bool resultApi, string resultGetDiv) = RequestToAPI.PostRequestToWebApi(urlGetDivPengguna, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResultDivPengguna = JsonConvert.DeserializeObject<List<DataDropdownDivPengguna>>(resultGetDiv);

            //ViewBag.DivisiPengguna = new SelectList(Utility.SelectDivisiPengguna(_context, HttpContext.Session.GetString(SessionConstan.jwt_Token)), "id", "text"); //return View();
            ViewBag.DivisiPengguna = new SelectList(jsonResultDivPengguna, "kodeDivisi", "text");
            //-------------------------------------------------

            var urlGetJnsLaporan = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownJnsLaporanByKodeDivisi"];
            (bool resultApiJnsLaporan, string resultGetJnsLaporan) = RequestToAPI.PostRequestToWebApi(urlGetJnsLaporan, new { kodeDivisi = KodeDivisi }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResultJnsLaporan = JsonConvert.DeserializeObject<List<DataDropdownServerSide>>(resultGetJnsLaporan);
            ViewBag.JenisLaporan = new SelectList(jsonResultJnsLaporan, "id", "text");
            //ViewBag.JenisLaporan = new SelectList(Utility.SelectJenisLaporan("JnsLaporan", _context, HttpContext.Session.GetString(SessionConstan.jwt_Token)), "Value", "Name");
            return View();
        }

        #region LoadData
        [HttpPost]
        public IActionResult LoadData()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

                var draw = dict["draw"];

                //Untuk mengetahui info paging dari datatable
                var start = dict["start"];
                var length = dict["length"];

                //Server side datatable hanya support untuk mendapatkan data mulai ke berapa, untuk mengirim row ke berapa
                //Kita perlu membuat logika sendiri
                var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;

                //Untuk mengetahui info order column datatable
                var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                var sortColumnDir = dict["order[0][dir]"];
                var JnsLaporanSearchParam = dict["columns[2][search][value]"];
                var DvsPenggunaSearchParam = dict["columns[3][search][value]"];
                var DateStartSearchParam = dict["columns[4][search][value]"];
                var DateEndSearchParam = dict["columns[5][search][value]"];

                //var TypeSearchParam = dict["columns[2][search][value]"];
                //var NamaSearchParam = dict["columns[3][search][value]"];
                //var ParentSearchParam = dict["columns[4][search][value]"];
                //var RoleSearchParam = dict["columns[7][search][value]"];


                //Untuk mengetahui info jumlah page dan total skip data
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var Role_Id = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Role_Id));

                var data = new DataTableViewModel();


                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:BNICloud:get"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        pageNumber = pageNumber,
                        pageSize = pageSize,
                        sortColumn = sortColumn,
                        sortColumnDir = sortColumnDir,
                        isCabang = 1,
                        jnsLaporanId = String.IsNullOrEmpty(JnsLaporanSearchParam) ? null : JnsLaporanSearchParam,
                        divPenggunaKode = String.IsNullOrEmpty(DvsPenggunaSearchParam) ? null : DvsPenggunaSearchParam,
                        startDate = String.IsNullOrEmpty(DateStartSearchParam) ? null : DateStartSearchParam,
                        endDate = String.IsNullOrEmpty(DateEndSearchParam) ? null : DateEndSearchParam

                        //name = String.IsNullOrEmpty(NamaSearchParam) ? null : NamaSearchParam,
                        //type = String.IsNullOrEmpty(TypeSearchParam) ? null : TypeSearchParam,
                        //role = String.IsNullOrEmpty(RoleSearchParam) ? null : RoleSearchParam,
                        //parent = String.IsNullOrEmpty(ParentSearchParam) ? null : ParentSearchParam

                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));

                //bool resultApi = true;

                //string result = "{\"code\":1,\"message\":\"sukses\",\"data\":{\"recordTotals\":2,\"data\":[" +
                //    "{\"number\":1,\"id\":1,\"jenisLaporan\":\"XXX\",\"namaLaporan\":\"Laporan Cabang\",\"divisiPengguna\":\"JAL\",\"tanggalUpload\":\"19-11-2021\",\"diUploadOleh\":\"821179-Tika\",\"diDownloadOleh\":\"83213-Farin\",\"status\":\"Submitted\"}," +
                //    "{\"number\":2,\"id\":2,\"jenisLaporan\":\"XXX\",\"namaLaporan\":\"Laporan Cabang\",\"divisiPengguna\":\"JAL\",\"tanggalUpload\":\"19-11-2021\",\"diUploadOleh\":\"821179-Tika\",\"diDownloadOleh\":\"83213-Farin\",\"status\":\"Submitted\"}," +
                //    "{\"number\":3,\"id\":3,\"jenisLaporan\":\"ZZZ\",\"namaLaporan\":\"Laporan Cabang\",\"divisiPengguna\":\"JAL\",\"tanggalUpload\":\"02-12-2021\",\"diUploadOleh\":\"821179-Tika\",\"diDownloadOleh\":\"83213-Farin\",\"status\":\"Rejected\"}]}}";

                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    data = JsonConvert.DeserializeObject<DataTableViewModel>(result);
                }
                return Json(
                    new
                    {
                        draw = draw,
                        recordsFiltered = data.data == null ? 0 : data.data.recordTotals,
                        recordsTotal = data.data == null ? 0 : data.data.recordTotals,
                        data = data.data == null ? new List<object>() : data.data.data == null ? new List<object>() : data.data.data
                    }
                );
            }
            catch (Exception Ex)
            {
                throw;
            }
        }
        #endregion

        //onprogress
        public ActionResult CascadeCodeDivision(string KodeDivisi)
        {
            var urlGetJnsLaporan = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownJnsLaporanByKodeDivisi"];
            (bool resultApiJnsLaporan, string resultGetJnsLaporan) = RequestToAPI.PostRequestToWebApi(urlGetJnsLaporan, new { kodeDivisi = KodeDivisi }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResultJnsLaporan = JsonConvert.DeserializeObject<List<DataDropdownServerSide>>(resultGetJnsLaporan);
            ViewBag.JenisLaporan = new SelectList(jsonResultJnsLaporan, "id", "text");

            return Content("");

        }

        #region Create
        public ActionResult Create(string KodeDivisi)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login");
            }
            var urlGetDivPengguna = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownDivisiPengguna"];
            (bool resultApi, string resultGetDiv) = RequestToAPI.PostRequestToWebApi(urlGetDivPengguna, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResultDivPengguna = JsonConvert.DeserializeObject<List<DataDropdownDivPengguna>>(resultGetDiv);

            //ViewBag.DivisiPengguna = new SelectList(Utility.SelectDivisiPengguna(_context, HttpContext.Session.GetString(SessionConstan.jwt_Token)), "id", "text"); //return View();
            ViewBag.DivisiPengguna = new SelectList(jsonResultDivPengguna, "kodeDivisi", "text");
            //-------------------------------------------------
            
            var urlGetJnsLaporan = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownJnsLaporanByKodeDivisi"];
            (bool resultApiJnsLaporan, string resultGetJnsLaporan) = RequestToAPI.PostRequestToWebApi(urlGetJnsLaporan, new { kodeDivisi = KodeDivisi }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResultJnsLaporan = JsonConvert.DeserializeObject<List<DataDropdownServerSide>>(resultGetJnsLaporan);
            ViewBag.JenisLaporan = new SelectList("");

            
            
            return PartialView("_Create");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SubmitCreate(DataBNICLoud model)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                if (!lastSession.Update())
                {
                    return RedirectToAction("Login", "Login", new { a = true });
                }

                //connect to api
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:BNICloudReport:create"];
                (bool resultApi, string result) = RequestToAPI.PostFormDataToWebApi(url, /*model*/
                      new
                      {
                          kodeDivisiPengguna = model.divisiPenggunaId,
                          jnsLaporanId = model.jenisLaporanId,
                          fileLaporan = model.file
                      }
                    , HttpContext.Session.GetString(SessionConstan.jwt_Token)
                    );
                

                //if (Roles == null)
                //{
                //    return Content(GetConfig.AppSetting["AppSettings:PilihRolesNavigation:BelumPilihRoles"]);
                //}
                //string[] ArrayRoles = Roles.Split(',');
                //List<NavigationAssignment> listnav = new List<NavigationAssignment>();
                //foreach (var item in ArrayRoles)
                //{
                //    NavigationAssignment dataAssign = new NavigationAssignment();
                //    dataAssign.NavigationId = model.Id;
                //    dataAssign.RoleId = int.Parse(item);
                //    dataAssign.CreatedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id));
                //    dataAssign.CreatedTime = DateTime.Now;
                //    dataAssign.IsActive = true;
                //    listnav.Add(dataAssign);
                //}
                ////connect to API
                //var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterMenu:create"];
                //(bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url,
                //    new
                //    {
                //        Id = model.Id,
                //        Name = model.Name,
                //        Type = model.Type,
                //        ParentNavigationId = model.ParentNavigationId,
                //        Route = model.Route,
                //        IconClass = model.IconClass,
                //        Order = model.Order,
                //        Visible = model.Visible,
                //        CreatedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id)),
                //        CreatedTime = DateTime.Now,
                //        NavigationAssignment = listnav
                //    }
                //    , HttpContext.Session.GetString(SessionConstan.jwt_Token));

               /* bool resultApi = true;
                string result = "ada";*/
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Save");
            }
            catch (Exception)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }

        #endregion

        #region LoadDataLog
        [HttpPost]
        public IActionResult LoadDataLog(int Id, string ListIdAttachment)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

                var draw = dict["draw"];

                //Untuk mengetahui info paging dari datatable
                var start = dict["start"];
                var length = dict["length"];

                //Server side datatable hanya support untuk mendapatkan data mulai ke berapa, untuk mengirim row ke berapa
                //Kita perlu membuat logika sendiri
                var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;

                //Untuk mengetahui info order column datatable
                var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                var sortColumnDir = dict["order[0][dir]"];
               


                //Untuk mengetahui info jumlah page dan total skip data
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var Role_Id = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Role_Id));

                var data = new DataTableViewModel();

                
                
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:ViewLog:get"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        pageNumber = pageNumber,
                        pageSize = pageSize,
                        sortColumn = sortColumn,
                        sortColumnDir = sortColumnDir,
                        laporanId = Id
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));

                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    data = JsonConvert.DeserializeObject<DataTableViewModel>(result);
                }
                return Json(
                    new
                    {
                        draw = draw,
                        recordsFiltered = data.data == null ? 0 : data.data.recordTotals,
                        recordsTotal = data.data == null ? 0 : data.data.recordTotals,
                        data = data.data == null ? new List<object>() : data.data.data == null ? new List<object>() : data.data.data
                    }
                );
                }
            catch (Exception Ex)
            {
                throw;
            }
        }

        #endregion

        #region View
        public async Task<ActionResult> View(int id)
        {
            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:BNICloudReport:view"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { id = id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            //string result = "{\"code\":1,\"message\":\"sukses\",\"data\":{\"id\":2,\"Jns_Laporan\":\"2\",\"Dvs_Pengguna\":\"2\"}}";
            var jsonResult = JsonConvert.DeserializeObject<ServiceResult2<DataBNICLoud>>(result);
            ViewBag.Id = jsonResult.data.id;

            //Update isNewInboxMsg
            /* var isCabang = int.Parse(HttpContext.Session.GetString(SessionConstan.isCabang));
             var urlReset = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:BNICloudReport:resetInbox"];
             (bool resultApiReset, string resultReset) = RequestToAPI.PostRequestToWebApi(urlReset, new { isCabang = isCabang }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
             var jsonResultReset = JsonConvert.DeserializeObject<ResetInbox>(resultReset);
             //var jsonResultReset = "false";
             HttpContext.Session.SetString(SessionConstan.isNewInboxMsg, jsonResultReset.data);*/

            return PartialView("_View", jsonResult.data);
        }
        #endregion

        #region Delete
        public ActionResult Delete(string Ids)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:BNICloud:delete"];
                var idarray = Ids.Split(',').Select(int.Parse).ToList();
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url,
                    new
                    {
                        ids = idarray
                    },
                 HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Delete");
            }
            catch
            {
                return Content("gagal");
            }
        }
        #endregion

        #region Edit
        public ActionResult Edit(int id,string q, string KodeDivisi)
        {
            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:BNICloudReport:view"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { id = id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            //string result = "{\"code\":1,\"message\":\"sukses\",\"data\":{\"id\":2,\"Jns_Laporan\":\"2\",\"Dvs_Pengguna\":\"2\"}}";
            var jsonResult = JsonConvert.DeserializeObject<ServiceResult<DataBNICLoud>>(result);

            var urlGetDivPengguna = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownDivisiPengguna"];
            (bool resultApiDivPengguna, string resultGetDiv) = RequestToAPI.PostRequestToWebApi(urlGetDivPengguna, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResultDivPengguna = JsonConvert.DeserializeObject<List<DataDropdownDivPengguna>>(resultGetDiv);

            //ViewBag.DivisiPengguna = new SelectList(Utility.SelectDivisiPengguna(_context, HttpContext.Session.GetString(SessionConstan.jwt_Token)), "id", "text"); //return View();
            ViewBag.DivisiPengguna = new SelectList(jsonResultDivPengguna, "kodeDivisi", "text", jsonResult.Data.divisiPenggunaKode);
            //-------------------------------------------------

            var urlGetJnsLaporan = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:DropdownJnsLaporanByKodeDivisi"];
            (bool resultApiJnsLaporan, string resultGetJnsLaporan) = RequestToAPI.PostRequestToWebApi(urlGetJnsLaporan, new { q = q,kodeDivisi = KodeDivisi }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResultJnsLaporan = JsonConvert.DeserializeObject<List<DataDropdownServerSide>>(resultGetJnsLaporan);
            ViewBag.JenisLaporan = new SelectList(jsonResultJnsLaporan, "id","text", jsonResult.Data.jenisLaporanId);
            //ViewBag.JenisLaporan = new SelectList(Utility.SelectJenisLaporan("JnsLaporan", _context, HttpContext.Session.GetString(SessionConstan.jwt_Token)), "Value", "Name");


            //ViewBag.JenisLaporan = new SelectList(Utility.SelectJenisLaporan("JnsLaporan", _context, HttpContext.Session.GetString(SessionConstan.jwt_Token)).ToList(), "Value", "Name", jsonResult.Data.jenisLaporanId);
            //ViewBag.DivisiPengguna = new SelectList(Utility.SelectDivisiPengguna(_context, HttpContext.Session.GetString(SessionConstan.jwt_Token)).ToList(), "id", "text", jsonResult.Data.divisiPenggunaId);
            //return View();
            return PartialView("_EditRevice", jsonResult.Data);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SubmitEdit(DataBNICLoud model)
        {
            try
            {
                if (!lastSession.Update())
                {
                    return RedirectToAction("Login", "Login", new { a = true });
                }


                //string[] ArrayRoles = Roles.Split(',');
                //List<NavigationAssignment> listnav = new List<NavigationAssignment>();
                //foreach (var item in ArrayRoles)
                //{
                //    NavigationAssignment dataAssign = new NavigationAssignment();
                //    dataAssign.NavigationId = model.Id;
                //    dataAssign.RoleId = int.Parse(item);
                //    dataAssign.UpdatedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id));
                //    dataAssign.UpdatedTime = DateTime.Now;
                //    dataAssign.IsActive = true;
                //    listnav.Add(dataAssign);
                //}
                ////connect to API
                //var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:MasterMenu:edit"];
                //(bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url,
                //    new
                //    {
                //        Id = model.Id,
                //        Name = model.Name,
                //        Type = model.Type,
                //        ParentNavigationId = model.ParentNavigationId,
                //        Route = model.Route,
                //        IconClass = model.IconClass,
                //        Order = model.Order,
                //        Visible = model.Visible,
                //        UpdateById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id)),
                //        UpdateTime = DateTime.Now,
                //        NavigationAssignment = listnav
                //    }
                //    , HttpContext.Session.GetString(SessionConstan.jwt_Token));
                
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:BNICloudReport:revise"];
                (bool resultApi, string result) = RequestToAPI.PostFormDataToWebApi(url, /*model*/
                      new
                      {
                          laporanId = model.id,
                          jnsLaporanId = model.jenisLaporanId,
                          kodeDivisiPengguna = model.divisiPenggunaId,
                          fileLaporan = model.file
                      }
                    , HttpContext.Session.GetString(SessionConstan.jwt_Token)
                    );

                //bool resultApi = true;
                //string result = "ada";

                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Update");
            }
            catch (Exception ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion

        #region Download
        public async Task<JsonResult> Download(int? id)
        {
            //if (!lastSession.Update())
            //{
            //    return RedirectToAction("Login", "Login");
            //}

            //var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:BNICloudReport:download"];
            //(bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { Id = id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));

            //var jsonResult = JsonConvert.DeserializeObject<DownloadBNICloud>(result);

            ////var uri = new System.Uri("c:\\foo");
            ////var converted = uri.AbsoluteUri;
            ////return Content("");
            ////string Folder = Path.Combine(jsonResult.data);
            ////string file = System.IO.Path.Combine(Folder, url);
            //var bytes = System.IO.File.ReadAllBytes(jsonResult.data);
            ////FileStream fs = new FileStream(file, FileMode.Open);
            ////return File(fs, "application/jpeg", "Report");
            //return File(bytes, "image/png", Path.GetFileName(jsonResult.data));
            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:BNICloudReport:view"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { Id = id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResult = JsonConvert.DeserializeObject<ServiceResult<DataBNICLoud>>(result);

            //string pth = jsonResult.Data.laporanPathDownload.Replace(_configuration["laporanPathDownload"], _configuration["url"]);
            string Folder = Path.Combine(jsonResult.Data.laporanPathDownload);
            string file = System.IO.Path.Combine(Folder);
            return Json(new { path = file });
        }
        #endregion

        #region ExportExcelLaporan
        [HttpPost]
        public async Task<JsonResult> ExportExcelLaporan() {

            //var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

            //var draw = dict["draw"];

            ////Untuk mengetahui info paging dari datatable
            //var start = dict["start"];
            //var length = dict["length"];

            ////Server side datatable hanya support untuk mendapatkan data mulai ke berapa, untuk mengirim row ke berapa
            ////Kita perlu membuat logika sendiri
            ////var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;

            ////Untuk mengetahui info order column datatable
            //var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
            //var sortColumnDir = dict["order[0][dir]"];

            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:BNICloudReport:exportExcelLaporan"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                   url,
                   new
                   {
                       isCabang = 1
                   },
                   HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResult = JsonConvert.DeserializeObject<DownloadBNICloud>(result);
            ViewBag.ExportExcelLaporan = jsonResult.data;
            string Folder = Path.Combine(jsonResult.data);
            string file = System.IO.Path.Combine(Folder);

            return Json(new { path = file });

        }
        #endregion

        #region ExportLogLaporan
        public async Task<JsonResult> ExportExcelLogLaporan(int LaporanId)
        {

            //var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

            //var draw = dict["draw"];

            ////Untuk mengetahui info paging dari datatable
            //var start = dict["start"];
            //var length = dict["length"];

            ////Server side datatable hanya support untuk mendapatkan data mulai ke berapa, untuk mengirim row ke berapa
            ////Kita perlu membuat logika sendiri
            ////var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;

            ////Untuk mengetahui info order column datatable
            //var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
            //var sortColumnDir = dict["order[0][dir]"];

            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:BNICloudReport:exportExcelLogLaporan"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                   url,
                   new
                   {
                       laporanId = LaporanId
                   },
                   HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResult = JsonConvert.DeserializeObject<DownloadBNICloud>(result);
            ViewBag.ExportExcelLogLaporan = jsonResult.data;
            string Folder = Path.Combine(jsonResult.data);
            string file = System.IO.Path.Combine(Folder);

            return Json(new { path = file });

        }
        #endregion
    }
}
