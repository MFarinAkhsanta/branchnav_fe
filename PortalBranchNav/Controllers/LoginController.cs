﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DigiSalesWebPortal.Interface;
using DigiSalesWebPortal.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PortalBranchNav.Component;
using PortalBranchNav.Models.dbBranchNav;
using PortalBranchNav.ViewModels;
using UAParser;

namespace PortalBranchNav.Controllers
{
    public class LoginController : Controller
    {

        private readonly dbBranchNavContext _context;
        private readonly IConfiguration _configuration;
        IHttpContextAccessor _accessor;
        private readonly LastSessionLog lastSession;
        private readonly ITokenManager _tokenManager;

        public static bool isLogout = false;
        public LoginController(IConfiguration config, dbBranchNavContext context, IHttpContextAccessor accessor,
            ITokenManager tokenManager)
        {
            _context = context;
            _configuration = config;
            _accessor = accessor;
            lastSession = new LastSessionLog(accessor, context, config);
            _tokenManager = tokenManager;
        }


        public IActionResult Login(bool? a)
        {
            if (HttpContext.Session.GetString(SessionConstan.Session_Nama_Pegawai) == null)
            {

                ViewBag.Tahun = DateTime.Now.Year;
                ViewBag.Islogout = a;
                return View();
            }
            else
            {
                lastSession.Update();
                return RedirectToAction("Index", "Home");
            }
        }

        public bool CekSession()
        {
            bool ret = false;
            if (_accessor.HttpContext.Session.GetString(SessionConstan.Session_User_Id) != null)
            {
                ret = true;
            }
            else
            {
                ret = false;
            }
            return ret;
        }

        public int CekIsCabang()
        {
            var ret = 0;
            var isCabangStatus = int.Parse(HttpContext.Session.GetString(SessionConstan.isCabang));
            if (isCabangStatus == 0)
            {
                ret = isCabangStatus;
            }
            else
            {
                //var isCabang = int.Parse(HttpContext.Session.GetString(SessionConstan.isCabang));
                var urlReset = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:BNICloudReport:resetInbox"];
                (bool resultApiReset, string resultReset) = RequestToAPI.PostRequestToWebApi(urlReset, new { isCabang = isCabangStatus }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                var jsonResultReset = JsonConvert.DeserializeObject<ResetInbox>(resultReset);
                //var jsonResultReset = "false";
                HttpContext.Session.SetString(SessionConstan.isNewInboxMsg, jsonResultReset.data);

                ret = isCabangStatus;
            }
            return ret;
        }

        public bool CekInbox()
        {
            var isInbox = bool.Parse(HttpContext.Session.GetString(SessionConstan.isNewInboxMsg));
            //var isInbox = false;
            if (isInbox == true)
            {
                return isInbox;
            }
            else
            {
                return isInbox;
            }
            
        }

        public int CekRole() {
            var Role_Id = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Role_Id));
            if (Role_Id == 14)
            {
                return Role_Id;
            }
            else if (Role_Id == 15)
            {
                return Role_Id;
            }
            else {
                return 0;
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> Login(Login_ViewModels model)
        {
            bool LoginAllowed = false;

            string IPAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            var authModel = new AuthenticationViewModel { Username = model.Username, Password = model.Password, ClientId = "", IpAddress = IPAddress };
            var urlAuth = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:loginnonsso"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(urlAuth, authModel);
            if (resultApi && !string.IsNullOrEmpty(result))
            {
                var jwt = JsonConvert.DeserializeObject<JWT_Response_ViewModel>(result);
                if (jwt.code != -1)
                {

                    HttpContext.Session.SetString(SessionConstan.jwt_Token, jwt.data.jwT_Token);
                    HttpContext.Session.SetString(SessionConstan.jwt_Refresh, jwt.data.refresh_Token);
                    HttpContext.Session.SetString(SessionConstan.isCabang, jwt.data.isCabang);
                    HttpContext.Session.SetString(SessionConstan.isNewInboxMsg, jwt.data.isNewInboxMsg);
                    LoginAllowed = true;
                }
                else
                {
                    ViewBag.ErrorMessage += jwt.message;
                }
            }
            else
            {
                ViewBag.ErrorMessage += result;
            }

            if (LoginAllowed)
            {
                if (String.IsNullOrEmpty(HttpContext.Session.GetString(SessionConstan.jwt_Token)))
                {
                    return View();
                }

                var data = _tokenManager.GetPrincipal(HttpContext.Session.GetString(SessionConstan.jwt_Token));

                List<NavigationVM> ListNav = new List<NavigationVM>();
                var resultDropdownAPI = new ServiceResult<List<NavigationVM>>();

                //ListNav = StoredProcedureExecutor.ExecuteSPList<NavigationVM>(_context, "sp_GetMenu", new SqlParameter[]{
                //        new SqlParameter("@Role_Id", data.Role_Id)});
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:GetMenuByRoleId"];
                (bool resultApi2, string result2) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        role_id = data.Role_Id
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi2 && !string.IsNullOrEmpty(result2))
                {
                    ListNav = JsonConvert.DeserializeObject<List<NavigationVM>>(result2);
                }
                //ListNav = resultDropdownAPI.Data;
                if (ListNav == null)
                {
                    ViewBag.ErrorMessage += string.Format("Anda tidak mempunyai ijin akses aplikasi ini!");
                    return View();
                }

                if (ListNav.Count == 0)
                {
                    return RedirectToAction("NotAccess", "Error");
                }

                HttpContext.Session.SetString(SessionConstan.Session_Nama_Pegawai, data.Nama_Pegawai == null ? "-" : data.Nama_Pegawai);
                HttpContext.Session.SetString(SessionConstan.Session_NPP_Pegawai, data.Npp == null ? "" : data.Npp);
                HttpContext.Session.SetString(SessionConstan.Session_Pegawai_Id, data.Pegawai_Id == null ? "" : data.Pegawai_Id);
                HttpContext.Session.SetString(SessionConstan.Session_Unit_Id, data.Unit_Id == null ? "" : data.Unit_Id);
                HttpContext.Session.SetString(SessionConstan.Session_Nama_Unit, data.Nama_Unit == null ? "-" : data.Nama_Unit);
                HttpContext.Session.SetString(SessionConstan.Session_User_Id, data.User_Id == null ? "" : data.User_Id);
                HttpContext.Session.SetString(SessionConstan.Session_Role_Id, data.Role_Id == null ? "" : data.Role_Id);
                HttpContext.Session.SetString(SessionConstan.Session_Role_Unit_Id, data.Role_Unit_Id == null ? "" : data.Role_Unit_Id);
                HttpContext.Session.SetString(SessionConstan.Session_Role_Nama_Unit, data.Role_Nama_Unit == null ? "-" : data.Role_Nama_Unit);
                HttpContext.Session.SetString(SessionConstan.Session_Nama_Role, data.Nama_Role == null ? "-" : data.Nama_Role);
                HttpContext.Session.SetString(SessionConstan.Session_Status_Role, data.Status_Role == null ? "-" : data.Status_Role);
                HttpContext.Session.SetString(SessionConstan.Session_User_Role_Id, data.User_Role_Id == null ? "-" : data.User_Role_Id);
                HttpContext.Session.SetObject("AllMenu", ListNav);

                //Masukkan ke dalam table Log Activity
                var userAgent = _accessor.HttpContext.Request.Headers["User-Agent"];
                string uaString = Convert.ToString(userAgent[0]);
                var uaParser = Parser.GetDefault();
                ClientInfo c = uaParser.Parse(uaString);
                
                return RedirectToAction("Index", "Home");
            }
            else
            {
                //Masukkan ke dalam table Log Activity
                var userAgent = _accessor.HttpContext.Request.Headers["User-Agent"];
                string uaString = Convert.ToString(userAgent[0]);
                var uaParser = Parser.GetDefault();
                ClientInfo c = uaParser.Parse(uaString);

                return View();
            }
        }


        public ActionResult Logout()
        {
            isLogout = true;
            HttpContext.Session.Clear();

            return RedirectToAction("Login");
        }
    }
}
