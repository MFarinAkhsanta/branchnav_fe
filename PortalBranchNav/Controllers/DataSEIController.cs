﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using PortalBranchNav.Component;
using PortalBranchNav.Models.dbBranchNav;
using PortalBranchNav.ViewModels;


namespace PortalBranchNav.Controllers
{
    public class DataSEIController : Controller
    {
        private readonly dbBranchNavContext _context;
        private readonly IConfiguration _configuration;
        private readonly LastSessionLog lastSession;
        private readonly AccessSecurity accessSecurity;

        public static List<DataSEI_ViewModels> ListDataSEITemp = new List<DataSEI_ViewModels>();

        public DataSEIController(IConfiguration config, dbBranchNavContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _configuration = config;
            lastSession = new LastSessionLog(accessor, context, config);
            accessSecurity = new AccessSecurity(accessor, context, config);
        }



        public IActionResult Index()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            var location = new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}");
            string Path = location.AbsolutePath;

            if (!accessSecurity.IsGetAccess(".." + Path))
            {
                return RedirectToAction("NotAccess", "Error");
            }

            ViewBag.CurrentPath = Path;

            return View();
        }

     

        #region LoadData
        [HttpPost]
        public IActionResult LoadData()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

                var draw = dict["draw"];

                //Untuk mengetahui info paging dari datatable
                var start = dict["start"];
                var length = dict["length"];

                //Server side datatable hanya support untuk mendapatkan data mulai ke berapa, untuk mengirim row ke berapa
                //Kita perlu membuat logika sendiri
                var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;

                //Untuk mengetahui info order column datatable
                var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                var sortColumnDir = dict["order[0][dir]"];
                var CodeSearchParam = dict["columns[2][search][value]"];
                var KodeOutletSearchParam = dict["columns[3][search][value]"];

                //Untuk mengetahui info jumlah page dan total skip data
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                List<DataAuditcix_ViewModels> list = new List<DataAuditcix_ViewModels>();

                //list = StoredProcedureExecutor.ExecuteSPList<DataAuditcix_ViewModels>(_context, "sp_Data_BRO_View", new SqlParameter[]{
                //        new SqlParameter("@KodeOutlet", KodeOutletSearchParam),
                //        new SqlParameter("@Code", CodeSearchParam),
                //        new SqlParameter("@sortColumn", sortColumn),
                //        new SqlParameter("@sortColumnDir", sortColumnDir),
                //        new SqlParameter("@PageNumber", pageNumber),
                //        new SqlParameter("@RowsPage", pageSize)});

                //recordsTotal = StoredProcedureExecutor.ExecuteScalarInt(_context, "sp_Data_BRO_Count", new SqlParameter[]{
                //       new SqlParameter("@KodeOutlet", KodeOutletSearchParam),
                //        new SqlParameter("@Code", CodeSearchParam)});

                if (list == null)
                {
                    list = new List<DataAuditcix_ViewModels>();
                    recordsTotal = 0;
                }
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = list });
            }
            catch (Exception Ex)
            {
                throw;
            }
        }
        #endregion

        #region Load Data Temp Import
        public ActionResult LoadDataTemp()
        {

            return Json(new { data = ListDataSEITemp });
        }
        #endregion

        #region Import Data
        public ActionResult Import()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login");
            }
            ListDataSEITemp = new List<DataSEI_ViewModels>();


            return PartialView("_Import");
        }
        #endregion

        #region Create
        public ActionResult Create()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login");
            }
            
            return PartialView("_Create");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SubmitCreate(TblDataAuditcix model)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                if (!lastSession.Update())
                {
                    return RedirectToAction("Login", "Login", new { a = true });
                }

                using (TransactionScope trx = new TransactionScope())
                {
                    model.CreatedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id));
                    //model. = DateTime.Now;
                    _context.TblDataAuditcix.Add(model);
                    _context.SaveChanges();

                    trx.Complete();
                }



                return Content("");
            }
            catch (Exception Ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }

        #endregion

        #region Edit
        public ActionResult Edit(int id)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });

            }
            TblDataAuditcix data = _context.TblDataAuditcix.Where(m => m.Id == id).FirstOrDefault();
            if (data == null)
            {
                data = new TblDataAuditcix();
            }

            return PartialView("_Edit", data);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SubmitEdit(TblDataAuditcix model)
        {
            try
            {
                if (!lastSession.Update())
                {
                    return RedirectToAction("Login", "Login", new { a = true });
                }
               
                //_context.Entry(data).State = EntityState.Modified;
                _context.SaveChanges();

                return Content("");
            }
            catch
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion

        #region View
        public ActionResult View(int id)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });

            }
            TblDataAuditcix data = _context.TblDataAuditcix.Where(m => m.Id == id).FirstOrDefault();
            if (data == null)
            {
                data = new TblDataAuditcix();
            }


            return PartialView("_View", data);
        }

        #endregion

        #region Delete
        public ActionResult Delete(string Ids)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                int[] confirmedDeleteId = Ids.Split(',').Select(int.Parse).ToArray();

                List<TblDataAuditcix> Transaksis = _context.TblDataAuditcix.Where(x => confirmedDeleteId.Contains(x.Id)).ToList(); //Ambil data sesuai dengan ID
                for (int i = 0; i < confirmedDeleteId.Length; i++)
                {
                    TblDataAuditcix data = _context.TblDataAuditcix.Find(Transaksis[i].Id);
                    data.IsDeleted = true; //Jika true data tidak akan ditampilkan dan data masih tersimpan di dalam database
                    data.DeletedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id));
                    //data.DeletedTime = System.DateTime.Now;
                    _context.Entry(data).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                return Content("");
            }
            catch
            {
                return Content("gagal");
            }
        }
        #endregion
        #region
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ImportFile(ImportFile_ViewModels model)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Account");
            }

            try
            {
                if (model.file == null && model.file.Length == 0)
                {
                    return Content("Masukkan file terlebih dahulu");
                }

                ListDataSEITemp = new List<DataSEI_ViewModels>();


                var RowErrorKe = 0;

                try
                {

                    //Ambil ext file yang diijinkan
                    TblSystemParameter AllowExtValue = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "AllowedFileImportType");
                    TblSystemParameter MaxFileValue = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "MaxFileImportUploadSize");
                    TblSystemParameter PathFolderFileImport = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "PathFolderFileImport");

                    string AllowedFileUploadType = AllowExtValue.Value;
                    string PathFolder = PathFolderFileImport.Value;
                    decimal MaxFileUploadSize = decimal.Parse(MaxFileValue.Value);
                    decimal SizeFile = model.file.Length / 1000000;


                    string Ext = Path.GetExtension(model.file.FileName);

                    //Validate Upload
                    if (!AllowedFileUploadType.Contains(Ext))
                    {
                        return Content(GetConfig.AppSetting["AppSettings:GlobalMessage:ImportDataTypeTidakSesuai"]);
                    }

                  
                    string SubPath = PathFolder + "DataBRO/";

                    if (!Directory.Exists(SubPath))
                    {
                        Directory.CreateDirectory(SubPath);
                    }


                    if (model.file.Length > 0)
                    {
                        string sFileExtension = Path.GetExtension(model.file.FileName).ToLower();
                        ISheet sheet;
                        string fullPath = Path.Combine(SubPath, model.file.FileName);
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            model.file.CopyTo(stream);
                            stream.Position = 0;
                            if (sFileExtension == ".xls")
                            {
                                HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //This will read the Excel 97-2000 formats  
                                sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook  
                            }
                            else
                            {
                                XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //This will read 2007 Excel format  
                                sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook   
                            }
                        }


                        using (TransactionScope trx = new TransactionScope())
                        {

                            for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                            {
                                RowErrorKe = i;
                                IRow row = sheet.GetRow(i);
                                if (row == null) continue;
                                if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;

                                DataSEI_ViewModels data = new DataSEI_ViewModels();

                                data.KodeOutlet = row.GetCell(0).ToString();
                                data.NamaUnit = row.GetCell(1).ToString();
                                data.Dimensi1 = decimal.Parse(row.GetCell(2).ToString());
                                data.Dimensi2 = decimal.Parse(row.GetCell(3).ToString());
                                data.OutletDimensi3 = decimal.Parse(row.GetCell(4).ToString());
                                data.CabangDimensi3 = decimal.Parse(row.GetCell(5).ToString());
                                data.WilayahDimensi3 = decimal.Parse(row.GetCell(6).ToString());
                                data.ScoreSeioutlet = decimal.Parse(row.GetCell(7).ToString());
                                data.ScoreSeicabang = decimal.Parse(row.GetCell(8).ToString());
                                data.ScoreSeiwilayah = decimal.Parse(row.GetCell(9).ToString());
                                ListDataSEITemp.Add(data);
                            }
                            trx.Complete();
                        }
                    }
                    return Content("");

                }
                catch (Exception Ex)
                {
                    return Content("Error Row ke -" + RowErrorKe + Ex.Message);
                }


            }
            catch (Exception Ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion
    }
}
