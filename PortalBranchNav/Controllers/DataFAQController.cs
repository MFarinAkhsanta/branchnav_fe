﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using PortalBranchNav.Component;
using PortalBranchNav.Models.dbBranchNav;
using PortalBranchNav.ViewModels;

namespace PortalBranchNav.Controllers
{
    public class DataFAQController : Controller
    {
        private readonly dbBranchNavContext _context;
        private readonly IConfiguration _configuration;
        private readonly LastSessionLog lastSession;
        private readonly AccessSecurity accessSecurity;
        private Utility Utility = new Utility();
        private IHostingEnvironment _env;

        public static List<DataFAQ_ViewModels> ListDataSewaATMTemp = new List<DataFAQ_ViewModels>();

        public DataFAQController(IHostingEnvironment env, IConfiguration config, dbBranchNavContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _configuration = config;
            lastSession = new LastSessionLog(accessor, context, config);
            accessSecurity = new AccessSecurity(accessor, context, config);
            _env = env;
        }

        public IActionResult Index()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            var location = new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}");
            string Path = location.AbsolutePath;

            if (!accessSecurity.IsGetAccess(".." + Path))
            {
                return RedirectToAction("NotAccess", "Error");
            }

            ViewBag.CurrentPath = Path;
            return View();
        }

        #region LoadData
        [HttpPost]
        public IActionResult LoadData()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

                var draw = dict["draw"];

                //Untuk mengetahui info paging dari datatable
                var start = dict["start"];
                var length = dict["length"];

                //Server side datatable hanya support untuk mendapatkan data mulai ke berapa, untuk mengirim row ke berapa
                //Kita perlu membuat logika sendiri
                var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;

                //Untuk mengetahui info order column datatable
                var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                var sortColumnDir = dict["order[0][dir]"];
                var txtKategoriSearchParam = dict["columns[2][search][value]"];
                var txtPertanyaanSearchParam = dict["columns[3][search][value]"];
                var txtJawabanSearchParam = dict["columns[4][search][value]"];

                //Untuk mengetahui info jumlah page dan total skip data
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var data = new DataTableViewModel();

                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataFAQ:get"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        pageNumber = pageNumber,
                        pageSize = pageSize,
                        sortColumn = sortColumn,
                        sortColumnDir = sortColumnDir,
                        kategori = String.IsNullOrEmpty(txtKategoriSearchParam) ? null : txtKategoriSearchParam,
                        pertanyaan = String.IsNullOrEmpty(txtPertanyaanSearchParam) ? null : txtPertanyaanSearchParam,
                        jawaban = String.IsNullOrEmpty(txtJawabanSearchParam) ? null : txtJawabanSearchParam
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    data = JsonConvert.DeserializeObject<DataTableViewModel>(result);
                }
                return Json(
                    new
                    {
                        draw = draw,
                        recordsFiltered = data.data == null ? 0 : data.data.recordTotals,
                        recordsTotal = data.data == null ? 0 : data.data.recordTotals,
                        data = data.data == null ? new List<object>() : data.data.data == null ? new List<object>() : data.data.data
                    }
                );
            }
            catch (Exception Ex)
            {
                throw;
            }
        }
        #endregion

        #region Load Data Temp Import
        public ActionResult LoadDataTemp()
        {

            return Json(new { data = ListDataSewaATMTemp });
        }
        #endregion

        #region Import Data
        public ActionResult Import()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login");
            }
            ListDataSewaATMTemp = new List<DataFAQ_ViewModels>();


            return PartialView("_Import");
        }
        #endregion

        #region Create
        public ActionResult Create()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login");
            }

            return PartialView("_Create");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SubmitCreate(TblDataFaq model)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                if (!lastSession.Update())
                {
                    return RedirectToAction("Login", "Login", new { a = true });
                }

                using (TransactionScope trx = new TransactionScope())
                {
                    model.CreatedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id));
                    //model. = DateTime.Now;
                    _context.TblDataFaq.Add(model);
                    _context.SaveChanges();

                    trx.Complete();
                }



                return Content("");
            }
            catch (Exception Ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }

        #endregion

        #region Edit
        public ActionResult Edit(int id)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });

            }
            TblDataFaq data = _context.TblDataFaq.Where(m => m.Id == id).FirstOrDefault();
            ViewBag.KategoriName = data.Kategori;
            if (data == null)
            {
                data = new TblDataFaq();
                ViewBag.TypeKategori = new SelectList(Utility.SelectLookup("TypeKategori", _context, HttpContext.Session.GetString(SessionConstan.jwt_Token)), "OrderBy", "Name");
            }
            else
            {
                ViewBag.TypeKategori = new SelectList(Utility.SelectLookup("TypeKategori", _context, HttpContext.Session.GetString(SessionConstan.jwt_Token)), "OrderBy", "Name", data.KategoriId);
            }

            return PartialView("_Edit", data);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> SubmitEdit(TblDataFaq model)
        {
            try
            {
                if (!lastSession.Update())
                {
                    return RedirectToAction("Login", "Login", new { a = true });
                }
                int.TryParse(model.Kategori, out int result);

                if (result != 0)
                {
                    var typeKategori = await _context.TblLookup.Where(x => x.Value == result
                                                            && x.IsActive == true && x.IsDeleted == false
                                                            && x.Type == GetConfig.AppSetting["AppSettings:DataLookup:TypeKategori"])
                                                            .Select(x => x.Name).LastOrDefaultAsync().ConfigureAwait(false);
                    model.Kategori = typeKategori == null ? model.Kategori : typeKategori;
                    model.KategoriId = result;
                }
                else
                {
                    model.KategoriId = result;
                }
                model.IsDeleted = false;
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();

                return Content("");
            }
            catch
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion

        #region View
        public ActionResult View(int id)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });

            }
            TblDataFaq data = _context.TblDataFaq.Where(m => m.Id == id).FirstOrDefault();
            if (data == null)
            {
                data = new TblDataFaq();
            }


            return PartialView("_View", data);
        }

        #endregion

        #region Delete
        public ActionResult Delete(string Ids)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                int[] confirmedDeleteId = Ids.Split(',').Select(int.Parse).ToArray();

                List<TblDataFaq> Transaksis = _context.TblDataFaq.Where(x => confirmedDeleteId.Contains(x.Id)).ToList(); //Ambil data sesuai dengan ID
                for (int i = 0; i < confirmedDeleteId.Length; i++)
                {
                    TblDataFaq data = _context.TblDataFaq.Find(Transaksis[i].Id);
                    data.IsDeleted = true; //Jika true data tidak akan ditampilkan dan data masih tersimpan di dalam database
                    data.DeletedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id));
                    data.IsActive = false;
                    //data.DeletedTime = System.DateTime.Now;
                    _context.Entry(data).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                return Content("");
            }
            catch
            {
                return Content("gagal");
            }
        }
        #endregion

        #region Import File
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> ImportFile(ImportFile_ViewModels model)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Account");
            }

            try
            {
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataFAQ:upload"];
                (bool resultApi, string result) = RequestToAPI.PostFormDataToWebApi(url, model, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    var data = JsonConvert.DeserializeObject<ServiceResult<object>>(result);
                    if (data.Code == 1)
                        return Content("");
                    else
                        return Content(data.Code + ": " + data.Message);
                }
                return Content(result);
                //if (model.file == null && model.file.Length == 0)
                //{
                //    return Content("Masukkan file terlebih dahulu");
                //}

                //List<TblDataFaq> ListTemp = new List<TblDataFaq>();


                //var RowErrorKe = 0;

                //try
                //{

                //    //Ambil ext file yang diijinkan
                //    TblSystemParameter AllowExtValue = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "AllowedFileImportType");
                //    TblSystemParameter MaxFileValue = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "MaxFileImportUploadSize");
                //    TblSystemParameter PathFolderFileImport = _context.TblSystemParameter.SingleOrDefault(m => m.Key == "PathFolderFileImport");

                //    string AllowedFileUploadType = AllowExtValue.Value;
                //    string PathFolder = PathFolderFileImport.Value;
                //    decimal MaxFileUploadSize = decimal.Parse(MaxFileValue.Value);
                //    decimal SizeFile = model.file.Length / 1000000;


                //    string Ext = Path.GetExtension(model.file.FileName);

                //    //Validate Upload
                //    if (!AllowedFileUploadType.Contains(Ext))
                //    {
                //        return Content(GetConfig.AppSetting["AppSettings:GlobalMessage:ImportDataTypeTidakSesuai"]);
                //    }


                //    string SubPath = PathFolder + "DataFAQ/";
                //    //SubPath = SubPath.Replace("C", "D");

                //    if (!Directory.Exists(SubPath))
                //    {
                //        Directory.CreateDirectory(SubPath);
                //    }


                //    if (model.file.Length > 0)
                //    {
                //        string sFileExtension = Path.GetExtension(model.file.FileName).ToLower();
                //        ISheet sheet;
                //        string fullPath = Path.Combine(SubPath, model.file.FileName);
                //        using (var stream = new FileStream(fullPath, FileMode.Create))
                //        {
                //            model.file.CopyTo(stream);
                //            stream.Position = 0;
                //            if (sFileExtension == ".xls")
                //            {
                //                HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //This will read the Excel 97-2000 formats  
                //                sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook  
                //            }
                //            else
                //            {
                //                XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //This will read 2007 Excel format  
                //                sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook   
                //            }
                //        }


                //        using (TransactionScope trx = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                //        {

                //            for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                //            {
                //                RowErrorKe = i;
                //                IRow row = sheet.GetRow(i);
                //                if (row == null) continue;
                //                if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;

                //                if (row.GetCell(1).ToString() != "" && row.GetCell(2).ToString() != "" && row.GetCell(3).ToString() != "")
                //                {
                //                    TblDataFaq data = new TblDataFaq();
                //                    data.Kategori = row.GetCell(1).ToString();
                //                    var typeKategori = await _context.TblLookup.Where(x => x.Name == data.Kategori
                //                                        && x.IsActive == true && x.IsDeleted == false
                //                                        && x.Type == GetConfig.AppSetting["AppSettings:dataLookup:TypeKategori"])
                //                                        .Select(x => x.Value).LastOrDefaultAsync().ConfigureAwait(false);
                //                    data.KategoriId = typeKategori == null ? 0 : typeKategori;
                //                    data.Pertanyaan = row.GetCell(2).ToString();
                //                    data.Jawaban = row.GetCell(3).ToString();
                //                    data.CreatedTime = DateTime.Now;
                //                    data.CreatedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id));
                //                    data.IsActive = true;
                //                    data.IsDeleted = false;
                //                    ListTemp.Add(data);
                //                }

                //            }

                //            //string query = "update dbo.[Tbl_Data_Sewa_ATM] set IsActive = 0 where [IDATM] in (select * from dbo.uf_SplitString('" + dataKodeIDATM + "',';'))";
                //            //_context.Database.ExecuteSqlCommand(query);

                //            _context.TblDataFaq.AddRange(ListTemp);
                //            _context.SaveChanges();

                //            trx.Complete();

                //        }

                //    }
                //    return Content("");

                //}
                //catch (Exception Ex)
                //{
                //    return Content("Error Row ke -" + RowErrorKe + Ex.Message);
                //}
            }
            catch (Exception Ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion

        #region Get Templates
        public FileStreamResult GetTemplate()
        {
            string fileName = GetConfig.AppSetting["templateUpload:FAQ"];
            if (string.IsNullOrWhiteSpace(_env.WebRootPath))
            {
                _env.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
            }
            var webRoot = _env.WebRootPath;
            string Folder = Path.Combine(webRoot, "templates");
            string file = System.IO.Path.Combine(Folder, fileName);
            FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read);
            return File(fs, "application/vnd.ms-excel", fileName);
        }
        #endregion
    }
}