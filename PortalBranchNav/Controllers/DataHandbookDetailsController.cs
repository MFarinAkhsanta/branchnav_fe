﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PortalBranchNav.Component;
using PortalBranchNav.Models.dbBranchNav;
using PortalBranchNav.ViewModels;


namespace PortalBranchNav.Controllers
{
    public class DataHandbookDetailsController : Controller
    {
        private readonly dbBranchNavContext _context;
        private readonly IConfiguration _configuration;
        private readonly LastSessionLog lastSession;
        private readonly AccessSecurity accessSecurity;
        public DataHandbookDetailsController(IConfiguration config, dbBranchNavContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _configuration = config;
            lastSession = new LastSessionLog(accessor, context, config);
            accessSecurity = new AccessSecurity(accessor, context, config);
        }


        public IActionResult Index()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            var location = new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}");
            string Path = location.AbsolutePath;

            if (!accessSecurity.IsGetAccess(".." + Path))
            {
                return RedirectToAction("NotAccess", "Error");
            }

            ViewBag.CurrentPath = Path;

            return View();
        }
        #region LoadData
        [HttpPost]
        public IActionResult LoadData()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

                var draw = dict["draw"];

                //Untuk mengetahui info paging dari datatable
                var start = dict["start"];
                var length = dict["length"];

                //Server side datatable hanya support untuk mendapatkan data mulai ke berapa, untuk mengirim row ke berapa
                //Kita perlu membuat logika sendiri
                var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;

                //Untuk mengetahui info order column datatable
                var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                var sortColumnDir = dict["order[0][dir]"];
                var HandbookSearchParam = dict["columns[2][search][value]"];
                var JudulSearchParam = dict["columns[3][search][value]"];
                var KeywordSearchParam = dict["columns[6][search][value]"];

                //Untuk mengetahui info jumlah page dan total skip data
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                var data = new DataTableViewModel<DataHandbookDetails_ViewModels>();

                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataHandbookDetail:get"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        pageNumber =  1,
                        pageSize = pageSize,
                        sortColumn = sortColumn,
                        sortColumnDir = sortColumnDir,
                        handbook = String.IsNullOrEmpty(HandbookSearchParam) ? null : HandbookSearchParam,
                        judul = String.IsNullOrEmpty(JudulSearchParam) ? null : JudulSearchParam,
                        keyword = String.IsNullOrEmpty(KeywordSearchParam) ? null : KeywordSearchParam
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    data = JsonConvert.DeserializeObject<DataTableViewModel<DataHandbookDetails_ViewModels>>(result);
                }

                List<DataHandbookDetails_ViewModels> output = new List<DataHandbookDetails_ViewModels>();
                if (data.data.data != null)
                {
                    for (int i = (pageNumber - 1) * 10; i < (pageNumber * pageSize); i++)
                    {
                        if (i >= data.data.data.Count)
                        {
                            break;
                        }
                        output.Add(data.data.data[i]);

                    }
                }

                return Json(
                    new
                    {
                        draw = draw,
                        recordsFiltered = data.data.data == null ? 0 : data.data.data.Count,
                        recordsTotal = data.data.data == null ? 0 : data.data.data.Count,
                        data = data.data.data == null ? new List<DataHandbookDetails_ViewModels>() : data.data.data == null ? new List<DataHandbookDetails_ViewModels>() : output
                    }
                );

            }
            catch (Exception Ex)
            {
                throw;
            }
        }
        #endregion

        #region LoadDataFileUpload
        [HttpPost]
        public IActionResult LoadDataFileUpload(int Id, string ListIdAttachment)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());

                var draw = dict["draw"];

                //Untuk mengetahui info paging dari datatable
                var start = dict["start"];
                var length = dict["length"];

                //Server side datatable hanya support untuk mendapatkan data mulai ke berapa, untuk mengirim row ke berapa
                //Kita perlu membuat logika sendiri
                var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;

                //Untuk mengetahui info order column datatable
                var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                var sortColumnDir = dict["order[0][dir]"];
                var FileNameSearchParam = dict["columns[3][search][value]"];
                var UploaderSearchParam = dict["columns[4][search][value]"];

                //Untuk mengetahui info jumlah page dan total skip data
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                List<DataHandbookDetails_Attachment__API_ViewModel> list = new List<DataHandbookDetails_Attachment__API_ViewModel>();

                if (Id != 0)
                {
                   var data = new DataTableViewModel<DataHandbookDetails_Attachment__API_ViewModel>();

                    //connect to API
                    var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataHandbookDetail:getFile"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                        url,
                        new
                        {
                            pageNumber = pageNumber,
                            pageSize = pageSize,
                            sortColumn = sortColumn,
                            sortColumnDir = sortColumnDir,
                            fileName = String.IsNullOrEmpty(FileNameSearchParam) ? null : FileNameSearchParam,
                            uploader = String.IsNullOrEmpty(UploaderSearchParam) ? null : UploaderSearchParam,
                            idHandbookDetail = Id
                        },
                        HttpContext.Session.GetString(SessionConstan.jwt_Token));
                    if (resultApi && !string.IsNullOrEmpty(result))
                    {
                        data = JsonConvert.DeserializeObject<DataTableViewModel<DataHandbookDetails_Attachment__API_ViewModel>>(result);
                    }
                    recordsTotal = data.data.recordTotals;
                    list = data.data.data;

                    ListIdAttachment = HttpContext.Session.GetString(SessionConstan.Session_ListAttachmentHD);
                    //List<DataTableViewModel<DataHandbookDetails_Attachment_ViewModel>> dataListAttachment = new DataTableViewModel<DataHandbookDetails_Attachment_ViewModel>();
                    var dataListAttachment = new DataTableViewModel<DataHandbookDetails_Attachment__API_ViewModel>();
                    
                    if (ListIdAttachment != null && ListIdAttachment != "")
                    {
                        dataListAttachment = new DataTableViewModel<DataHandbookDetails_Attachment__API_ViewModel>();
                        var urlIdsAttachment = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataHandbookDetail:getFilebyIds"];
                        (bool resultApiAttachment, string resultAttachment) = RequestToAPI.PostRequestToWebApi(
                        urlIdsAttachment,
                        new
                        {
                            Ids = ListIdAttachment
                        },
                        HttpContext.Session.GetString(SessionConstan.jwt_Token));
                        if (resultApiAttachment && !string.IsNullOrEmpty(resultAttachment))
                        {
                            dataListAttachment = JsonConvert.DeserializeObject<DataTableViewModel<DataHandbookDetails_Attachment__API_ViewModel>>(resultAttachment);
                        }
                        //Untuk sinkronisasiData yang sudah masuk ke db dengan yang masih belum memiliki idHandbookDetail
                        data.data.data = dataListAttachment.data.data;
                        recordsTotal = data.data.recordTotals;
                        list = data.data.data;
                    }
                    else
                    {
                        if (data.data.data != null)
                        {
                            foreach (var dataAttachment in data.data.data)
                            {
                                ListIdAttachment = ListIdAttachment + dataAttachment.id.ToString() + ",";
                            }
                            //Untuk sinkronisasiData yang sudah masuk ke db dengan yang masih belum memiliki idHandbookDetail
                            HttpContext.Session.SetString(SessionConstan.Session_ListAttachmentHD, ListIdAttachment == null ? "" : ListIdAttachment); //Update list data File Attachment

                        }
                        else
                        {
                            HttpContext.Session.SetString(SessionConstan.Session_ListAttachmentHD, ""); //Update list data File Attachment
                        }
                    }
                }
                else
                {
                    //Untuk Load Data attachment yang belum memiliki Id HandbookDetail
                    ListIdAttachment = HttpContext.Session.GetString(SessionConstan.Session_ListAttachmentHD);
                    var dataListAttachment = new DataTableViewModel<DataHandbookDetails_Attachment__API_ViewModel>();
                    var urlIdsAttachment = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataHandbookDetail:getFilebyIds"];
                    (bool resultApiAttachment, string resultAttachment) = RequestToAPI.PostRequestToWebApi(
                    urlIdsAttachment,
                    new
                    {
                        Ids = ListIdAttachment
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                    if (resultApiAttachment && !string.IsNullOrEmpty(resultAttachment))
                    {
                        dataListAttachment = JsonConvert.DeserializeObject<DataTableViewModel<DataHandbookDetails_Attachment__API_ViewModel>>(resultAttachment);
                    }

                    if(dataListAttachment.data != null)
                    {
                        recordsTotal = dataListAttachment.data.data.Count;
                        list = dataListAttachment.data.data;
                    }
                }

                if (list == null || list.Count == 0)
                {
                    list = new List<DataHandbookDetails_Attachment__API_ViewModel>();
                    recordsTotal = 0;
                }

                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = list });
            }
            catch (Exception Ex)
            {
                throw;
            }
        }
        #endregion

        #region Create
        public async Task<ActionResult> Create()
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login");
            }

            var urlGetAllHandbook = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:GetAllHandbook"];
            (bool resultApiGetAllHandbook, string resultGetAllHandbook) = RequestToAPI.PostRequestToWebApi(urlGetAllHandbook, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResultGetAllHandbook = JsonConvert.DeserializeObject<DataTableViewModel<TblDataHandbook>>(resultGetAllHandbook);
            ViewBag.dataHandbookId = new SelectList(jsonResultGetAllHandbook.data.data, "Id", "Nama");

            var urlGetAllUnit = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:getAllUnit"];
            (bool resultApiGetAllUnit, string resultGetAllUnit) = RequestToAPI.PostRequestToWebApi(urlGetAllUnit, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResultGetAllUnit = JsonConvert.DeserializeObject<DataTableViewModel<TblUnit>>(resultGetAllUnit);
            ViewBag.KodeUnitId = new SelectList(jsonResultGetAllUnit.data.data, "KodeOutlet", "Name");

            HttpContext.Session.SetString(SessionConstan.Session_ListAttachmentHD, "");

            return PartialView("_Create");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> SubmitCreate(TblDataHandbookDetails model, string KodeUnits)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                var AttachmentIds = HttpContext.Session.GetString(SessionConstan.Session_ListAttachmentHD);
                if (AttachmentIds != "")
                {
                    AttachmentIds = AttachmentIds.Remove(AttachmentIds.Length - 1, 1);
                }
                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataHandbookDetail:create"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        handbookId = model.HandbookId,
                        judul = model.Judul,
                        kodeUnits = KodeUnits,
                        attachmentIds = AttachmentIds,
                        forAllUnit = model.ForAllUnit,
                        deskripsi = model.Deskripsi,
                        orderBy = model.OrderBy,
                        keyword = model.Keyword
                    }, 
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Save");
            }
            catch (Exception Ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }

        #endregion

        #region Edit
        public async Task<ActionResult> Edit(int id)
        {
            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataHandbookDetail:getById"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { id = id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResult = JsonConvert.DeserializeObject<ServiceResult<List<TblDataHandbookDetails>>>(result);

            var urlHandbookDetails = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataHandbookDetail:ViewHandbookDetailById"];
            (bool resultApi2, string result2) = RequestToAPI.PostRequestToWebApi(urlHandbookDetails, new { id = id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResult2 = JsonConvert.DeserializeObject<ServiceResult<TblDataHandbookDetails>>(result2);
            //TblDataHandbookDetails data = jsonResult.Data.Where(x => x.Id == id).LastOrDefault();

            var urlGetAllUnit = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:getAllUnit"];
            (bool resultApiGetAllUnit, string resultGetAllUnit) = RequestToAPI.PostRequestToWebApi(urlGetAllUnit, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResultGetAllUnit = JsonConvert.DeserializeObject<DataTableViewModel<TblUnit>>(resultGetAllUnit);
            
            var urlGetAllHandbook = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:GetAllHandbook"];
            (bool resultApiGetAllHandbook, string resultGetAllHandbook) = RequestToAPI.PostRequestToWebApi(urlGetAllHandbook, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResultGetAllHandbook = JsonConvert.DeserializeObject<DataTableViewModel<TblDataHandbook>>(resultGetAllHandbook);

            //return PartialView("_Edit", jsonResult.Data);
            if (jsonResult2.Data != null)
            {
                ViewBag.dataHandbookId = new SelectList(jsonResultGetAllHandbook.data.data, "Id", "Nama", jsonResult2.Data.HandbookId);

                var urlGetHandbookByKeyword = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataHandbookDetail:getByKeyword"];
                (bool resultApiGetHandbookByKeyword, string resultGetHandbookByKeyword) = RequestToAPI.PostRequestToWebApi(urlGetHandbookByKeyword, new { keyword = jsonResult2.Data.Keyword }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                var jsonResultGetHandbookByKeyword = JsonConvert.DeserializeObject<ServiceResult<List<TblDataHandbookDetails>>>(result);
                ViewBag.KodeUnitId = new SelectList(jsonResultGetAllUnit.data.data, "KodeOutlet", "Name");

                List<int> listDataOutlet = new List<int>();
                List<int> listUnitId = new List<int>();
                foreach(var list in jsonResultGetHandbookByKeyword.Data)
                {
                    var dataOutlet = jsonResultGetAllUnit.data.data.Where(m => m.KodeOutlet == list.ForKodeUnit).Select(m => m.Id).LastOrDefault();
                    listUnitId.Add(list.Id);

                    listDataOutlet.Add(dataOutlet);
                }

                ViewBag.KodeOutletId = String.Join(",", listDataOutlet.ToArray());
                ViewBag.IdHandbookDetail = String.Join(",", listUnitId.ToArray());
                jsonResult2.Data.ForKodeUnit = null; //agar tidak ikut terbawa di nilai unitnya ketika view / edit
            }
            else
            {
                ViewBag.dataHandbookId = new SelectList(jsonResultGetAllHandbook.data.data, "Id", "Nama", jsonResult2.Data.HandbookId);
                ViewBag.KodeUnitId = new SelectList(jsonResultGetAllUnit.data.data, "KodeOutlet", "Name");

                List<int> listDataOutlet = new List<int>();
                List<int> listUnitId = new List<int>();

                ViewBag.KodeOutletId = String.Join(",", listDataOutlet.ToArray());

                ViewBag.IdHandbookDetail = String.Join(",", listUnitId.ToArray());
            }
            HttpContext.Session.SetString(SessionConstan.Session_ListAttachmentHD, "");

            return PartialView("_Edit", jsonResult2.Data);

        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> SubmitEdit(TblDataHandbookDetails model, string KodeUnits, string listHandbookDetailId)
        {
            try
            {
                if (!lastSession.Update())
                {
                    return RedirectToAction("Login", "Login", new { a = true });
                }

                var AttachmentIds = HttpContext.Session.GetString(SessionConstan.Session_ListAttachmentHD);
                if (AttachmentIds != "")
                {
                    AttachmentIds = AttachmentIds.Remove(AttachmentIds.Length - 1, 1);
                }
                //connect to API
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataHandbookDetail:edit"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(
                    url,
                    new
                    {
                        handbookId = model.HandbookId,
                        judul = model.Judul,
                        kodeUnits = KodeUnits,
                        attachmentIds = AttachmentIds,
                        listHandbookDetailId = listHandbookDetailId,
                        forAllUnit = model.ForAllUnit,
                        deskripsi = model.Deskripsi,
                        orderBy = model.OrderBy,
                        keyword = model.Keyword
                    },
                    HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Save");

            }
            catch
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion

        #region View
        public async Task<ActionResult> View(int id)
        {
            var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataHandbookDetail:getById"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { id = id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResult = JsonConvert.DeserializeObject<ServiceResult<List<TblDataHandbookDetails>>>(result);

            //TblDataHandbookDetails data = jsonResult.Data.Where(x => x.Id == id).LastOrDefault();
            var urlHandbookDetails = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataHandbookDetail:ViewHandbookDetailById"];
            (bool resultApi2, string result2) = RequestToAPI.PostRequestToWebApi(urlHandbookDetails, new { id = id }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResult2 = JsonConvert.DeserializeObject<ServiceResult<TblDataHandbookDetails>>(result2);

            var urlGetAllUnit = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:getAllUnit"];
            (bool resultApiGetAllUnit, string resultGetAllUnit) = RequestToAPI.PostRequestToWebApi(urlGetAllUnit, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResultGetAllUnit = JsonConvert.DeserializeObject<DataTableViewModel<TblUnit>>(resultGetAllUnit);

            var urlGetAllHandbook = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Utility:GetAllHandbook"];
            (bool resultApiGetAllHandbook, string resultGetAllHandbook) = RequestToAPI.PostRequestToWebApi(urlGetAllHandbook, new { }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
            var jsonResultGetAllHandbook = JsonConvert.DeserializeObject<DataTableViewModel<TblDataHandbook>>(resultGetAllHandbook);

            //var ListHandbook = datagetHandbook.data.data;
            if (jsonResult2.Data != null)
            {
                ViewBag.dataHandbookId = new SelectList(jsonResultGetAllHandbook.data.data, "Id", "Nama", jsonResult2.Data.HandbookId);

                var urlGetHandbookByKeyword = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataHandbookDetail:getByKeyword"];
                (bool resultApiGetHandbookByKeyword, string resultGetHandbookByKeyword) = RequestToAPI.PostRequestToWebApi(urlGetHandbookByKeyword, new { keyword = jsonResult2.Data.Keyword }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                var jsonResultGetHandbookByKeyword = JsonConvert.DeserializeObject<ServiceResult<List<TblDataHandbookDetails>>>(result);
                ViewBag.KodeUnitId = new SelectList(jsonResultGetAllUnit.data.data, "KodeOutlet", "Name");

                List<int> listDataOutlet = new List<int>();
                List<int> listUnitId = new List<int>();
                
                foreach (var list in jsonResultGetHandbookByKeyword.Data)
                {
                    var dataOutlet = jsonResultGetAllUnit.data.data.Where(m => m.KodeOutlet == list.ForKodeUnit).Select(m => m.Id).LastOrDefault();
                    listUnitId.Add(list.Id);

                    listDataOutlet.Add(dataOutlet);
                }

                ViewBag.KodeOutletId = String.Join(",", listDataOutlet.ToArray());
                ViewBag.IdHandbookDetail = String.Join(",", listUnitId.ToArray());
                jsonResult2.Data.ForKodeUnit = null; //agar tidak ikut terbawa di nilai unitnya ketika view / edit
            }
            else
            {
                ViewBag.dataHandbookId = new SelectList(jsonResultGetAllHandbook.data.data, "Id", "Nama", jsonResult2.Data.HandbookId);
                
                ViewBag.KodeUnitId = new SelectList(jsonResultGetAllUnit.data.data, "KodeOutlet", "Name");

                List<int> listDataOutlet = new List<int>();
                List<int> listUnitId = new List<int>();

                ViewBag.KodeOutletId = String.Join(",", listDataOutlet.ToArray());

                ViewBag.IdHandbookDetail = String.Join(",", listUnitId.ToArray());
            }
            HttpContext.Session.SetString(SessionConstan.Session_ListAttachmentHD, "");

            return PartialView("_View", jsonResult2.Data);
        }

        #endregion

        #region Delete
        public async Task<ActionResult> Delete(string Id)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Login", new { a = true });
            }

            try
            {
                #region Not Used
                //var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:Lookup:delete"];
                //var idarray = Ids.Split(',').Select(int.Parse).ToList();
                //(bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url,
                //    new
                //    {
                //        ids = idarray
                //    },
                // HttpContext.Session.GetString(SessionConstan.jwt_Token));
                //if (resultApi && !string.IsNullOrEmpty(result))
                //{
                //    return Content("");
                //}
                //else
                //    return Content("Failed Delete");
                //int[] confirmedDeleteId = Id.Split(',').Select(int.Parse).ToArray();

                //List<TblDataHandbookDetails> Transaksis = _context.TblDataHandbookDetails.Where(x => confirmedDeleteId.Contains(x.Id)).ToList(); //Ambil data sesuai dengan ID
                //for (int i = 0; i < confirmedDeleteId.Length; i++)
                //{
                //    TblDataHandbookDetails data = _context.TblDataHandbookDetails.Find(Transaksis[i].Id);
                //    data.IsDeleted = true; //Jika true data tidak akan ditampilkan dan data masih tersimpan di dalam database
                //    data.DeletedById = int.Parse(HttpContext.Session.GetString(SessionConstan.Session_Pegawai_Id));
                //    data.DeletedTime = System.DateTime.Now;
                //    _context.Entry(data).State = EntityState.Modified;
                //    _context.SaveChanges();
                //}

                //int.TryParse(Id, out int result);
                //TblDataHandbookDetails data = await _context.TblDataHandbookDetails.Where(m => m.Id == result).LastOrDefaultAsync();


                //if (data != null)
                //{
                //    using (TransactionScope trx = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                //    {
                //        //List<TblDataHandbookDetails> listData = await _context.TblDataHandbookDetails.Where(m => m.Keyword == data.Keyword).ToListAsync().ConfigureAwait(false);
                //        //foreach (var dataHandbook in listData)
                //        //{
                //        //    _context.Remove(dataHandbook);
                //        //    await _context.SaveChangesAsync().ConfigureAwait(false);
                //        //}
                //        List<TblDataHandbookDetails> listData = await _context.TblDataHandbookDetails.Where(m => m.Keyword == data.Keyword).ToListAsync().ConfigureAwait(false);
                //        foreach (var cekData in listData)
                //        {
                //            List<TblDataHandbookAttachment> listAttachment = await _context.TblDataHandbookAttachment.Where(x => x.HandbookDetailsId == cekData.Id).ToListAsync().ConfigureAwait(false);
                //            if (listAttachment != null && listAttachment.Count != 0)
                //            {
                //                foreach (var cekDataAttachment in listAttachment)
                //                {
                //                    _context.TblDataHandbookAttachment.Remove(cekDataAttachment);
                //                    await _context.SaveChangesAsync().ConfigureAwait(false);
                //                }
                //            }

                //            _context.TblDataHandbookDetails.Remove(cekData);
                //            await _context.SaveChangesAsync().ConfigureAwait(false);
                //        }

                //        trx.Complete();
                //    }
                //}
                //else
                //{
                //    return Content("IdNull");
                //}

                //return Content("");
                //connect to API
                //int.TryParse(Id, out int resultParse);
                #endregion
                var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataHandbookDetail:delete"];
                var idarray = Id.Split(',').Select(int.Parse).ToList();
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url,
                new
                {
                    ids = idarray
                }, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                if (resultApi && !string.IsNullOrEmpty(result))
                {
                    return Content("");
                }
                else
                    return Content("Failed Delete");
            }
            catch
            {
                return Content("gagal");
            }
        }
        #endregion

        #region Submit Upload File
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> ImportFile(DataHandbookDetails_Attachment_ViewModel model)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Account");
            }

            try
            {
                if (model.FileHandbookDetail == null && model.FileHandbookDetail.Length == 0)
                {
                    return Content("Masukkan file terlebih dahulu");
                }
                try
                {
                    sendUploadFile_ViewModels sendToApi = new sendUploadFile_ViewModels
                    {
                        HandbookDetailsId = -1,
                        file = model.FileHandbookDetail
                    };
                    var url = GetConfig.AppSetting["baseApi"] + GetConfig.AppSetting["urlapi:DataHandbookDetail:upload"];
                    (bool resultApi, string result) = RequestToAPI.PostFormDataToWebApi(url, sendToApi, HttpContext.Session.GetString(SessionConstan.jwt_Token));
                    if (resultApi && !string.IsNullOrEmpty(result))
                    {
                        var data = JsonConvert.DeserializeObject<ServiceResult<List<int>>>(result);
                        if (data.Code == 1)
                        {
                            model.ListIdAttachment = HttpContext.Session.GetString(SessionConstan.Session_ListAttachmentHD);
                            model.ListIdAttachment = model.ListIdAttachment + Convert.ToString(data.Data[0]) + ",";
                            @ViewBag.HandbookAttachmentId = model.ListIdAttachment;
                            HttpContext.Session.SetString(SessionConstan.Session_ListAttachmentHD, model.ListIdAttachment == null ? "" : model.ListIdAttachment);
                            return Content("");
                        }
                        else
                        {
                            return Content(data.Code + ": " + data.Message);
                        }
                    }
                    return Content(result);

                }
                catch (Exception Ex)
                {
                    return Content(Ex.Message);
                }


            }
            catch (Exception Ex)
            {
                return Content(GetConfig.AppSetting["AppSettings:SistemError"]);
            }
        }
        #endregion

        #region Delete Handbook File
        //[ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> DeleteHandbookFile(int Id)
        {
            if (!lastSession.Update())
            {
                return RedirectToAction("Login", "Account");
            }

            try
            {
                string output = "";
                using(TransactionScope trx = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    TblDataHandbookAttachment data = await _context.TblDataHandbookAttachment.Where(x => x.Id == Id).LastOrDefaultAsync().ConfigureAwait(false);

                    if(data != null)
                    {
                        _context.TblDataHandbookAttachment.Remove(data);
                        await _context.SaveChangesAsync().ConfigureAwait(false);
                    }
                    else
                    {
                        output = "idNull";
                    }

                    trx.Complete();
                }

                return Content(output);
            }catch(Exception ex)
            {
                return Content(ex.Message);
            }
        }
        #endregion
    }
}
