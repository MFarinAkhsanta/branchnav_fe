﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.ViewModels
{
    public class ServiceResponseSingle2<T>
    {
        public int code { get; set; }
        public string message { get; set; }
        public T data { get; set; }
    }
    public class ListDataDropdownServerSide
    {
        public List<DataDropdownServerSide> items { get; set; }
        public int total_count { get; set; }
    }

    public class ListDataDropdownServerSide2
    {
        public List<DataDropdownServerSide> data { get; set; }
        public int recordTotals { get; set; }
    }

    public class ListDataDropdownServerSideString
    {
        public List<DataDropdownServerSideIdString> items { get; set; }
        public int total_count { get; set; }
    }

    public class DataPegawai { 
        public string nama { get; set; }
        public string email { get; set; }
    }

    public class DataDropdownServerSide
    {
        public int id { get; set; }
        public string text { get; set; }
        public string format_selected { get; set; }
        public string nama_text { get; set; }
        public string kodeOutlet { get; set; }
        
    }

    public class DataDropdownDivPengguna {
        public int id { get; set; }
        public string kodeDivisi { get; set; }
        public string text { get; set; }
        public string format_selected { get; set; }
        public string nama_text { get; set; }
    }

    public class DataDropdownServerSideIdString
    {
        public string id { get; set; }
        public string text { get; set; }
        public string format_selected { get; set; }
        public string nama_text { get; set; }
    }


}
