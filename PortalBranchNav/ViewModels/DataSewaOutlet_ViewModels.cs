﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.ViewModels
{
    public class DataSewaOutlet_ViewModels
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public string KodeUnit { get; set; }
        public string Periode { get; set; }
        public string Alamat { get; set; }
        public string KodePos { get; set; }
        public string TanggalLive { get; set; }
        public string TanggalMulaiSewa { get; set; }
        public string TanggalAkhirSewa { get; set; }
        public decimal? TotalHargaSewa { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CreatedTime { get; set; }
        public string UpdatedTime { get; set; }
        public string DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public string Nama { get; set; }
    }
}
