﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PortalBranchNav.ViewModels
{
    public class DataPendingAudit_ViewModels
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public string KodeOutlet { get; set; }
        public string NamaUnit { get; set; }

        public string JudulDokumen { get; set; }
        public DateTime? Periode { get; set; }
        public int? Tahun { get; set; }
        public int? TotalTemuan { get; set; }
        public int? TotalTemuanJatuhTempo { get; set; }
        public int? PendingJatuhTempo { get; set; }
        public int? PendingBelumJatuhTempo { get; set; }
        public int? Selesai { get; set; }
        public decimal? Presentase { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
    }

}
