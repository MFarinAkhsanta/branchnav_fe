﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.ViewModels
{
    public class MasterDataPengguna_ViewModels
    {
        public Int64 Number { get; set; }
        public int? Id { get; set; }
        public string Npp { get; set; }
        public string Nama { get; set; }
        public string Email { get; set; }

        public string Unit { get; set; }
        public int? UnitId { get; set; }
        public int? JabatanId { get; set; }
        public string ListJabatan { get; set; }

        public string Role { get; set; }
        public int? RoleId { get; set; }
        public string LastLogin { get; set; }

        public string Status { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsLDAP { get; set; }

        public string CreatedTime { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedTime { get; set; }
        public string UpdatedBy { get; set; }
        //public MasterDataPengguna_ViewModels()
        //{
        //    //baru
        //    IsActive = true;
        //    IsLDAP = true;

        //}
    }

    public class GetDataMasterPenggunaFromAPI
    {
        public Int64 number { get; set; }
        public int? id { get; set; }
        public string npp { get; set; }
        public string nama { get; set; }
        public string email { get; set; }

        public string unit { get; set; }
        public int? unitId { get; set; }
        public int? jabatanId { get; set; }
        public string jabatan { get; set; }
        public string listJabatan { get; set; }
        public int? roleId { get; set; }
        public string lastLogin { get; set; }

        public string status { get; set; }
        public bool? isActive { get; set; }
        public bool? isLDAP { get; set; }

        public string createdTime { get; set; }
        public string createdBy { get; set; }
        public string updatedTime { get; set; }
        public string updatedBy { get; set; }
    }

    public class GetJabatanPegawaiFromAPI
    {
        public int? id { get; set; }
        public int? pegawai_id { get; set; }
        public string pegawaiName { get; set; }
        public int? jabatan_Id { get; set; }
        public string jabatanName { get; set; }
        public int? unit_Id { get; set; }
        public string unit_Name { get; set; }
        public int? createdBy_Id { get; set; }
        public string createBy { get; set; }
        public DateTime? createdTime { get; set; }
        public int? statusJabatan { get; set; }
        public string statusJabatanName { get; set; }
        public DateTime? dateStart { get; set; }
        public DateTime? dateEnd { get; set; }
        public int? updatedBy_Id { get; set; }
        public string updatedBy { get; set; }
        public DateTime? updatedTime { get; set; }
        public bool? isDeleted { get; set; }
        public string isDeleteName { get; set; }
    }

    
}
