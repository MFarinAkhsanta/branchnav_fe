﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.ViewModels
{
    public class MasterJabatan_ViewModel
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public string Kode { get; set; }
        public string Nama { get; set; }
        public int GradeAwal { get; set; }
        public int GradeAkhir { get; set; }
        public string Keterangan { get; set; }
        public int Order_By { get; set; }
        public string CreatedTime { get; set; }
        public string UpdatedTime { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string Status { get; set; }
        public bool IsActive { get; set; }
        public string IsDelete { get; set; }
        public MasterJabatan_ViewModel() {
            IsActive = true;
        }
    }

    public class MJDataTableViewModel
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public string draw { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public MJTableViewModel data { get; set; }
    }

    public class MJTableViewModel
    {
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public int recordTotals { get; set; }
        public object data { get; set; }
    }
}
