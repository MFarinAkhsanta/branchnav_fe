﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using PortalBranchNav.Models.dbBranchNav;

namespace PortalBranchNav.ViewModels
{
    public class DataRevitalisasi_ViewModels
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public string KodeOutlet { get; set; }
        public string NamaUnit { get; set; }
        public string Periode { get; set; }
        public string NamaProyek { get; set; }
        public string JenisProyek { get; set; }
        public decimal? EstimasiBiaya { get; set; }
        //public string StatusProgress { get; set; }
        //public string DetailProgress { get; set; }
        public string NoDokumen { get; set; }
        public string UIC { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
    }

    public class DataRevitalisasiDetails_ViewModels
    {
        public int Id { get; set; }
        public int? DataRevitilisasiId { get; set; }
        public int? PegawaiId { get; set; }
        public int? UnitId { get; set; }
        public DateTime? Tanggal { get; set; }
        public string TanggalString { get; set; }
        public string Catatan { get; set; }
        public decimal? Progress { get; set; }
        public string ProgressString { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
    }

    public class DataRevitalisasiPhoto_ViewModels
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public int? RevitalisasiId { get; set; }
        public string Path { get; set; }
        public string FileName { get; set; }
        public int? UploadedById { get; set; }
        public string Uploader { get; set; }
        public DateTime? UploadedTime { get; set; }
        public string UploadedTimeString { get; set; }
    }

    public class DataRevitalisasiEditDataDetails_ViewModels
    {
        public int Id { get; set; }
        public int RevitalisasiId { get; set; }
        public IFormFile FileDetailPhoto { get; set; }
        public IFormFile file { get; set; }
        public IFormFile FileDetailDesain { get; set; }
        public string CatatanDesain { get; set; }
        public string CatatanPelaksanaan { get; set; }
        public DataRevitalisasiDetails_ViewModels Detail { get; set; }
        public List<DataRevitalisasiPhoto_ViewModels> Photo { get; set; }
        public List<DataRevitalisasiPhoto_ViewModels> Desain { get; set; }
    }

    public class sendUploadPhoto_ViewModels
    {
        public int RevitalisasiId { get; set; }
        public string Catatan { get; set; }
        public IFormFile file { get; set; }
    }

    public class cekRevitalisasi_ViewModel
    {
        public int Id { get; set; }
        public int? UnitId { get; set; }
        public string ProjectNo { get; set; }
        public string KodeOutlet { get; set; }
        public DateTime? Periode { get; set; }
        public string PeriodeString { get; set; }
        public string NamaProyek { get; set; }
        public string JenisProyek { get; set; }
        public int JenisProyekId { get; set; }
        public decimal? EstimasiBiaya { get; set; }
        public string EstimasiBiayaString { get; set; }
        public string NoDokumen { get; set; }
        public string Uic { get; set; }
        public string StatusProject { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
    }
    
}
