﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.ViewModels
{
    public class PengaturanSystemParameter
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public string Key { get; set; }
        public string value { get; set; }
        public string Keterangan { get; set; }
        public string CreatedTime { get; set; }
        public string UpdatedTime { get; set; }
        public string DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public PengaturanSystemParameter()
        {
            //baru
            IsActive = true;
        }
    }
}
