﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.ViewModels
{
    public class DataHandbookDetails_ViewModels
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public int? HandbookId { get; set; }
        public string Nama { get; set; }
        public string Kode { get; set; }
        public string Judul { get; set; }
        public string ForKodeUnit { get; set; }
        public bool? ForAllUnit { get; set; }
        public string Deskripsi { get; set; }
        public string Keyword { get; set; }
        
        //public string status { get; set; }
    }

    public class DataHandbookDetails2_ViewModels
    {
        public int id { get; set; }
        public int? handbookId { get; set; }
        public string judul { get; set; }
        public string forKodeUnit { get; set; }
        public bool? forAllUnit { get; set; }
        public string deskripsi { get; set; }
        public string keyword { get; set; }
        public int? orderBy { get; set; }
        public string createdTime { get; set; }
        public string updatedTime { get; set; }
        public string deletedTime { get; set; }
        public int? createdById { get; set; }
        public int? updatedById { get; set; }
        public int? deletedById { get; set; }
        public bool? isDeleted { get; set; }
        public bool? isActive { get; set; }
    }

    public class DataHandbookDetails_Attachment_ViewModel
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public int? HandbookDetailsId { get; set; }
        public string NamaFile { get; set; }
        public string TypeFile { get; set; }
        public string Size { get; set; }
        public string Path { get; set; }
        public string FullPath { get; set; }
        public DateTime? UploadTime { get; set; }
        public string UploadedTimeString { get; set; }
        public string Uploader { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? DeletedBy_Id { get; set; }
        public bool? IsDeleted { get; set; }
        public IFormFile FileHandbookDetail { get; set; }
        public string ListIdAttachment { get; set; }
    }

    public class DataHandbookDetails_Attachment__API_ViewModel
    {
        public Int64 number { get; set; }
        public int id { get; set; }
        public int? handbookDetailsId { get; set; }
        public string namaFile { get; set; }
        public string typeFile { get; set; }
        public string size { get; set; }
        public string path { get; set; }
        public string fullPath { get; set; }
        public DateTime? uploadTime { get; set; }
        public string uploadedTimeString { get; set; }
        public string uploader { get; set; }
        public DateTime? deletedTime { get; set; }
        public int? deletedBy_Id { get; set; }
        public bool? isDeleted { get; set; }
        public IFormFile fileHandbookDetail { get; set; }
        public string listIdAttachment { get; set; }
    }

    public class DataViewLog { 
        public Int64 number { get; set; }
        public DateTime createdTime { get; set; }
        public string aktivitas { get; set; }
        public string dilakukanOleh { get; set; }
    }

    public class sendUploadFile_ViewModels
    {
        public IFormFile file { get; set; }
        public int HandbookDetailsId { get; set; }
    }
}
