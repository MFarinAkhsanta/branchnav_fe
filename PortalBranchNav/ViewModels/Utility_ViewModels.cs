﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.ViewModels
{
    public class Utility_ViewModels<T>
    {
        public int status { get; set; }
        public string msg { get; set; }
        public T data { get; set; }
    }

    public class UtilityList_ViewModels<T>
    {
        public int status { get; set; }
        public string msg { get; set; }
        public List<T> data { get; set; }
    }

    public class GetDataFromAPI<T>
    {
        public int code { get; set; }
        public string message { get; set; }
        public T data { get; set; }
    }

    public class GetItem<T, M>
    {
        public T item1 { get; set; }
        public List<M> item2 { get; set; }
    }
}
