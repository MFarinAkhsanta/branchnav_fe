﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PortalBranchNav.ViewModels
{
    public class DataSewaATM_ViewModels
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public string KodeOutlet { get; set; }
        public string NamaUnit { get; set; }
        public string NamaWilayah { get; set; }
        public string NamaCabang { get; set; }
        public string Idatm { get; set; }
        public string TipeAtm { get; set; }
        public int? TahunDeployment { get; set; }
        public string Alamat { get; set; }
        public string Dati2 { get; set; }
        public string KodePos { get; set; }
        public DateTime? TanggalAktivasi { get; set; }
        public string TanggalAktivasiConvert { get; set; }

        public DateTime? TanggalMulaiSewa { get; set; }
        public string TanggalMulaiSewaConvert { get; set; }

        public DateTime? TanggalAkhirSewa { get; set; }
        public string TanggalAkhirSewaConvert { get; set; }


        public string TipePeriodeSewa { get; set; }
        public decimal? HargaSewaPerPeriode { get; set; }
        public decimal? HargaTotalSewa { get; set; }
        public string NoPks { get; set; }
        public DateTime? TanggalPks { get; set; }
        public string TanggalPksConvert { get; set; }

        public string PengelolaKasAtm { get; set; }
        public string NamaPengelolaKasAtm { get; set; }
        public string NamaVendorMaintaince { get; set; }
        public string Atmplace { get; set; }
        public string StatusAtm { get; set; }
        public string MerkMesinAtm { get; set; }
        public string SerialNumber { get; set; }
        public string PengelolaJaringanKomunikasi { get; set; }
        public string AtmBankPesaing { get; set; }

        public string BankPesaing { get; set; }
        public string NoPolisAsuransi { get; set; }
        public DateTime? BerlakuDari { get; set; }
        public string BerlakuDariConvert { get; set; }
        public string BerlakuSampaiConvert { get; set; }

        public DateTime? BerlakuSampai { get; set; }
        public int? Cctv { get; set; }
        public int? TitikCctv { get; set; }
        public string Kapasitas { get; set; }
        public string PeriodeBackup { get; set; }
        public string LokasiDvr { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
    }
}
