﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace PortalBranchNav.ViewModels
{
    public class DataTableFilter_ViewModels
    {
        public string draw { get; set; }
        public string sortColumn { get; set; }
        public string sortColumnDir { get; set; }
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
    }
}
