﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PortalBranchNav.ViewModels
{
    public class DataBRO_ViewModels
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public string Code { get; set; }
        public string NamaUnit { get; set; }
        public string KodeOutlet { get; set; }
        public DateTime? Periode { get; set; }
        public decimal? BroKecuranganInternal { get; set; }
        public decimal? RecoveryBroKecuranganInternal { get; set; }
        public decimal? BroKejahatan { get; set; }
        public decimal? RecoveryBroKejahatan { get; set; }
        public decimal? BroLokasi { get; set; }
        public decimal? RecoveryBroLokasi { get; set; }
        public decimal? BroProduk { get; set; }
        public decimal? RecoveryBroProduk { get; set; }
        public decimal? BroKerusakan { get; set; }
        public decimal? RecoveryBroKerusakan { get; set; }
        public decimal? BroGangguanBisnis { get; set; }
        public decimal? RecoveryBroGangguanBisnis { get; set; }
        public decimal? BroTransaksiDanDistribusi { get; set; }
        public decimal? RecoveryBroTransaksiDanDistribusi { get; set; }
        public decimal? NetBRO { get; set; }

        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
    }

    public class ImportFile_ViewModels {
        public IFormFile file { get; set; }
        public IFormFile FileImport { get; set; }

        public int? PeriodeImportData { get; set; }
        public string Periode { get; set; }
        public int? PeriodeTahun { get; set; }

        public int? TriwulanId { get; set; }
        public string PeriodeBulanTahun { get; set; }
    }
}
