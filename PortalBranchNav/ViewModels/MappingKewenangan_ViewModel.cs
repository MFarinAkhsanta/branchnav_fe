﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.ViewModels
{
    public class MappingKewenangan_ViewModel
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public int JabatanId { get; set; }
        public string Jabatan { get; set; }
        public int RoleId { get; set; }
        public string Role { get; set; }
        public string Keterangan { get; set; }
        public string CreatedTime { get; set; }
        public string UpdatedTime { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string Status { get; set; }
        public bool IsActive { get; set; }
        public string IsDelete { get; set; }
        public MappingKewenangan_ViewModel()
        {
            IsActive = true;
        }
    }
}
