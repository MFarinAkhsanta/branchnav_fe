﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.ViewModels
{
    public class PengaturanLookupViewModels
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
        public int Order_By { get; set; }
        public string Status { get; set; }
        public bool IsActive { get; set; }
        public string CreatedTime { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedTime { get; set; }
        public string UpdatedBy { get; set; }
        public PengaturanLookupViewModels()
        {
            //baru
            IsActive = true;
        }
    }

    public class DataTableViewModel
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public string draw { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public TableViewModel data { get; set; }
    }

    public class DataTableViewModel<T>
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public string draw { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public TableViewModel<T> data { get; set; }
    }

    public class TableViewModel
    {
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public int recordTotals { get; set; }
        public object data { get; set; }
    }

    public class TableViewModel<T>
    {
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public int recordTotals { get; set; }
        public List<T> data { get; set; }
    }

    public class PengaturanLookup_API_ViewModels
    {
        public Int64 number { get; set; }
        public int id { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public int? value { get; set; }
        public int? order_By { get; set; }
        public string status { get; set; }
        public string createdTime { get; set; }
        public string updatedTime { get; set; }
        public string createdBy { get; set; }
        public string updatedBy { get; set; }
        public bool isActive { get; set; }
    }
}
