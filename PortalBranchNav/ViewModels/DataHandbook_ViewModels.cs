﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.ViewModels
{
    public class DataHandbook_ViewModels
    {
        public Int64 number { get; set; }
        public int id { get; set; }
        public string kode { get; set; }
        public string nama { get; set; }
        public string keterangan { get; set; }

        public int order_By { get; set; }
        public string status { get; set; }
        public bool isActive { get; set; }
        public string createdTime { get; set; }
        public string createdBy { get; set; }
        public string updatedTime { get; set; }
        public string updatedBy { get; set; }
        public DataHandbook_ViewModels()
        {
            //baru
            isActive = true;
        }
    }
}
