﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.ViewModels
{
    public class RealisasiKinerjaDPK_ViewModels
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public string KodeUnit { get; set; }
        public string NamaUnit { get; set; }
        public decimal? RealisasiTabungan { get; set; }
        public decimal? RealiasiGiro { get; set; }
        public decimal? RealisasiDeposito { get; set; }
        public string Periode { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
