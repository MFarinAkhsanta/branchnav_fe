﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.ViewModels
{
    public class PengaturanMenu_ViewModels
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string Route { get; set; }
        public int? Order { get; set; }
        public int Visible { get; set; }
        public int? ParentNavigationId { get; set; }
        public string CreatedTime { get; set; }
        public string UpdatedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public string IconClass { get; set; }
        public bool? IsDeleted { get; set; }
        public int? DeletedById { get; set; }
        public string DeletedTime { get; set; }
        public string RoleId { get; set; }
        public string Role { get; set; }

    }
}
