﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.ViewModels
{
    public class TargetKinerjaKredit_ViewModels
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public string KodeUnit { get; set; }
        public string NamaUnit { get; set; }
        public decimal? BaselineKreditBb { get; set; }
        public decimal? TargetKreditBbtahunBerjalan { get; set; }
        public decimal? BaselinePenyaluranKur { get; set; }
        public decimal? TargetPenyaluranKurtahunBerjalan { get; set; }
        public decimal? BaselineKreditFlexi { get; set; }
        public decimal? TargetKreditFlexiTahunBerjalan { get; set; }
        public decimal? BaselineKreditGriya { get; set; }
        public decimal? TargetKreditGriyaTahunBerjalan { get; set; }
        public decimal? BaselineKreditOthers { get; set; }
        public decimal? TargetKreditOthersTahunBerjalan { get; set; }
        public string Periode { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
