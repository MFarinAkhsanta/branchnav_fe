﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiSalesWebPortal.ViewModels
{
    public class AuthenticationViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string IpAddress { get; set; }
        public string ClientId { get; set; }
    }
}
