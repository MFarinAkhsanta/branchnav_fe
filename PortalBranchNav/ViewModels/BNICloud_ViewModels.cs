﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.ViewModels
{
    public class DataBNICLoud
    {
        public int id { get; set; }
        public int jenisLaporanId { get; set; }
        public string jenisLaporanName { get; set; }
        public string divisiPenggunaId { get; set; }
        public string divisiPenggunaKode { get; set; }
        public string divisiPenggunaName { get; set; }
        public string laporanName { get; set; }
        public string laporanPathDownload { get; set; }
        public int posisiLaporanTerakhirById { get; set; }
        public string posisiLaporanTerakhirByName { get; set; }
        public int? downloadedById { get; set; }
        public string downloadedByName { get; set; }
        public int laporanStatusId { get; set; }
        public string laporanStatusName { get; set; }
        public string komentarReject { get; set; }
        public bool isRead { get; set; }
        public int createdById { get; set; }
        public string createdByName { get; set; }
        public int laporanCreatedByUnitId { get; set; }
        public string laporanCreatedByUnitName { get; set; }
        public string createdTime { get; set; }
        public int? updatedById { get; set; }
        public string updatedByName { get; set; }
        public string updatedTime { get; set; }
        public string deletedByName { get; set; }
        public string deletedTime { get; set; }
        public bool isActive { get; set; }
        public bool isDeleted { get; set; }
        //public IFormFile file { get; set; }
        public int Jns_Laporan { get; set; }
        public int Dvs_Pengguna { get; set; }
        public string Comment { get; set; }
        public IFormFile file { get; set; }
    }

    public class ResetInbox
    {
        public int code { get; set; }
        public string message { get; set; }
        public string data { get; set; }
    }

    public class DownloadBNICloud
    {
        public int code { get; set; }
        public string message { get; set; }
        public System.String data { get; set; }
    }

}
