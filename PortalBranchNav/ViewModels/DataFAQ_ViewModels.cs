﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.ViewModels
{
    public class DataFAQ_ViewModels
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public int KategoriId { get; set; }
        public string Kategori { get; set; }
        public string Pertanyaan { get; set; }
        public string Jawaban { get; set; }
    }
}
