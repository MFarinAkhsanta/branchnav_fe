﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.ViewModels
{
    public class Login_ViewModels
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class DetailLogin_ViewModels
    {
        public string Pegawai_Id { get; set; }
        public string Npp { get; set; }
        public string Nama_Pegawai { get; set; }
        public string Unit_Id { get; set; }
        public string Nama_Unit { get; set; }
        public string Jenis_Kelamin { get; set; }
        public string Role_Id { get; set; }
        public string Role_Unit_Id { get; set; }
        public string Role_Nama_Unit { get; set; }
        public string User_Id { get; set; }
        public string Nama_Role { get; set; }
        public string Status_Role { get; set; }
        public string User_Role_Id { get; set; }

        public string Images_User { get; set; }
        public string Password { get; set; }
        public bool? IsActive { get; set; }
        public bool? LDAPLogin { get; set; }

    }

    public class DeserializeListDataDropdownServerSide<T>
    {
        public List<T> data { get; set; }
        public int recordTotals { get; set; }
    }

    public class ServiceResult<T>
    {
        public int Code { get; set; } = 1;
        public string Message { get; set; }
        public T Data { get; set; }
    }

    public class ServiceResult2<T>
    {
        public int code { get; set; } = 1;
        public string message { get; set; }
        public T data { get; set; }
    }

    public class ServiceDropdownResult
    {
        public int Code { get; set; } = 1;
        public string Message { get; set; }
        public TableViewModel data { get; set; }
    }

    public class ServiceDropdownResult<T>
    {
        public int Code { get; set; } = 1;
        public string Message { get; set; }
        public TableViewModel<T> data { get; set; }
    }

    public class JWT_Response_ViewModel
    {
        public int code { get; set; }
        public string message { get; set; }
        public JWT_Data data { get; set; }
    }

    public class JWT_Data
    {
        public string jwT_Token { get; set; }
        public string refresh_Token { get; set; }
        public object error { get; set; }
        public string isCabang { get; set; }
        public string isNewInboxMsg { get; set; }
    }
}
