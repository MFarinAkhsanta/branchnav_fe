﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PortalBranchNav.ViewModels
{
    public class DataAuditcix_ViewModels
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public string KodeOutlet { get; set; }
        public string NamaUnit { get; set; }

        public DateTime? Periode { get; set; }
        public int? Tahun { get; set; }
        public string Triwulan { get; set; }
        public decimal? TotalScore { get; set; }
        public string Yudisium { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
    }

}
