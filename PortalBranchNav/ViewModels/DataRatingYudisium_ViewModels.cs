﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PortalBranchNav.ViewModels
{
    public class DataRatingYudisium_ViewModels
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public string KodeOutlet { get; set; }
        public string NamaUnit { get; set; }
        public string NamaWilayah { get; set; }

        public string Periode { get; set; }

        public int? Tahun { get; set; }
        public string RatingKinerja { get; set; }
        public string RatingPengelolaanResiko { get; set; }
        public string RatingGabungan { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public DateTime? DeletedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public int? DeletedById { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
    }

}
