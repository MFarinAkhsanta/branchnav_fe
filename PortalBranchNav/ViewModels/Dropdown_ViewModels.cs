﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalBranchNav.ViewModels
{
    public class Dropdown_ViewModels
    {
        public int? Value { get; set; }
        public string Name { get; set; }
    }
}

//Scaffold-DbContext "Server=localhost;Database=dbBranchNav;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models/dbBranchNav -force
